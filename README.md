# Getting Started

## Full installation

First, install git and curl, and then run the following:

``` bash
curl https://bitbucket.org/carta-healthcare/carta-toolbox/raw/master/scripts/bootstrap.sh > cartainstall.sh
chmod +x cartainstall.sh
sudo ./cartainstall.sh

```
Once that command completes, you'll have a fully-functional installation of carta-client, located in ~/dev/carta-toolbox. You'll need to reload your terminal
to make sure that carta-client is available on your PATH.

## Partial install for devs
**Note**: If you are doing the initial setup on your local machine to gain access to Carta dev resources and services, a full installation is not required. Run the following instead:
``` bash
curl https://bitbucket.org/carta-healthcare/carta-toolbox/raw/master/bootstrap-macosx.sh | bash

```
The command uses homebrew and pip to install the necessary packages before placing the carta-client script on your PATH, which is sufficient for running `carta-client` commands you need locally.
If this is your first time installing python3 you may need to: run the bash script once, restart your session, and then run it again for full installation of python packages.

# Using carta-client
See https://paper.dropbox.com/doc/Atlas-Sandbox-Guide--Ay7r5IeD565COmjMgJMjcvaCAg-NiF1PbY1FB71vq0UXVFxZ for a guide on how to use carta-client to interact with a
deployment.

# Launching Platform with carta-client

You can use carta-client to stand up a new deployment of the platform with either docker-compose or Kubernetes. First, contact the assigned Customer Support Engineer
for the deployment and get the Vault Role ID and Vault Secret ID for the deployment. These function as the license key for the deployment, and allow the deployment
to access the code and its configuration.

Once the Role ID and Secret ID are obtained, run these commands to launch the platform (putting the exports in the permanent environment somewhere):
``` bash
export VAULT_ROLE_ID="xxx" 
export VAULT_SECRET_ID="yyy"
scripts/carta-client release deploy --deployment <deployment_name> --no-version-matching --pull
```



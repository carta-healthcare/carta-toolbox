#anaconda, python, shell init
class cartaclient::base {
  $core_packages = [
    'vim', 'wget', 'curl', 'zip', 'unzip']
  package { $core_packages:
    ensure => installed,
  }

  file { "${home}/bin" :
    ensure => directory,
    owner  => $user,
  }

  #Anaconda pre-requisites
  case $facts['os']['name'] {
    'RedHat', 'CentOS': {
      $sys_packages = [
        'libXcomposite','libXcursor','libXi','libXtst','libXrandr',
        'alsa-lib','mesa-libEGL','libXdamage','mesa-libGL'
      ]
      package { $sys_packages:
        ensure => installed,
      }
    }
    /^(Debian|Ubuntu)$/: {
      $sys_packages = [
        'libgl1-mesa-glx','libegl1-mesa','libxrandr2',
        'libxss1','libxcursor1','libxcomposite1','libasound2','libxi6','libxtst6', 'jq'
        ]

      package { $sys_packages:
        ensure => installed,
      }
    }
  }

  # Anaconda
  file {"${home}/Miniconda2-4.7.12.1-Linux-x86_64.sh":
    ensure         => present,
    source         => 'https://repo.anaconda.com/miniconda/Miniconda2-4.7.12.1-Linux-x86_64.sh',
    checksum       => 'md5',
    checksum_value => '23bf3acd6aead6e91fb936fc185b033e',
    mode           => '0644'
  }

  exec { "bash ${home}/Miniconda2-4.7.12.1-Linux-x86_64.sh -b -p ${home}/anaconda":
    unless => "ls ${home}/anaconda",
    path   => '/bin:/usr/bin:/usr/local/bin',
  }

  exec {'conda-activate':
    command => "echo \"source activate cartaclient\" >> ${home}/.bashrc",
    unless  => "grep 'source activate cartaclient' ${home}/.bashrc",
    path    => '/bin:/usr/bin:/usr/local/bin'
  }

  exec { "${home}/anaconda/bin/conda env create -f ${home}/dev/carta-toolbox/puppet/files/environment.yml -p ${home}/anaconda/envs/cartaclient":
    unless => "ls ${home}/anaconda/envs/cartaclient",
    path   => '/bin:/usr/bin:/usr/local/bin'
  }
}

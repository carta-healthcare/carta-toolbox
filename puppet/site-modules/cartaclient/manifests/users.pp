#ensure user belongs to docker group and has sudo with no password
class cartaclient::users {
  $groups = ['docker']
  group { $groups:
    ensure => 'present'
  }

  user { "${user}":
    ensure     => present,
    comment    => 'add to docker group',
    managehome => true,
    groups     => $groups,
  }
}

#docker and extra software required by carta-client
class cartaclient::software {
  # Software packages
  case $facts['os']['name'] {

    'RedHat', 'CentOS': {
      $sys_packages = [ 'yum-utils','device-mapper-persistent-data','lvm2']
      package { $sys_packages:
        ensure => installed,
      }

      # Azure cli
      exec { 'rpm --import https://packages.microsoft.com/keys/microsoft.asc':
        unless => 'rpm -qa --qf \'%{VERSION}-%{RELEASE} %{SUMMARY}\n\' gpg-pubkey* | grep Microsoft',
        path   => '/bin:/usr/bin'
      }

      file {'/etc/yum.repos.d/azure-cli.repo':
        ensure  => present,
        owner   => 'root',
        mode    => '0644',
        content => '[azure-cli]
name=Azure CLI
baseurl=https://packages.microsoft.com/yumrepos/azure-cli
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
'
      }

      exec { "ln -s ${home}/anaconda/envs/cartaclient/bin/python3 /usr/bin/python3":
        unless => 'ls /usr/bin/python3',
        path   => '/bin:/usr/bin'
      }

      exec { 'yumdownloader azure-cli && mv azure-cli-*.rpm /tmp':
        unless => 'ls /tmp/azure-cli-*.rpm',
        path   => '/bin:/usr/bin'
      }

      exec { 'rpm -ivh --nodeps /tmp/azure-cli*':
        unless => 'command -v az',
        path   => '/bin:/usr/bin'
      }

      # Docker
      $repos = [
        # 'rhel-7-server-rpms',
        'rhel-*-server-extras-rpms'
      ]
      $repos.each |$repo| {
        exec {"subscription-manager repos --enable=${repo}":
          unless => "subscription-manager repos --list-enabled | grep ${repo}",
          path   => '/bin:/usr/bin'
        }
      }
      exec {'yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo':
        unless => 'yum-config-manager | grep docker-ce-stable',
        path   => '/bin:/usr/bin'
      }

      $docker_packages = [ 'docker-ce','docker-ce-cli','containerd.io']
      package { $docker_packages:
        ensure => installed,
      }

      service { 'docker':
        ensure => 'running',
        enable => true,
      }

    }
    /^(Debian|Ubuntu)$/: {
      $sys_packages = [
        'apt-transport-https', 'ca-certificates', 'software-properties-common','gnupg','lsb-release'
        ]

      package { $sys_packages:
        ensure => installed,
      }

      # Azure
      exec { 'curl -sL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | tee /etc/apt/trusted.gpg.d/microsoft.asc.gpg':
        unless => 'ls /etc/apt/trusted.gpg.d/microsoft.asc.gpg',
        path   => '/bin:/usr/bin'
      }

      exec { 'add-apt-repository -y "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $(lsb_release -cs) main"' :
        unless => 'grep "deb \[arch=amd64\] https://packages.microsoft.com/repos/azure-cli/" /etc/apt/sources.list',
        path   => '/bin:/usr/bin'
      }

      package { 'azure-cli' :
        ensure => 'installed'
      }

      # Docker
      exec { 'curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -' :
        path   => '/bin:/usr/bin:/usr/local/bin',
        unless => 'apt-key list | grep docker.com',
      }

      exec { 'add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"':
        unless => 'grep "deb \[arch=amd64\] https://download.docker.com/linux/ubuntu" /etc/apt/sources.list',
        path   => '/bin:/usr/bin:/usr/local/bin'
      }

      $docker_packages = ['docker-ce', 'docker-ce-cli', 'containerd.io']
      package { $docker_packages:
        ensure => 'installed'
      }

      service { 'docker':
        ensure => 'running',
        enable => true,
      }

    }
    Default: {
      $sys_packages = [ ]
      package { $sys_packages:
        ensure => installed,
      }
    }
  }

  # Vault
  file {"${home}/vault_1.6.2_linux_amd64.zip":
    ensure         => present,
    source         => 'https://releases.hashicorp.com/vault/1.6.2/vault_1.6.2_linux_amd64.zip',
    checksum       => 'md5',
    checksum_value => 'f29242b342d2a676e8ea5953b736591a',
    mode           => '0600'
  }

  exec { 'unzip vault':
    command => "unzip ${home}/vault_1.6.2_linux_amd64.zip -d ${home}/bin",
    unless  => "ls ${home}/bin/vault",
    path    => '/bin:/usr/bin'
  }

  # # Dockerize
  # file {"${home}/dockerize-linux-amd64-v0.6.1.tar.gz":
  #   ensure         => present,
  #   source         => 'https://github.com/jwilder/dockerize/releases/download/v0.6.1/dockerize-linux-amd64-v0.6.1.tar.gz',
  #   checksum       => 'md5',
  #   checksum_value => '5c1b3d9a7d9341e23e0b8bdf2a288387',
  #   mode           => '0600'
  # }

  # exec { 'untar dockerize':
  #   command => "tar xvzf ${home}/dockerize-linux-amd64-v0.6.1.tar.gz && mv dockerize ${home}/bin",
  #   unless  => "ls ${home}/bin/dockerize",
  #   path    => '/bin:/usr/bin'
  # }
}

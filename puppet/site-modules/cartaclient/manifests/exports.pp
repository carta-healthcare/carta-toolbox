#shell setup
class cartaclient::exports {

  $shelrcs = [
    "${home}/.bashrc",
    "${home}/.zshrc",
  ]
  $exports = [
    'export FACTER_home=$HOME',
    "export FACTER_user=${user}",
    'export VAULT_ROLE_ID=',
    'export VAULT_SECRET_ID=',
    'export AWS_ACCESS_KEY_ID=',
    'export AWS_SECRET_ACCESS_KEY=',
    'export PATH=$PATH:$HOME/dev/carta-toolbox/scripts',
    'export PATH=$PATH:$HOME/anaconda/bin',
    'export PATH=$PATH:$HOME/bin'
  ]
  $shelrcs.each |$shellrc| {
      file { $shellrc:
        ensure => present,
        path   => $shellrc,
      }
      $exports.each |$line| {
      file_line {"ensure ${line} is in ${shellrc}":
        ensure => present,
        path   => $shellrc,
        line   => $line,
      }
    }
  }
}

import pathlib
from setuptools import setup, find_packages

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# Metadata from files
README = (HERE / "README.md").read_text()
VERSION = (HERE / "VERSION").read_text().strip()

packages = find_packages()
setup(
    name="carta-client",
    description="Client API for Carta Healthcare's Products",
    long_description=README,
    url="https://carta.healthcare/cartographer",
    author="Carta Healthcare, Inc.",
    author_email="hello@carta.healthcare",
    version=VERSION,
    packages=packages,
    package_data={k: ["*.csv", "*.yml", "*.html"] for k in packages},
    scripts=["scripts/carta-client"],
    entry_points={},
    license="Apache-2.0",
    install_requires=["websockets", "requests", "pyyaml", "jinja2", "boto3"],
    include_package_data=True,
)

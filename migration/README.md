# Upgrade path: From 3.0.0 or 3.1.0 to 3.1.1
# Steps for upgrade:
# JSON file name for 3.0.0: field_info_3.0.0.json
# JSON file name for 3.1.0: field_info_3.1.0.json
1. Backup current database: command: (carta-client local-backup --all)
2. Copy the backup file name.
3. Switch all installed plugins to 3.1.1 version.
4. Switch carta-toolbox to 3.1.1 version.
5. Upgrade ATLAS to 3.1.1 version.
6. Restart redis container
7. Load released registries that supports Atlas 3.1.1.
8. Load dynamic lists for the installed NCDR registries only (CathPCI/ AFib/ TVT based on the deployments).
9. Backup database with current state (Updated registries & dynamic list)
	a. command: (carta-client local-backup --all).
	b. Copy the backup file name.
10. Execute upgrade script to update the encounters as per latest data structures
     a. Execute migration script in test mode first to validate the migration behavior
     b. Execute migration script to perform actual upgrade
	Note: The actual migration should be done only if the test migration output seems appropriate]
	
Test Migration (Reference 9.a)
------------------------------
1. cd /home/carta-dev/dev/carta-toolbox/migration/upgrade_to_3.1.1
2. python execute_migration.py --file_path=<complete path of the JSON file> &> output.txt
    Note: This will generate output file in the same location
3. Review output file for any error.
    Note: Report for any error if identified.

Actual Migration (Reference 9.b)
1. cd /home/carta-dev/dev/carta-toolbox/migration/upgrade_to_3.1.1
2. python execute_migration.py --update_flag=True --create_patient='True' --file_path=<complete path of the JSON file> &> output.txt
    Note: This will generate output file in the same location
3. Review output file for any error.
    Note: Report for any error if identified.

#!/usr/bin/env python
import sys, os
import logging
import time
import pickle
import subprocess
from argparse import ArgumentParser
from carta_client.utils import carta_popen

LOG = logging.getLogger(__name__)


def execute_migration(options):

    script_path = "/carta/modules/carta-atlas/scripts/update_encounter_data.py"
    file_path = options.file_path

    docker_ps_command = carta_popen(["docker", "ps"], stdout=subprocess.PIPE)
    docker_ps_command.wait()

    # Get the ocean container id
    grep_command = carta_popen(
        ["grep", "ocean"], stdin=docker_ps_command.stdout, stdout=subprocess.PIPE,
    )
    grep_command.wait()
    ocean_cut_command = carta_popen(
        ["cut", "-d", " ", "-f", "1"],
        stdin=grep_command.stdout,
        stdout=subprocess.PIPE,
    )
    ocean_cut_command.wait()
    ocean_name = ocean_cut_command.stdout.read().decode("utf-8")
    if ocean_name:
        ocean_name = ocean_name.split()[0]
    else:
        print("No Ocean container is running. Exiting.")
        sys.exit(1)

    # Check if ocean container is working fine
    carta_popen(["docker", "exec", "-it", ocean_name, "/bin/bash", "-c", "pwd",]).wait()
    copy_flag = False
    try:
        if os.path.exists(file_path):
            # Copy json file in docker container in /tmp folder
            docker_path = f"{ocean_name}:/tmp/field_info.json"
            carta_popen(
                ["docker", "cp", file_path, docker_path],
                stdin=grep_command.stdout,
                stdout=subprocess.PIPE,
            ).wait()
            copy_flag = True
            file_name = file_path.split("/")[-1]
            ocean_file_path = f"/tmp/{file_name}"

            chmod_cmd = f"chmod 755 {script_path}"
            carta_popen(
                ["docker", "exec", "-it", ocean_name, "/bin/bash", "-c", chmod_cmd,]
            ).wait()
            # Execute the script
            run_script_cmd = f"python {script_path} --update_flag={options.update_flag} --output_directory='{options.output_directory}' --limit='{options.limit}' --customer_id='{options.customer_id}'"
            if options.registry_name_version:
                run_script_cmd += (
                    f" --registry_name_version='{options.registry_name_version}'"
                )
            if options.create_patient:
                run_script_cmd += f" --create_patient='{options.create_patient}'"

            print(f"Executing command:{run_script_cmd}")
            carta_popen(
                [
                    "docker",
                    "exec",
                    "-it",
                    ocean_name,
                    "/bin/bash",
                    "-c",
                    run_script_cmd,
                ]
            ).wait()
            carta_popen(
                ["docker", "cp", f"{ocean_name}:/tmp/shared.pkl", "/tmp/shared.pkl"]
            ).wait()

            shared = ""
            with open("/tmp/shared.pkl", "rb") as fp:
                shared = pickle.load(fp)
            summary_file_path = f"{ocean_name}:{shared}"
            dest_file_path = f"/tmp/."
            carta_popen(["docker", "cp", summary_file_path, dest_file_path]).wait()
        else:
            print("field_info.json file doesnt exist. Exiting.")
            sys.exit(1)
    except Exception as ex:
        print(f"Failed to execute the script:{ex}.")
        sys.exit(1)
    finally:
        if copy_flag:
            delete_file_cmd = f"rm -f {ocean_file_path}"
            carta_popen(
                [
                    "docker",
                    "exec",
                    "-it",
                    ocean_name,
                    "/bin/bash",
                    "-c",
                    delete_file_cmd,
                ]
            ).wait()


def parse_args():
    parser = ArgumentParser()
    parser.add_argument(
        "--update_flag",
        default=False,
        help="Set it to true to run actual migration and update encounters in db",
    )
    parser.add_argument(
        "--customer_id", help="Customer ID to process", default="carta",
    )
    parser.add_argument(
        "--limit",
        default=250,
        help="Number of encounters to be processed in one transaction",
    )
    parser.add_argument(
        "--registry_name_version",
        help="Registry schema name and version like AFib_v1.0",
    )
    parser.add_argument(
        "--create_patient",
        default=False,
        help="Create patients for existing encounters",
    )
    parser.add_argument(
        "--file_path",
        required=True,
        help="Complete path of JSON file name for field mapping",
    )
    parser.add_argument(
        "--output_directory",
        default="/carta",
        help="Directory where logs and migration summary files are written",
    )
    return parser.parse_args()


def exit_method():
    parser = ArgumentParser()
    parser.print_usage(sys.stderr)
    sys.exit(0)


def main():
    options = parse_args()
    execute_migration(options=options)


if __name__ == "__main__":
    main()

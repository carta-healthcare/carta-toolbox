#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."

cd "$DIR"

docker build -t carta-client-centos -f salt/Dockerfile.centos .
docker run -it -v $DIR:/carta-toolbox carta-client-centos /bin/bash


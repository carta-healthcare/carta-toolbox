#!/usr/bin/env bash


if [ $(uname) == "Darwin" ]
then
    echo "Hey there partner, seems like you got yourself a mac, please use the mac bootstrap"
    echo "---------------------------"
    echo "curl https://bitbucket.org/carta-healthcare/carta-toolbox/raw/master/bootstrap-macosx.sh | $SHELL"
    echo "---------------------------"
    exit
fi

if [ $(id -u) != "0" ] && [ -z "${SUDO_USER}" ]
then
    echo "This script needs to be run with sudo"
    exit $?
fi

if grep "Ubuntu" /etc/issue
then
  if ! command -v wget
  then
    apt-get update
    apt-get install -y wget
  fi
  if ! command -v bolt
  then
    if grep "Ubuntu 20" /etc/issue
    then
      wget https://apt.puppet.com/puppet-tools-release-focal.deb
      dpkg -i puppet-tools-release-focal.deb
    fi
    if grep "Ubuntu 18" /etc/issue
    then
      wget https://apt.puppet.com/puppet-tools-release-bionic.deb
      dpkg -i puppet-tools-release-bionic.deb
    fi
    if grep "Ubuntu 16" /etc/issue
    then
      wget https://apt.puppet.com/puppet-tools-release-xenial.deb
      dpkg -i puppet-tools-release-xenial.deb
    fi
    apt-get update
    apt install puppet-bolt
  fi
fi

if grep "Red Hat Enterprise" /etc/redhat-release && ! command -v bolt
then
  if grep "release 6" /etc/redhat-release
  then
    rpm -Uvh https://yum.puppet.com/puppet-tools-release-el-6.noarch.rpm 
  fi
  if grep "release 7" /etc/redhat-release
  then
    rpm -Uvh https://yum.puppet.com/puppet-tools-release-el-7.noarch.rpm
  fi
  if grep "release 8" /etc/redhat-release
  then
    rpm -Uvh https://yum.puppet.com/puppet-tools-release-el-8.noarch.rpm
  fi
  if grep "release 9" /etc/redhat-release
  then
    rpm -Uvh https://yum.puppet.com/puppet-tools-release-el-9.noarch.rpm
  fi
  yum install -y puppet-bolt
fi

# Bolt is installed in /usr/local/bin, and sometimes that isn't in the PATH... add it
export PATH=/usr/local/bin:$PATH

###############################################################

export FACTER_user=$SUDO_USER
export FACTER_home=/home/$SUDO_USER
env | grep FACTER
env | grep SUDO_USER
bolt -h

mkdir -p $FACTER_home/dev/puppet
cd $FACTER_home/dev/puppet

touch bolt.yaml

cat > bolt-project.yaml << EOF
---
modulepath:
- "./site-modules"
modules:
- puppetlabs/vcsrepo

EOF

cat > Puppetfile << EOF
moduledir '.modules'

mod 'puppetlabs/vcsrepo', '3.2.0'

EOF

cat > bootstrap.pp << EOF
class cartaclient::base {
  \$core_packages = ['vim', 'wget', 'curl', 'git']
  package { \$core_packages:
    ensure => installed,
  }

  vcsrepo { "$FACTER_home/dev/carta-toolbox":
    ensure   => present,
    provider => git,
    source   => 'https://bitbucket.org/carta-healthcare/carta-toolbox.git'
  }

  exec { 'own carta-toolbox':
    command => "chown -R $SUDO_USER:$SUDO_USER $FACTER_home/dev/carta-toolbox",
    path => '/usr/bin:/bin'
  }

  exec { 'DEBUG':
    command => "echo $FACTER_home && echo $FACTER_user",
    path => '/usr/bin:/bin'
  }

}

include cartaclient::base
EOF

#global settings directory 
mkdir -p $FACTER_home/.puppetlabs/etc/bolt/
mkdir $FACTER_home/tmp

#disable analitycs
cat > $FACTER_home/.puppetlabs/etc/bolt/analytics.yaml << EOF
disabled: true
EOF

#override tmp directory for localhost
cat > $FACTER_home/.puppetlabs/etc/bolt/bolt-defaults.yaml << EOF
inventory-config:
  local:
    cleanup: false
    tmpdir: $FACTER_home/tmp
EOF
###############################################################
  
bolt module install
bolt apply bootstrap.pp --targets localhost
rm -rf $FACTER_home/dev/puppet

#################################

# docker testing 
if test "$CARTA_BOOTSTRAP"
then
  cd /app/puppet
else
  cd $FACTER_home/dev/carta-toolbox/puppet
fi

bolt module install
bolt apply carta_client.pp --targets localhost
# sudo -E env "PATH=$PATH" bolt apply carta_client.pp --targets localhost

echo "Setup completed"
echo "Please reload your shell"
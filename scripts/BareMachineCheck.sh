#!/usr/bin/env bash

# Declare Commands, Results, and Error arrays
COMMANDS=()
RESULTS=()
MESSAGES=()

# Sudo check
SUDO_OUTPUT=$(sudo -l)
# Add 'sudo' to Commands array
COMMANDS+=("Sudo")
## If ALL permissions found...
if [[ $SUDO_OUTPUT == *"(ALL) ALL"* ]]; then

    ## ...add 0 to the Results array (for sudo)
    RESULTS+=(0)
    ## Add empty error message
    MESSAGES+=("✅ User has sudo permissions")

else

    ## Otherwise add 1 to the Results array (for sudo)
    RESULTS+=(1)
    ## And add error message to error array
    MESSAGES+=("❌ User does not have sudo permissions")

fi

# RAM check, assuming a windows machine is not used
COMMANDS+=("RAM")
## This is a mac machine
if [[ $OSTYPE == "darwin"* ]]; then

    ## Get memory in GiB
    MEM=$(sysctl -n hw.memsize)
    RAMSIZE=$(expr $MEM / $((1024**3)))

## This is a unix machine
else

    # Get the memory in KiB and convert it to GiB
    MEM=$(grep MemTotal /proc/meminfo | awk '{print $2}')
    RAMSIZE=$(expr $MEM / $((1024 ** 2)))

fi
## Memory is at least 32 GiB - Success
if [[ $RAMSIZE -ge 32 ]]; then

    RESULTS+=(0)
    MESSAGES+=("✅ At least 32 GiB Memory")

## Memory is not at least 32 GiB - Failed
else 

    RESULTS+=(1)
    MESSAGES+=("❌ At least 32 GiB required. Available: ${RAMSIZE} GiB")

fi

# Docker check
_=$(docker)
DOCKER_RETURN_CODE=$?
## Add 'docker' to the Commands array
COMMANDS+=("Docker")
## Add the return code to the Results array
RESULTS+=($DOCKER_RETURN_CODE)
## (Optional) Add message to Messages array
if [[ $DOCKER_RETURN_CODE == 0 ]]; then

    MESSAGES+=("✅ Docker installed")

else

    MESSAGES+=("❌ Docker not installed")

fi

# Python install check
PYTHON_VERSION=$(python -V)

# For versions below 3.4, version prints to stderr instead of stdout
if [[ $PYTHON_VERSION == "" ]]; then

    # Get the version from stderr
    PYTHON_VERSION=$(python -V 2>&1 > /dev/null)

fi

PYTHON_RETURN_CODE=$?
## Add 'python_install' to the Commands array
COMMANDS+=("Python_Install")
## Add the return code to the Results array
RESULTS+=($PYTHON_RETURN_CODE)
## (OPTIONAL) Add message to MESSAGES array
if [[ $PYTHON_RETURN_CODE == 0 ]]; then

    MESSAGES+=("✅ Python installed")

else

    MESSAGES+=("❌ Python not installed")

fi

# Python version check
## Add 'python_version' to the Commands array
COMMANDS+=("Python_Version")
## If PYTHON_VERSION contains 'Python 3.'...
if [[ $PYTHON_VERSION == "Python 3."* ]]; then

    ## ...then add 0 to the Results array (for python_version)
    RESULTS+=(0)
    ## Add empty string for error message
    MESSAGES+=("✅ Python version 3")

else

    ## Otherwise, add 1 to the Results array (for python_version)
    RESULTS+=(1)
    ## Add error message to MESSAGES array
    MESSAGES+=("❌ Python version 3 required. Installed version: ${PYTHON_VERSION}")

fi

FAILED=0
FAILURE_MESSAGE=""
echo ""
echo "Results:"
echo ""
# Iterate through Results
for i in "${!RESULTS[@]}"; do

    # If any result is not equal to 0, the health check fails
    if [[ ${RESULTS[$i]} != 0 ]]; then

        ((FAILED=FAILED+1))

    fi
    echo "${MESSAGES[$i]}"

done

# The check failed
if [[ $FAILED -ge 1  ]]; then

    echo ""
    echo "Health check failed :("
    exit 1

# The check passed
else

    echo "Health check passed :)"
    exit 0

fi

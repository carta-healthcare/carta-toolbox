#! /bin/bash

for img in ubuntu:18.04 ubuntu:20.04 registry.access.redhat.com/ubi7/ubi registry.access.redhat.com/ubi8/ubi
# for img in ubuntu:18.04
do
    echo $img
    sudo docker build --build-arg IMAGE=$img --no-cache -t test-bootstrap-sh .
done
if ! ls $HOME/dev/carta-toolbox
then
    echo "Cloning the toolbox"
    mkdir -p $HOME/dev/
    git clone https://bitbucket.org/carta-healthcare/carta-toolbox.git $HOME/dev/carta-toolbox
fi



# Homebrew, the correct way ############################################################
if command -v brew
then
    echo "Installing the tools"
    brew update
    if ! command -v python3
    then
        brew install python3
        # User will need to restart terminal in order for pip3 to install packages to /opt/homebrew/lib/python3.10/site-packages
        echo "Please restart terminal session for brew python path to be added to pip"
        exit 0
    fi
    
    pip3 install requests websockets PyYAML jinja2 boto3
    
    if ! command -v vault
    then
        brew tap hashicorp/tap
        brew install hashicorp/tap/vault
    fi

    if ! command -v aws 
    then
        brew install awscli
    fi

    if ! command -v aws-vault
    then
	brew install aws-vault
    fi
fi
#########################################################################################
if command -v port
then
    echo "script the wrong way of installing things in a mac here"
fi


if ! echo $PATH | grep carta-toolbox
then
    echo "Exporting the toolbox"  
    echo "export PATH=\"\$PATH:\$HOME/dev/carta-toolbox/scripts\"" >> $HOME/.zshrc
    echo "export PATH=\"\$PATH:\$HOME/dev/carta-toolbox/scripts\"" >> $HOME/.bashrc
    export PATH=$PATH:$HOME/dev/carta-toolbox/scripts
fi

if carta-client -h ; then
    echo "carta-client was successfully installed and is ready to use."
else
    echo "carta-client command did not execute successfully. Make sure carta-client is in your PATH variable."
fi


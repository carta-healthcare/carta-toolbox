#!/bin/bash
mamba install conda-pack
mamba create -n carta-client python=3.8
conda activate carta-client
python setup.py install
VERSION=`cat VERSION`
FILENAME="carta-client-$VERSION.tar.gz"
OUTPUT="dist/$FILENAME"
mkdir -p dist
rm "$OUTPUT"
echo `which python3`
conda pack -n carta-client -o "$OUTPUT"
aws s3 cp "$OUTPUT" s3://carta-healthcare-deployment-infrastructure/releases/$FILENAME
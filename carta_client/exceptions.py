import json
import requests

decode_exceptions = [json.decoder.JSONDecodeError]
try:
    import simplejson.errors

    decode_exceptions.append(simplejson.errors.JSONDecodeError)
except ImportError:
    pass


class CartaClientError(Exception):
    def __init__(self, message, context=None, status_code=None):
        if status_code:
            message += " || " + str(status_code)
        if isinstance(context, requests.Response):
            try:
                data = context.json()
                message += " || " + str(data)
            except tuple(decode_exceptions):
                message += " || " + str(context.content)

        super(CartaClientError, self).__init__(message)

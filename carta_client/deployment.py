import os
import subprocess
from rich.console import Console
from rich.syntax import Syntax
from rich.prompt import Prompt, Confirm
from rich.table import Table


class DeploymentSetup:
    def __init__(self, deployment=None):
        self.console = Console(record=True)
        self.style = "bold green"
        self.deployment = deployment
        self.helm_values = None
        self.console.clear()
        self.extra_env = {
            "VAULT_ROLE_ID": None,
            "VAULT_SECRET_ID": None,
            "VAULT_ADDR": "https://vault.carta.healthcare",
            "CARTA_DEPLOYMENT": self.deployment,
        }

    def print(self, text):
        self.console.print(text, style=self.style)

    def print_code(self, code, syntax="bash"):
        syntax = Syntax(
            code, syntax, theme="monokai", line_numbers=True, word_wrap=True
        )
        self.print(syntax)

    def vault(self):
        while not self.deployment:
            self.console.clear()
            self.console.rule("Setting up Vault")
            choice = Prompt.ask(
                "Do you wish to create a new vault secret or use an existing one?",
                choices=["new", "existing"],
                default="new",
            )
            self.deployment = Prompt.ask("Deployment name")
            if not self.deployment:
                continue

            confirm = "Setting up an existing"
            if choice == "new":
                confirm = "Creating a new"
            confirm += f" deployment named [cyan]{self.deployment}[/cyan]"

            if not Confirm.ask(confirm, default="y"):
                self.deployment = None
                continue

            self.extra_env["CARTA_DEPLOYMENT"] = self.deployment

            if choice == "new":
                self.print(
                    "To [cyan]create[/cyan] a new deployment, [yellow]execute[/yellow] the following command or ask someone with [yellow]admin privileges[/yellow] to run it for you:"
                )
                self.print_code(f"carta config --release {self.deployment}")
                input("Press Enter when you have execute the command...")

    def get_shell_rc(self):
        home = os.environ.get("HOME")
        shell_rc = os.path.basename(os.environ.get("SHELL"))
        shell_rc = f".{shell_rc}rc"
        shell_rc = os.path.join(home, shell_rc)
        return shell_rc

    def persist_export(self, key, value):
        shell_rc = self.get_shell_rc()
        contents = ""
        with open(shell_rc, "r") as f:
            contents = f.read()

        new_contents = []
        for line in contents.split("\n"):
            new_contents.append(line)
            if key in line:
                del new_contents[len(new_contents) - 1]

        new_contents.append(f"export {key}={value}")
        os.environ[key] = value
        with open(shell_rc, "w") as f:
            f.write("\n".join(new_contents))

    def get_vault_command(self):
        return (
            f"vault read -format=json secret/deployments/{self.deployment}/helm-values"
        )

    def mask(self, string, length):
        if not string:
            return None
        return string[0:length] + len(string[length:-length]) * "*" + string[-length:]

    def exports(self):
        self.console.clear()
        self.console.rule("Setting deployment credentials")

        self.print("Execute the following command to retrieve the credentials:")
        vault_command = self.get_vault_command()
        self.print_code(f"{vault_command} | jq .data.vault")

        input("Press Enter when you have execute the command...")

        for export_key, export_value in self.extra_env.items():
            export_value = os.environ.get(export_key, export_value)
            while True:
                export_value = Prompt.ask(
                    f"Value for: {export_key}", default=export_value
                )
                if export_value:
                    self.extra_env[export_key] = export_value
                    break

        shell_rc = self.get_shell_rc()
        write = Confirm.ask(
            f"You you wish to save the exports to {shell_rc}?", default=True
        )
        if write:
            for k, v in self.extra_env.items():
                self.persist_export(k, v)
            self.print("Exports have been written.")

        # input("Press Enter to continue...")
        from carta_client.deploy import VaultSecretClient

        try:
            vault = VaultSecretClient(
                self.deployment,
                self.extra_env["VAULT_ROLE_ID"],
                self.extra_env["VAULT_SECRET_ID"],
            )

            # inspect(vault, methods=True)
            self.helm_values = vault.read_secret(
                f"/secret/deployments/{self.deployment}/helm-values"
            )
            self.carta_config = vault.read_secret(
                f"/secret/deployments/{self.deployment}/carta-configuration"
            )
        except Exception as e:
            self.print(
                f"Failed to connect to vault with the provided VAULT_ROLE_ID and VAULT_SECRET_ID: {e}"
            )
            self.exports()

    def release(self):
        self.console.clear()
        self.console.rule("Deploy")
        version = self.helm_values["docker_tag"]
        version = Prompt.ask("Version to be deployed", default=version)

        table = Table(show_footer=False)
        table.add_column("deployment")
        table.add_column("version")
        table.add_row(self.deployment, version, style=self.style)
        self.console.print(table, justify="center")
        correct = Confirm.ask(f"Is this correct?", default=True)
        if not correct:
            return self.release()

        do_release = Confirm.ask(
            f"Do you wish to bring the deployment up?", default=True
        )
        if do_release:
            release_command = subprocess.run(
                [
                    "carta-client",
                    "release",
                    "deploy",
                    "--deployment",
                    self.deployment,
                    "--version",
                    version,
                    "--pull",
                ],
                env={**self.extra_env, **dict(os.environ)},
            )
            retcode = release_command
            if retcode == 0:
                self.print("Release succeeded.")

    def auth_plugin_install(self):
        self.console.print("Cheking carta-client login")
        from carta_client import Dotfile

        dotfile = Dotfile()
        base_url = dotfile.get_base_url()
        if not base_url:
            base_url = Prompt.ask(
                "Deployment endpoint?", default="https://local.carta.healthcare"
            )
            dotfile.set_base_url(base_url)
            dotfile.save()

        username = dotfile.get_username()
        password = dotfile.get_password()
        if not username or not password:
            self.print("To install plugins, we need to authenticate.")
            username = Prompt.ask("username", default="creator@carta.healthcare")
            password = Prompt.ask("password", password=True, default="****")
            if password == "****" and username == "creator@carta.healthcare":
                password = self.carta_config["default_password"]

        self.print("Attempting to login...")
        login = subprocess.Popen(
            ["carta-client", "login", "--store"], stdin=subprocess.PIPE
        )
        login.communicate(f"{username}\n{password}".encode("utf-8"))
        if login.returncode == 0:
            self.print("Login succeeded!")
        else:
            os.remove(dotfile.store_path)
            self.auth_plugin_install()

        self.console.print("Cheking ssh keys for plugin install")
        ssh_key_path = os.path.join(os.environ["HOME"], ".ssh", "id_rsa")
        if not os.path.exists(ssh_key_path):
            create_key = Confirm.ask(
                f"Could not find a ssh key, would you like to create one at {ssh_key_path}?"
            )
            if create_key:
                cmd = [
                    "ssh-keygen",
                    "-t",
                    "rsa",
                    "-C",
                    self.deployment,
                    "-f",
                    ssh_key_path,
                    "-P",
                    "",
                ]
                create_key_cmd = subprocess.Popen(cmd)
                create_key_cmd.wait()
                with open(ssh_key_path + ".pub", "r") as f:
                    key_contents = f.read()
                    self.print("id_rsa.pub")
                    self.print("")
                    self.print(key_contents)
                    self.print("Adding the key to your bitbucket user:")
                    self.print("1 - Copy the contents of id_rsa.pub")
                    self.print(
                        "2 - Proceed to https://bitbucket.org/account/settings/ssh-keys/"
                    )
                    self.print("3 - Click the [blue] Add key [/blue] button")
                    self.print("4 - Paste the key contents in the Key field")
                    self.print("5 - Save")

    def install_plugin(self, plugin):
        plugin = plugin.replace("_", "-")
        plugin = plugin.replace(" ", "-")
        self.print(f"installing {plugin}")
        subprocess.run(
            ["carta-client", "plugin", "install", "--upload-ssh-key", plugin]
        )

    def ensure_dockerized_is_installed(self):
        proc = subprocess.run(["which", "dockerize"])
        if proc.returncode != 0:
            path = os.path.join(os.environ.get("HOME"), "bin", "dockerize")
            cmds = [
                [
                    "wget",
                    "https://github.com/jwilder/dockerize/releases/download/v0.6.1/dockerize-linux-amd64-v0.6.1.tar.gz",
                ],
                ["tar", "xvzf", "dockerize-linux-amd64-v0.6.1.tar.gz"],
                ["mv", "dockerize", path],
                ["chmod", "755", path],
            ]
            self.print("Installing dockerized.")
            for cmd in cmds:
                subprocess.run(cmd)

    def wait_for_plugin_endpoint(self):
        self.ensure_dockerized_is_installed()
        self.print("Waiting for ocean to become available.")
        cmd = [
            "dockerize",
            "-wait",
            "https://local.carta.healthcare:443/api/ocean/metadata",
            "-timeout",
            "10m",
            "-wait-retry-interval",
            "10s",
        ]
        subprocess.run(cmd)

    def plugins(self):
        self.console.clear()
        self.console.rule("Plugins")

        # wait for deployment to be ready
        self.wait_for_plugin_endpoint()

        self.auth_plugin_install()
        if "plugins" in self.helm_values:
            plugins = self.helm_values["plugins"]
            table = Table(show_footer=False)
            table.add_column("plugin")
            table.add_column("version")
            for plugin in plugins:
                table.add_row(plugin, "master", style=self.style)
            self.console.print(table, justify="center")
            install_plugins = Confirm.ask(
                f"Do you wish to install these plugins?", default=True
            )
            if install_plugins:
                for plugin in plugins:
                    self.install_plugin(plugin)

        more = True
        while more:
            more = Confirm.ask(
                "Would you like to install any more plugins?", default=False
            )
            if more:
                plugin_name = Prompt.ask("Plugin name", default="carta-cardio")
                self.install_plugin(plugin_name)

    def run(self):
        try:
            self.vault()
            self.exports()
            self.release()
            self.plugins()
            self.print("🎉 That's it! 🎉")
        except Exception as e:
            home = os.environ.get("HOME")
            logs_path = os.path.join(home, "deployment-setup.log")
            with open(logs_path, "a") as logs:
                import datetime

                time = datetime.datetime.now()
                logs.write(f"starting logs for {time}\n")
                logs.write(self.console.export_text())
                logs.write(e.__repr__())
                logs.write(f"ending logs for {time}\n")
            print(f"logs saved to {logs_path}")

from carta_client.api.base import BaseInterface
from http.client import responses
from abc import abstractmethod
from dataclasses import dataclass
from typing import Dict
import subprocess
import socket


def all_subclasses(cls):
    return set(cls.__subclasses__()).union(
        [s for c in cls.__subclasses__() for s in all_subclasses(c)]
    )


class CartaDoctor(BaseInterface):
    def check_heartbeat(self):
        base_url = self.base_url
        services = ["ocean", "cartographer", "atlas", "compass"]
        heartbeats = {}
        for service in services:
            response = self.session.get(base_url + f"/api/{service}/status")
            heartbeats[service] = responses[response.status_code]

        return heartbeats


class Doctor(object):
    def __init__(self, base_url=None, jwt=None):
        self.base_url = base_url
        self.jwt = jwt

    def preflight_check(self):
        classes = all_subclasses(PreflightHealthCheck)
        for clazz in classes:
            check = clazz()
            result = check.check()
            name = check.get_name()
            yield (name, result)

    def post_deployment_check(self):
        classes = all_subclasses(PostDeploymentHealthCheck)
        for clazz in classes:
            check = clazz(self.base_url, self.jwt)
            result = check.check()
            name = check.get_name()
            yield (name, result)


@dataclass
class CheckResult:
    result: bool
    context: Dict

    def __bool__(self):
        return self.result


class PreflightHealthCheck(object):
    @abstractmethod
    def check(self):
        raise NotImplementedError()

    @abstractmethod
    def get_name(self):
        raise NotImplementedError()


class PostDeploymentHealthCheck(BaseInterface):
    @abstractmethod
    def check(self):
        raise NotImplementedError()

    @abstractmethod
    def get_name(self):
        raise NotImplementedError()


class HeartbeatHealthCheck(PostDeploymentHealthCheck):
    def get_name(self):
        return "heartbeat"

    def check(self):
        base_url = self.base_url
        services = ["ocean", "cartographer", "atlas", "compass"]
        heartbeats = {}
        for service in services:
            response = self.session.get(base_url + f"/api/{service}/status")
            heartbeats[service] = responses[response.status_code]

        result = all(heartbeats.values())

        return CheckResult(result=result, context=heartbeats)


class BareMachineHealthCheck(PreflightHealthCheck):
    def get_name(self):
        return "bare metal"

    def check(self):
        machine_name = socket.gethostname()
        pre_checks = {}

        # sudo check
        try:
            sudo_ret = (
                subprocess.run(["sudo", "-l"], capture_output=True)
                .stdout.decode("utf-8")
                .split(machine_name)[-1]
            )
            pre_checks["sudo"] = 0 if "ALL" in sudo_ret else 1
        except:
            pre_checks["sudo"] = 1

        # docker ps check
        docker_ret = subprocess.run(["docker", "ps"], capture_output=True)
        pre_checks["docker"] = docker_ret.returncode

        # python install and version check
        python_ret = subprocess.run(["python", "-V"], capture_output=True)
        pre_checks["python_install"] = python_ret.returncode

        python_version = python_ret.stdout.decode("utf-8").split(" ")[1]
        pre_checks["python_version"] = 0 if python_version.find("2") != 0 else 1

        result = not (any(pre_checks.values()))
        return CheckResult(result=result, context=pre_checks)

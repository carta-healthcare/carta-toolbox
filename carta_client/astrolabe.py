from urllib.parse import urlparse


def astrolabe_connect(api_key, base_url=None, username=None):
    import trino

    if base_url is None:
        from carta_client import Dotfile

        dotfile = Dotfile()
        base_url = dotfile.get_base_url()

    base_url_parsed = urlparse(base_url)

    if username is None:
        from carta_client import IdentityInterface

        identity_interface = IdentityInterface(base_url, dotfile.get_access_token())
        response = identity_interface.check_permissions([])
        username = f"{response['data']['user']['customer_id']}/{response['data']['user']['username']}"

    return trino.dbapi.connect(
        host=base_url_parsed.hostname,
        port=8086,
        user=username,
        auth=trino.auth.BasicAuthentication(username, api_key),
        http_scheme="https",
    )

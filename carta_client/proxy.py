import os
import subprocess
from carta_client.utils import LOG
import random
import string
import glob
import re
from urllib.parse import urlparse

# /etc/ssl/certs/ca-certificates.crt


# cleanup certificate path
# sudo rm -rf /usr/local/share/ca-certificates/* && sudo update-ca-certificates --fresh


def trust_proxy(options):
    truster = TrustCertificates()

    if options.host:
        truster.add_host(options.host)

    if options.get_os_cabundle:
        print(truster.get_ca_bundle_by_os())
        return

    cabundle_path = options.cabundle_path or None
    if "VAULT_ACCESS_CERTIFICATE_PEM" in os.environ:
        cabundle_path = cabundle_path or os.environ["VAULT_ACCESS_CERTIFICATE_PEM"]

    if "CARTA_DEPLOYMENT_CA_BUNDLE_PEM" in os.environ:
        cabundle_path = cabundle_path or os.environ["CARTA_DEPLOYMENT_CA_BUNDLE_PEM"]

    if cabundle_path:
        LOG.info("Making the custom ca-bundle trust certificates")
    else:
        LOG.info("Making the os trust certificates")

    truster.check_for_untrusted_certs(cabundle_path)


class TrustCertificates:
    def __init__(self):
        self.hosts = [
            "https://vault.carta.healthcare/",
            "https://www.cypress.io/",
            "https://cartacr.azurecr.io",
            "https://cr.carta.healthcare",
            "http://registry.npmjs.org/",
            "https://github.com/",
            "https://pypi.org/",
        ]

    def add_host(self, host):
        self.hosts.append(host)

    def get_ca_bundle_by_os(self):
        os_options = self.get_trust_options_by_os()
        return os_options["cabundle_path"]

    def get_trust_options_by_os(self):
        os_name = self.get_os_name()
        options = {
            "Ubuntu": {
                "cabundle_path": "/etc/ssl/certs/ca-certificates.crt",
                "certificate_path": "/usr/local/share/ca-certificates/",
                "trust_commands": [["sudo", "update-ca-certificates", "--fresh"]],
            },
            "CentOS Linux": {
                "cabundle_path": "/etc/pki/tls/certs/ca-bundle.crt",
                "certificate_path": "/etc/pki/ca-trust/source/anchors/",
                "trust_commands": [
                    ["sudo", "update-ca-trust", "force-enable"],
                    ["sudo", "update-ca-trust", "extract"],
                ],
            },
        }
        options["Red Hat Enterprise Linux Server"] = options["CentOS Linux"]

        if os_name not in options:
            raise ValueError(f"No options configured for trusting {os_name}.")

        return options[os_name]

    def get_os_name(self):
        proc = subprocess.Popen(
            ["grep", "^NAME", "/etc/os-release"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        out, err = proc.communicate()
        if proc.returncode == 0:
            # Ubuntu
            # CentOS Linux
            # Alpine Linux
            try:
                return re.search(r'^NAME="(.*)"\n$', out.decode("utf-8")).group(1)
            except Exception as e:
                raise ValueError("Couldn't get os name.") from e
        else:
            return "Unkown"
            # raise ValueError(err)

    def get_random_string(self, length):
        # Random string with the combination of lower and upper case
        letters = string.ascii_letters
        return "".join(random.choice(letters) for i in range(length))

    def check_for_untrusted_certs(
        self, cabundle_path: str = None, additional_hosts: list = []
    ):
        LOG.info("Checking for untrusted certificates in chain")
        for host in self.hosts + additional_hosts:
            cmd = ["curl", host]
            if cabundle_path:
                cmd = cmd + ["--cacert", cabundle_path]
            LOG.info(f"Connecting to f{host}")
            if (
                not 0
                == subprocess.Popen(
                    cmd, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL
                ).wait()
            ):
                LOG.info("Trusting chain")
                self.trust_host(host, cabundle_path)
            else:
                LOG.info("Nothing to be done")

    def trust_host_append(self, temp_folder, cabundle_path):
        for file in glob.glob(f"{temp_folder}/*.crt", recursive=True):
            with open(cabundle_path, "a") as outfile, open(
                file, "r", encoding="utf-8"
            ) as infile:
                for line in infile:
                    outfile.write(line)

    def trust_host_os(self, temp_folder):
        os_trust_options = self.get_trust_options_by_os()
        trust_certificate_path = os_trust_options["certificate_path"]

        for file in glob.glob(f"{temp_folder}/*.crt", recursive=True):
            # adding to trust path for the OS
            file_components = file.split("/")
            rename_to = (
                os.path.join(trust_certificate_path, file_components[2])
                + file_components[3]
            )
            copy_command = ["sudo", "cp", file, rename_to]
            print(" ".join(copy_command))
            subprocess.call(copy_command)
        commands = os_trust_options["trust_commands"]
        for command in commands:
            subprocess.call(command)

    def download_certs(self, host: str) -> str:
        temp_folder = self.get_random_string(20)
        temp_folder = f"/tmp/cartatmp{temp_folder}"
        subprocess.call(["mkdir", "-p", temp_folder])
        LOG.info(f"Temp folder {temp_folder}")
        LOG.info("Downloading certificate chain")

        url = urlparse(host)
        print(url.netloc)
        cmds = [
            "echo",
            f"openssl s_client -showcerts -verify 5 -connect {url.netloc}:443",
            ["awk", '/BEGIN/,/END/{ if(/BEGIN/){a++}; out="cert"a".crt"; print >out}'],
        ]

        popens = []
        previous = None
        for i, cmd in enumerate(cmds):
            kwargs = {
                "stdout": subprocess.PIPE,
                "stderr": subprocess.DEVNULL,
                "cwd": temp_folder,
            }
            if previous:
                kwargs["stdin"] = previous.stdout

            if not isinstance(cmd, list):
                cmd = cmd.split(" ")

            previous = subprocess.Popen(cmd, **kwargs)
            if i < len(cmds) - 1:
                popens.append(previous)

        for proc in popens:
            proc.stdout.close()

        out, err = previous.communicate()
        if err:
            LOG.error(err)
            raise ValueError(err)

        return temp_folder

    def trust_host(self, host, cabundle_path: str = None):
        temp_folder = self.download_certs(host)

        if cabundle_path:
            self.trust_host_append(temp_folder, cabundle_path)
        else:
            self.trust_host_os(temp_folder)
        # cleaning up
        subprocess.call(["rm", "-rf", temp_folder])

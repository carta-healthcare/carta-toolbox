import os
import subprocess
import sys
from pathlib import Path

from jinja2 import Template
from yaml import dump as yaml_dump

import carta_client
from carta_client.utils import LOG, carta_popen, check_output

from .pypi import PrivatePypiServer


class PluginManager(object):
    def __init__(self, deployment_manager=None):
        self.deployment_manager = deployment_manager
        if self.deployment_manager:
            self.deployment_manager.initialize()
            self.pypi = PrivatePypiServer(self.deployment_manager)

    def get_install_arguments(self):
        try:
            return self.pypi.get_install_arguments()
        except Exception as e:
            LOG.error("Failed fetch private repository token")
            raise e

    def fetch_plugin(self, plugin, version=None, plugins_directory=None):
        """Fetches the specified plugin into the plugin cache."""
        if plugins_directory is None:
            plugins_directory = os.path.join(carta_client.TOOLBOX_ROOT, "plugins")
        if not os.path.exists(plugins_directory):
            os.makedirs(plugins_directory)

        if not os.path.exists(os.path.join(plugins_directory, plugin)):
            # Clone it
            git_url = f"https://bitbucket.org/carta-healthcare/{plugin}"
            git = subprocess.Popen(
                ["git", "clone", git_url, plugin], cwd=plugins_directory
            )
            check_output(git, f"Failed to checkout {plugin} from {git_url} ")

        # Check out the right version
        if version is None:
            version = "master"

        git = subprocess.Popen(["git", "fetch"], cwd=plugins_directory).wait()
        check_output(git, f"Failed to fetch {plugin}")
        git = subprocess.Popen(
            ["git", "checkout", version], cwd=plugins_directory
        ).wait()
        check_output(git, f"Failed to checkout version {version} of {plugin}")

    def create_plugin(self, project_name, project_directory):

        package_name = project_name.replace("-", "_")
        project_name_camel_case = "".join(
            word.title() for word in project_name.split("_")
        )
        project_name_camel_case = "".join(
            word.title() for word in project_name_camel_case.split("-")
        )
        project_package_directory = os.path.join(project_directory, package_name)

        if os.path.exists(project_directory):
            print(
                f"Directory for project {project_name} already exists at {project_directory}"
            )
            sys.exit(1)

        os.makedirs(project_package_directory)
        Path(os.path.join(project_package_directory, "__init__.py")).touch()

        template_dir = os.path.join(os.path.dirname(carta_client.__file__), "templates")
        context = {
            "project_name": project_name,
            "project_name_camel_case": project_name_camel_case,
            "package_name": package_name,
        }
        # Create placeholder plugin file
        with open(os.path.join(template_dir, "plugin.py.tmpl")) as f:
            template = Template(f.read())
            plugin_py = template.render(**context)

        # Create placeholder plugin test
        test_dir = os.path.join(project_package_directory, "tests")
        os.makedirs(test_dir)
        Path(os.path.join(test_dir, "__init__.py")).touch()
        with open(os.path.join(template_dir, "test_plugin.py.tmpl")) as f:
            template = Template(f.read())
            test_plugin_py = template.render(**context)

        # Create placeholder conftest
        with open(os.path.join(template_dir, "conftest.py.tmpl")) as f:
            template = Template(f.read())
            conftest_py = template.render(**context)

        # Create placeholder connectors file
        with open(os.path.join(template_dir, "connectors.py.tmpl")) as f:
            template = Template(f.read())
            connectors_py = template.render(**context)

        # Create placeholder navigator responders file
        with open(os.path.join(template_dir, "navigator_responders.py.tmpl")) as f:
            template = Template(f.read())
            navigator_responders_py = template.render(**context)

        # Create placeholder API file
        with open(os.path.join(template_dir, "rest.py.tmpl")) as f:
            template = Template(f.read())
            rest_py = template.render(**context)

        # Create placeholder cubes file
        with open(os.path.join(template_dir, "cubes.py.tmpl")) as f:
            template = Template(f.read())
            cubes_py = template.render(**context)

        # Create placeholder analyses file
        with open(os.path.join(template_dir, "analyses.py.tmpl")) as f:
            template = Template(f.read())
            analyses_py = template.render(**context)

        # Create placeholder setup file
        with open(os.path.join(template_dir, "setup.py.tmpl")) as f:
            template = Template(f.read())
            setup_py = template.render(**context)

        manifest = {
            "name": project_name,
            "plugins": [f"{package_name}.plugin:{project_name_camel_case}Plugin"],
            "description": "",
        }

        with open(os.path.join(project_directory, "manifest.yml"), "w") as f:
            yaml_dump(manifest, f)

        with open(os.path.join(project_directory, "setup.py"), "w") as f:
            f.write(setup_py)

        with open(os.path.join(project_directory, "VERSION"), "w") as f:
            f.write("0.0.1")

        registry_configuration = {
            "registry_name__registry_version": {
                "revision": "revision_number",
                "boolean_config": True,
            }
        }
        with open(
            os.path.join(project_directory, "registry-configuration.yml"), "w"
        ) as f:
            yaml_dump(registry_configuration, f)

        with open(os.path.join(project_package_directory, "plugin.py"), "w") as f:
            f.write(plugin_py)

        with open(os.path.join(project_package_directory, "connectors.py"), "w") as f:
            f.write(connectors_py)

        with open(os.path.join(project_package_directory, "rest.py"), "w") as f:
            f.write(rest_py)

        with open(
            os.path.join(project_package_directory, "navigator_responders.py"), "w"
        ) as f:
            f.write(navigator_responders_py)

        with open(os.path.join(project_package_directory, "cubes.py"), "w") as f:
            f.write(cubes_py)

        with open(os.path.join(project_package_directory, "analyses.py"), "w") as f:
            f.write(analyses_py)

        with open(
            os.path.join(project_package_directory, "tests", "test_plugin.py"), "w"
        ) as f:
            f.write(test_plugin_py)

        with open(
            os.path.join(project_package_directory, "tests", "conftest.py"), "w"
        ) as f:
            f.write(conftest_py)

        # Initialze Git
        subprocess.Popen(["git", "init"], cwd=project_directory).wait()

        # Setup datasets directory
        datasets_dir = os.path.abspath(os.path.join(project_directory, "datasets"))
        phi_dir = os.path.join(datasets_dir, "phi")
        non_phi_dir = os.path.join(datasets_dir, "non-phi")
        if not os.path.exists(phi_dir):
            os.makedirs(phi_dir)
        if not os.path.exists(non_phi_dir):
            os.makedirs(non_phi_dir)

        for data_dir in [phi_dir, non_phi_dir]:
            old_cwd = os.getcwd()
            os.chdir(data_dir)
            subprocess.Popen(["dvc", "init", "--subdir"]).wait()
            subprocess.Popen(
                ["dvc", "remote", "add", "-d", project_name, f"azure://{project_name}"]
            ).wait()
            os.chdir(old_cwd)

        # Add everything to get, set up remote
        subprocess.Popen(
            [
                "git",
                "remote",
                "add",
                "origin",
                f"git@bitbucket.org:carta-healthcare/{project_name}.git",
            ],
            cwd=project_directory,
        )
        subprocess.Popen(["git", "add", "*"], cwd=project_directory).wait()
        subprocess.Popen(
            ["git", "commit", "-m", f"Initial commit for {project_name}"],
            cwd=project_directory,
        ).wait()


def _get_git_unstaged_status(directory):
    try:
        cmd = f"git -C {directory} status --porcelain"
        values = []
        output = subprocess.check_output(cmd, shell=True).decode("utf-8").strip()
        if output == "":
            return values
        for line in output.split("\n"):
            status, filename = line.strip().split(" ")
            if status == "??":
                continue
            else:
                values.append(filename)
        return values

    except subprocess.CalledProcessError as err:
        if err.returncode == 128:
            # This happens when we a non-git directory is passed in.
            return None
        else:
            raise err


def _get_git_branch(directory):
    try:
        cmd = f"git -C {directory} rev-parse --abbrev-ref HEAD"
        return subprocess.check_output(cmd, shell=True).decode("utf-8").strip()
    except subprocess.CalledProcessError as err:
        if err.returncode == 128:
            # This happens when we a non-git directory is passed in.
            return None
        else:
            raise err


def update_all_local_plugins(options):
    if os.environ["CARTA_ENVIRONMENT"] != "development":
        print(
            "This command must be run in development environments only.",
            file=sys.stderr,
        )
        sys.exit(1)
    results = {
        "updated": [],
        "skipped": [],
        "failed": [],
    }
    plugins_path = "/home/carta-dev/dev/carta/plugins/"
    for directory in os.listdir(plugins_path):
        full_path = os.path.join(plugins_path, directory)
        if not os.path.isdir(full_path):
            if options.verbose:
                LOG.info(f"{directory} is not a directory. skipping.")
            continue
        branch = _get_git_branch(full_path)
        if branch is None:
            if options.verbose:
                LOG.info(f"{directory} is not tracked in git. Skipping.")
            continue
        if options.force:
            if options.verbose:
                LOG.info(f"Updating {directory}")
            try:
                carta_popen(
                    f"cd {full_path} && git fetch && git reset --hard && git checkout master && git pull",
                    shell=True,
                ).wait()
                results["updated"].append(directory)
            except subprocess.CalledProcessError:
                results["failed"].append(directory)
        elif branch != "master":
            LOG.info(f"{directory} is on branch {branch}. skipping")
            results["skipped"].append(directory)
        elif len(_get_git_unstaged_status(full_path)) > 0:
            LOG.info(f"{directory} has uncommitted changes. skipping")
            results["skipped"].append(directory)
        else:
            try:
                carta_popen(f"cd {full_path} && git pull", shell=True).wait()
                results["updated"].append(directory)
            except subprocess.CalledProcessError:
                results["failed"].append(directory)

    LOG.info("Results:")
    LOG.info("-" * 80)
    for k, v in results.items():
        if len(v) == 0:
            continue
        if k == "failed":
            LOG.info(
                f"{len(v)} repo{'s' if len(v) != 1 else ''} failed to update: ({', '.join(v)})"
            )
        else:
            LOG.info(
                f"{len(v)} repo{'s were' if len(v) != 1 else ' was'} {k}: ({', '.join(v)})"
            )

version: "2.2"
services:

  # Carta services
  ocean:
    depends_on:
      - cartadb
      - elasticsearch
      - redis
    image: "cr.carta.healthcare/carta/ocean:${CARTA_DOCKER_TAG}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    restart: "always"
    mem_limit: "${CARTA_OCEAN_MEMORY_LIMIT:-5g}"
    labels:
      # Traefik 1.x
      - "traefik.frontend.rule=PathPrefix:/api"
      - "traefik.port=${CARTA_OCEAN_PORT}"
      - "traefik.enable=true"

      # Traefik 2.x
      - "traefik.http.routers.ocean.rule=PathPrefix(`${CARTA_API_PREFIX}`)"
      - "traefik.http.routers.ocean.tls=true"
      - "traefik.http.routers.ocean.entrypoints=websecure"

      - "traefik.http.routers.ocean-swagger.rule=PathPrefix(`/swaggerui`)"
      - "traefik.http.routers.ocean-swagger.tls=true"
      - "traefik.http.routers.ocean-swagger.entrypoints=websecure"

      - "traefik.http.routers.ocean-swagger-doc.rule=Path(`${CARTA_API_PREFIX}/{api:[A-Za-z-_]+}/`)"
      - "traefik.http.routers.ocean-swagger-doc.tls=true"
      - "traefik.http.routers.ocean-swagger-doc.entrypoints=websecure"

      - "traefik.http.routers.ocean-http.rule=PathPrefix(`${CARTA_API_PREFIX}`)"
      - "traefik.http.routers.ocean-http.middlewares=https-redirect@file"
      - "traefik.http.routers.ocean-http.entrypoints=web"

      - "traefik.http.services.ocean.loadbalancer.server.port=${CARTA_OCEAN_PORT}"

    environment:
      PGPASSWORD: ${CARTA_CARTADB_POSTGRES_PASSWORD}
    env_file: "${COMPOSE_ENV_FILE}"
    ports:
      - "${CARTA_OCEAN_PORT}"
    volumes:
      - "carta-plugins:/carta/plugins" # Default volume for plugins
      - "carta-datasets:/carta/datasets"
      - "carta-cache:/carta/.carta-cache"
      - "carta-contents:/carta/contents"
      - "carta-downloads:/carta/downloads"
      - elastic-umls:/carta/datasets/penguinsearch/umls_snapshots
      - elastic-notes:/carta/datasets/penguinsearch/notes_snapshots
    networks:
      default:
        aliases:
          - ocean.internal.carta.healthcare

  cartographer-worker:
    # this worker will run with all functionalities (including OCR and NLP)
    image: "cr.carta.healthcare/carta/cartographer-worker:${CARTA_DOCKER_TAG}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_CARTOGRAPHER_WORKER_MEMORY_LIMIT:-6.5g}"
    shm_size: "${CARTA_CARTOGRAPHER_WORKER_SHARED_MEMORY:-1g}"
    restart: "always"
    command: "entrypoint.sh worker"
    # labels:
    # - "traefik.enable=false"
    # - "traefik.http.middlewares.cartographer-websocket-stripprefix.stripprefix.prefixes=${CARTA_CARTOGRAPHER_BASE_URL}/websocket"
    # - "traefik.http.routers.cartographer-websocket.service=cartographer-websocket"
    # - "traefik.http.routers.cartographer-websocket.middlewares=cartographer-websocket-stripprefix"
    # - "traefik.http.routers.cartographer-websocket.rule=PathPrefix(`${CARTA_CARTOGRAPHER_BASE_URL}/websocket`)"
    # - "traefik.http.routers.cartographer-websocket.tls=true"
    # - "traefik.http.routers.cartographer-websocket.entrypoints=websecure"
    # - "traefik.http.services.cartographer-websocket.loadbalancer.server.port=${CARTA_CARTOGRAPHER_WEBSOCKET_PORT}"
    env_file: "${COMPOSE_ENV_FILE}"
    volumes:
      - "carta-plugins:/carta/plugins" # Default volume for plugins
      - "carta-datasets:/carta/datasets"
      - "carta-cache:/carta/.carta-cache"
      - "carta-contents:/carta/contents"
      - "carta-downloads:/carta/downloads"
      - elastic-umls:/carta/datasets/penguinsearch/umls_snapshots
      - elastic-notes:/carta/datasets/penguinsearch/notes_snapshots
    ports:
      - ${CARTA_CARTOGRAPHER_WEBSOCKET_PORT}
    networks:
      default:
        aliases:
          - cartographer-worker.internal.carta.healthcare

  babelfish-worker:
    # this worker will process all except NLP and OCR requests  
    image: "cr.carta.healthcare/carta/cartographer-worker:${CARTA_DOCKER_TAG}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_CARTOGRAPHER_FAUST_MEMORY_LIMIT:-5g}"
    shm_size: "${CARTA_CARTOGRAPHER_FAUST_SHARED_MEMORY:-1g}"
    restart: "always"
    command: "entrypoint.sh slim"
    # labels:
    # - "traefik.enable=false"
    # - "traefik.http.middlewares.cartographer-websocket-stripprefix.stripprefix.prefixes=${CARTA_CARTOGRAPHER_BASE_URL}/websocket"
    # - "traefik.http.routers.cartographer-websocket.service=cartographer-websocket"
    # - "traefik.http.routers.cartographer-websocket.middlewares=cartographer-websocket-stripprefix"
    # - "traefik.http.routers.cartographer-websocket.rule=PathPrefix(`${CARTA_CARTOGRAPHER_BASE_URL}/websocket`)"
    # - "traefik.http.routers.cartographer-websocket.tls=true"
    # - "traefik.http.routers.cartographer-websocket.entrypoints=websecure"
    # - "traefik.http.services.cartographer-websocket.loadbalancer.server.port=${CARTA_CARTOGRAPHER_WEBSOCKET_PORT}"
    env_file: "${COMPOSE_ENV_FILE}"
    volumes:
      - "carta-plugins:/carta/plugins" # Default volume for plugins
      - "carta-datasets:/carta/datasets"
      - "carta-contents:/carta/contents"
      - "carta-downloads:/carta/downloads"
    ports:
      - ${CARTA_CARTOGRAPHER_WEBSOCKET_PORT}
    networks:
      default:
        aliases:
          - cartographer-worker.internal.carta.healthcare

  babelfish-ocr:
    # this worker will process OCR requests  
    image: "cr.carta.healthcare/carta/cartographer-worker:${CARTA_DOCKER_TAG}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_CARTOGRAPHER_FAUST_MEMORY_LIMIT:-5g}"
    shm_size: "${CARTA_CARTOGRAPHER_FAUST_SHARED_MEMORY:-1g}"
    restart: "always"
    command: "entrypoint.sh ocr"
    # labels:
    # - "traefik.enable=false"
    # - "traefik.http.middlewares.cartographer-websocket-stripprefix.stripprefix.prefixes=${CARTA_CARTOGRAPHER_BASE_URL}/websocket"
    # - "traefik.http.routers.cartographer-websocket.service=cartographer-websocket"
    # - "traefik.http.routers.cartographer-websocket.middlewares=cartographer-websocket-stripprefix"
    # - "traefik.http.routers.cartographer-websocket.rule=PathPrefix(`${CARTA_CARTOGRAPHER_BASE_URL}/websocket`)"
    # - "traefik.http.routers.cartographer-websocket.tls=true"
    # - "traefik.http.routers.cartographer-websocket.entrypoints=websecure"
    # - "traefik.http.services.cartographer-websocket.loadbalancer.server.port=${CARTA_CARTOGRAPHER_WEBSOCKET_PORT}"
    env_file: "${COMPOSE_ENV_FILE}"
    volumes:
      - "carta-plugins:/carta/plugins" # Default volume for plugins
      - "carta-datasets:/carta/datasets"
      - "carta-contents:/carta/contents"
      - "carta-downloads:/carta/downloads"
    ports:
      - ${CARTA_CARTOGRAPHER_WEBSOCKET_PORT}
    networks:
      default:
        aliases:
          - cartographer-worker.internal.carta.healthcare

  babelfish-nlp:
    # this worker will serve NLP requests.
    image: "cr.carta.healthcare/carta/cartographer-worker:${CARTA_DOCKER_TAG}"
    mem_limit: "${CARTA_CARTOGRAPHER_FAUST_MEMORY_LIMIT:-5g}"
    shm_size: "${CARTA_CARTOGRAPHER_FAUST_SHARED_MEMORY:-1g}"
    restart: "always"
    command: "entrypoint.sh nlp"
    env_file: "${COMPOSE_ENV_FILE}"
    volumes:
      - "carta-plugins:/carta/plugins" # Default volume for plugins
      - "carta-datasets:/carta/datasets"
      - "carta-contents:/carta/contents"
      - "carta-downloads:/carta/downloads"
    ports:
      - ${CARTA_CARTOGRAPHER_WEBSOCKET_PORT}
    networks:
      default:
        aliases:
          - cartographer-worker.internal.carta.healthcare

  cartographer-websocket:
    image: "cr.carta.healthcare/carta/cartographer-worker:${CARTA_DOCKER_TAG}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_CARTOGRAPHER_WORKER_MEMORY_LIMIT:-5g}"
    shm_size: "${CARTA_CARTOGRAPHER_WORKER_SHARED_MEMORY:-1g}"
    restart: "always"
    command: "entrypoint.sh smee-websocket"
    labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.cartographer-websocket-stripprefix.stripprefix.prefixes=${CARTA_CARTOGRAPHER_BASE_URL}/websocket"
      - "traefik.http.routers.cartographer-websocket.service=cartographer-websocket"
      - "traefik.http.routers.cartographer-websocket.middlewares=cartographer-websocket-stripprefix"
      - "traefik.http.routers.cartographer-websocket.rule=PathPrefix(`${CARTA_CARTOGRAPHER_BASE_URL}/websocket`)"
      - "traefik.http.routers.cartographer-websocket.tls=true"
      - "traefik.http.routers.cartographer-websocket.entrypoints=websecure"
      - "traefik.http.services.cartographer-websocket.loadbalancer.server.port=${CARTA_CARTOGRAPHER_WEBSOCKET_PORT}"
    env_file: "${COMPOSE_ENV_FILE}"
    volumes:
      - "carta-plugins:/carta/plugins" # Default volume for plugins
      - "carta-datasets:/carta/datasets"
      - "carta-contents:/carta/contents"
      - "carta-downloads:/carta/downloads"
    ports:
      - ${CARTA_CARTOGRAPHER_WEBSOCKET_PORT}
    networks:
      default:
        aliases:
          - cartographer-websocket.internal.carta.healthcare

  cartographer-cron:
    image: "cr.carta.healthcare/carta/cartographer-worker:${CARTA_DOCKER_TAG}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_CARTOGRAPHER_WORKER_MEMORY_LIMIT:-5g}"
    shm_size: "${CARTA_CARTOGRAPHER_WORKER_SHARED_MEMORY:-1g}"
    restart: "always"
    command: "entrypoint.sh cron"
    env_file: "${COMPOSE_ENV_FILE}"
    volumes:
      - "carta-plugins:/carta/plugins" # Default volume for plugins
      - "carta-datasets:/carta/datasets"
      - "carta-contents:/carta/contents"
      - "carta-downloads:/carta/downloads"
    networks:
      default:
        aliases:
          - cartographer-cron.internal.carta.healthcare

  mustang:
    image: "cr.carta.healthcare/carta/mustang:${CARTA_DOCKER_TAG}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_MUSTANG_MEMORY_LIMIT:-5g}"
    restart: "always"
    labels:
      # Traefik 1.x
      - "traefik.frontend.rule=PathPrefix:${CARTA_MUSTANG_BASE_URL}"
      - "traefik.enable=true"
      - "traefik.port=${CARTA_MUSTANG_PORT}"

      # Traefik 2.x
      - "traefik.http.routers.mustang.rule=PathPrefix(`${CARTA_MUSTANG_BASE_URL}`)"
      - "traefik.http.routers.mustang.tls=true"
      - "traefik.http.routers.mustang.entrypoints=websecure"
      - "traefik.http.services.mustang.loadbalancer.server.port=${CARTA_MUSTANG_PORT}"
      - "traefik.http.routers.mustang-http.middlewares=https-redirect@file"
      - "traefik.http.routers.mustang-http.rule=PathPrefix(`${CARTA_MUSTANG_BASE_URL}`)"
      - "traefik.http.routers.mustang-http.entrypoints=web"
    ports:
      - "${CARTA_MUSTANG_PORT}"
    env_file: "${COMPOSE_ENV_FILE}"
    volumes:
      - "carta-plugins:/carta/plugins"
      - "carta-contents:/carta/contents"
      - "carta-downloads:/carta/downloads"
    networks:
      default:
        aliases:
          - mustang.internal.carta.healthcare

  navigator:
    depends_on:
      - cartadb
      - elasticsearch
      - redis
    image: "cr.carta.healthcare/carta/navigator:${CARTA_DOCKER_TAG}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    restart: "always"
    command: "entrypoint.sh prod"
    mem_limit: "${CARTA_NAVIGATOR_MEMORY_LIMIT:-5g}"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.navigator.rule=PathPrefix(`${CARTA_NAVIGATOR_BASE_PATH:-/navigator}`)"
      - "traefik.http.routers.navigator.tls=true"
      - "traefik.http.routers.navigator.entrypoints=websecure"
      - "traefik.http.routers.navigator.middlewares=navigator-trailingslash"
      - "traefik.http.middlewares.navigator-trailingslash.redirectregex.regex=^(.*)${CARTA_NAVIGATOR_BASE_PATH:-/navigator}$$"
      - "traefik.http.middlewares.navigator-trailingslash.redirectregex.replacement=$${1}${CARTA_NAVIGATOR_BASE_PATH:-/navigator}/"
      - "traefik.http.middlewares.navigator-trailingslash.redirectregex.permanent=true"

      - "traefik.http.routers.navigator-http.rule=PathPrefix(`${CARTA_NAVIGATOR_BASE_PATH:-/navigator}`)"
      - "traefik.http.routers.navigator-http.middlewares=https-redirect@file"
      - "traefik.http.routers.navigator-http.entrypoints=web"
      - "traefik.http.services.navigator.loadbalancer.server.port=${CARTA_NAVIGATOR_PORT:-3001}"

    environment:
      PGPASSWORD: ${CARTA_CARTADB_POSTGRES_PASSWORD}
    env_file: "${COMPOSE_ENV_FILE}"
    ports:
      - "${CARTA_NAVIGATOR_PORT}"
    volumes:
      - "carta-plugins:/carta/plugins" # Default volume for plugins
      - "carta-datasets:/carta/datasets"
      - "carta-cache:/carta/.carta-cache"
      - "carta-contents:/carta/contents"
      - "carta-downloads:/carta/downloads"
      - elastic-umls:/carta/datasets/penguinsearch/umls_snapshots
      - elastic-notes:/carta/datasets/penguinsearch/notes_snapshots

    networks:
      default:
        aliases:
          - navigator.internal.carta.healthcare

  backup:
    image: "cr.carta.healthcare/carta/backup:${CARTA_DOCKER_TAG}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_BACKUP_MEMORY_LIMIT:-2g}"
    env_file: "${COMPOSE_ENV_FILE}"
    restart: "always"
    volumes:
      - "./backup/:/data/backup"
    networks:
      default:
        aliases:
          - backup.internal.carta.healthcare

  # Platform Services
  load-balancer:
    image: "cr.carta.healthcare/carta/load-balancer:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_LOAD_BALANCER_MEMORY_LIMIT:-2g}"
    restart: "always"
    command: "entrypoint.sh"
    env_file: "${COMPOSE_ENV_FILE}"
    ports:
      - "80:80" # The HTTP port
      - "443:443" # The HTTPS port
      - "8080:8080" # The Web UI
      - "127.0.0.1:1443:1443" # The local-only HTTPS port
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock # So that Traefik can listen to the Docker events
      #- "./deployment/docker/traefik/traefik.toml:/traefik.toml"
    networks:
      default:
        aliases:
          - load-balancer.internal.carta.healthcare

  minio:
    image: "cr.carta.healthcare/carta/minio:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_MINIO_MEMORY_LIMIT:-500m}"
    restart: "always"
    command: "server /var/lib/minio/data"
    env_file: "${COMPOSE_ENV_FILE}"
    environment:
      MINIO_ACCESS_KEY: "${CARTA_OBJECT_STORE_ACCESS_KEY}"
      MINIO_SECRET_KEY: "${CARTA_OBJECT_STORE_SECRET_KEY}"
    ports:
      - "9000"
    volumes:
      - "/var/lib/minio/data"
    networks:
      default:
        aliases:
          - minio.internal.carta.healthcare

  cartadb:
    image: "cr.carta.healthcare/carta/postgres:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_CARTADB_MEMORY_LIMIT:-3g}"
    restart: "always"
    env_file: "${COMPOSE_ENV_FILE}"
    command: postgres -c shared_buffers=${CARTA_ATLASDB_POSTGRES_SHARED_BUFFERS:-512MB} -c max_connections=${CARTA_ATLASDB_POSTGRES_MAX_CONNECTIONS:-110} -c 'config_file=/etc/postgresql/postgresql.conf'
    environment:
      POSTGRES_USER: ${CARTA_ATLASDB_POSTGRES_USERNAME}
      POSTGRES_PASSWORD: ${CARTA_ATLASDB_POSTGRES_PASSWORD}
    ports:
      - "${CARTA_ATLASDB_POSTGRES_PORT}"
      - "127.0.0.1:${CARTA_ATLASDB_POSTGRES_PORT}:${CARTA_ATLASDB_POSTGRES_PORT}"
    volumes:
      - "cartadb-data:/var/lib/postgresql/data"
    networks:
      default:
        aliases:
          - cartadb.internal.carta.healthcare
  rabbitmq:
    image: "cr.carta.healthcare/carta/rabbitmq:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_RABBITMQ_MEMORY_LIMIT:-1g}"
    restart: "always"
    env_file: "${COMPOSE_ENV_FILE}"
    environment:
      RABBITMQ_SERVER_ADDITIONAL_ERL_ARGS: "-rabbit consumer_timeout 3600000"
    ports:
      - "${CARTA_RABBITMQ_PORT:-5672}"
    networks:
      default:
        aliases:
          - rabbitmq.internal.carta.healthcare
  elasticsearch:
    image: "cr.carta.healthcare/carta/elasticsearch:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_ELASTICSEARCH_MEMORY_LIMIT:-2g}"
    restart: "always"
    env_file: "${COMPOSE_ENV_FILE}"
    labels:
      - "traefik.enable=false"
    environment:
      discovery.type: "single-node"
      ES_JAVA_OPTS: "-Xms${CARTA_ELASTICSEARCH_JVM_MEMORY_LIMIT:-1024m} -Xmx${CARTA_ELASTICSEARCH_JVM_MEMORY_LIMIT:-1024m}"
      http.cors.enabled: "true"
      http.cors.allow-origin: "http://localhost:1358,http://127.0.0.1:1358"
      http.cors.allow-headers: "X-Requested-With,X-Auth-Token,Content-Type,Content-Length,Authorization"
      http.cors.allow-credentials: "true"
    expose:
      - "9200"
      - "9300"
    volumes:
      - elastic-umls:/umls_snapshots
      - elastic-notes:/notes_snapshots
      # - ./carta_client/elasticsearch_config.yml:/usr/share/elasticsearch/config/elasticsearch.yml
      - elastic-data:/usr/share/elasticsearch/data
    networks:
      default:
        aliases:
          - elasticsearch.internal.carta.healthcare

  kibana:
    image: "cr.carta.healthcare/carta/kibana:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_KIBANA_MEMORY_LIMIT:-2g}"
    labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.usermgmt-auth.forwardauth.address=${CARTA_USERMGMT_INTERNAL_BASE_URL}/account/jwt/authorize?ide&redirect-to-login"

      # Theia
      - "traefik.http.routers.kibana.service=kibana"
      #      - "traefik.http.middlewares.kibana-stripprefix.stripprefix.prefixes=/kibana"

      - "traefik.http.routers.kibana.middlewares=usermgmt-auth"
      - "traefik.http.routers.kibana.rule=PathPrefix(`/kibana`)"
      - "traefik.http.routers.kibana.tls=true"
      - "traefik.http.routers.kibana.entrypoints=websecure"
      - "traefik.http.services.kibana.loadbalancer.server.port=5601"

      # HTTP -> HTTPS redirects
      - "traefik.http.routers.kibana-http.service=kibana-http"
      - "traefik.http.routers.kibana-http.middlewares=https-redirect@file"
      - "traefik.http.routers.kibana-http.rule=PathPrefix(`/kibana`)"
      - "traefik.http.routers.kibana-http.entrypoints=web"
      - "traefik.http.services.kibana-http.loadbalancer.server.port=5601"

    environment:
      SERVER_NAME: carta-elasticsearch
      ELASTICSEARCH_HOSTS: ${CARTA_ELASTICSEARCH_INTERNAL_URL}
      SERVER_BASEPATH: "/kibana"
      SERVER_REWRITEBASEPATH: "true"

    ports:
      - "5601"
    networks:
      default:
        aliases:
          - kibana.internal.carta.healthcare

  fluentd:
    image: "cr.carta.healthcare/carta/fluent-bit:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_FLUENTD_MEMORY_LIMIT:-500m}"
    restart: "always"
    env_file: "${COMPOSE_ENV_FILE}"
    labels:
      - "traefik.enable=false"
    ports:
      - "24224"
    volumes:
      - "/fluentd/log"
    networks:
      default:
        aliases:
          - fluentd.internal.carta.healthcare

  redis:
    image: "cr.carta.healthcare/carta/redis:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_REDIS_MEMORY_LIMIT:-500m}"
    restart: "always"
    env_file: "${COMPOSE_ENV_FILE}"
    labels:
      - "traefik.enable=false"
    expose:
      - "6379"
    volumes: []
    networks:
      default:
        aliases:
          - redis.internal.carta.healthcare

  prometheus:
    image: "cr.carta.healthcare/carta/prometheus:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_PROMETHEUS_MEMORY_LIMIT:-1g}"
    env_file: "${COMPOSE_ENV_FILE}"
    labels:
      # Traefik 1.x
      - "traefik.frontend.rule=PathPrefix:/prometheus"
      - "traefik.port=9090"
      - "traefik.enable=true"

      # Traefik 2.x
      - "traefik.http.routers.prometheus.rule=PathPrefix(`/prometheus`)"
      - "traefik.http.routers.prometheus.tls=true"
      - "traefik.http.routers.prometheus.entrypoints=websecure"
      - "traefik.http.services.prometheus.loadbalancer.server.port=9090"
      - "traefik.http.routers.prometheus-http.middlewares=https-redirect@file"
      - "traefik.http.routers.prometheus-http.rule=PathPrefix(`/prometheus`)"
      - "traefik.http.routers.prometheus-http.entrypoints=web"
    restart: "always"
    volumes:
      - "prometheus-data:/prometheus"
    networks:
      default:
        aliases:
          - prometheus.internal.carta.healthcare

  node-exporter:
    mem_limit: "${CARTA_NODE_EXPORTER_MEMORY_LIMIT:-500m}"
    image: "cr.carta.healthcare/carta/node-exporter:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    env_file: "${COMPOSE_ENV_FILE}"
    volumes:
      - "/proc:/host/proc:ro"
      - "/sys:/host/sys:ro"
      - "/:/rootfs:ro"
      - "/tmp/exporter:/tmp/exporter:ro"
    command: "--collector.textfile.directory=/tmp/exporter --web.listen-address=:${CARTA_NODE_EXPORTER_PORT} --path.procfs=/host/proc --path.sysfs=/host/sys --collector.filesystem.ignored-mount-points ^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs)($$|/)"
    ports:
      - ${CARTA_NODE_EXPORTER_PORT:-9100}
    restart: always
    networks:
      default:
        aliases:
          - node-exporter.internal.carta.healthcare

  blackbox-exporter:
    mem_limit: "${CARTA_BLACKBOX_EXPORTER_MEMORY_LIMIT:-250m}"
    image: "cr.carta.healthcare/carta/blackbox-exporter:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    env_file: "${COMPOSE_ENV_FILE}"
    command: "--web.listen-address=:${CARTA_BLACKBOX_EXPORTER_PORT} --config.file=/carta/plugins/carta-platform/docker/blackbox_exporter/blackbox.yml"
    ports:
      - ${CARTA_BLACKBOX_EXPORTER_PORT:-9115}
    restart: always
    networks:
      default:
        aliases:
          - blackbox-exporter.internal.carta.healthcare

  cadvisor:
    mem_limit: "${CARTA_CADVISOR_MEMORY_LIMIT:-500m}"
    image: "cr.carta.healthcare/carta/cadvisor:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "100mb"
        max-file: "3"
    command: "--port=${CARTA_CADVISOR_PORT} --enable_load_reader=true --housekeeping_interval=30s"
    env_file: "${COMPOSE_ENV_FILE}"
    volumes:
      - "/:/rootfs:ro"
      - "/var/run:/var/run:rw"
      - "/sys:/sys:ro"
      - "/var/lib/docker/:/var/lib/docker:ro"
      - "/dev/disk:/dev/disk:ro"
    ports:
      - ${CARTA_CADVISOR_PORT:-8001}
    restart: always
    networks:
      default:
        aliases:
          - cadvisor.internal.carta.healthcare

  grafana:
    mem_limit: "${CARTA_GRAFANA_MEMORY_LIMIT:-2g}"
    image: "cr.carta.healthcare/carta/grafana:${CARTA_PLATFORM_VERSION}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    env_file: "${COMPOSE_ENV_FILE}"
    volumes:
      - "carta-plugins:/carta/plugins/carta-plugins"
      - "carta-contents:/carta/contents"
      - "carta-downloads:/carta/downloads"
      - "grafana-data:/var/lib/grafana"
    labels:
      - "traefik.enable=true"

      # Traefik 2.x
      - "traefik.http.routers.grafana.rule=PathPrefix(`${CARTA_GRAFANA_BASE_URL:-/grafana}`)"
      - "traefik.http.routers.grafana.tls=true"
      - "traefik.http.routers.grafana.entrypoints=websecure"
      - "traefik.http.services.grafana.loadbalancer.server.port=3002"
      - "traefik.http.middlewares.grafana-forwardauth.forwardauth.address=${CARTA_USERMGMT_INTERNAL_BASE_URL}/account/jwt/authorize?grafana&redirect-to-login"
      - "traefik.http.middlewares.grafana-forwardauth.forwardauth.authResponseHeaders=X-WEBAUTH-USER, X-WEBAUTH-NAME, X-WEBAUTH-GROUPS"
      - "traefik.http.routers.grafana.middlewares=grafana-forwardauth"

      # HTTP -> HTTPS Redirect
      - "traefik.http.routers.grafana-http.middlewares=https-redirect@file"
      - "traefik.http.routers.grafana-http.rule=PathPrefix(`${CARTA_GRAFANA_BASE_URL:-/grafana}`)"
      - "traefik.http.routers.grafana-http.entrypoints=web"

    restart: "always"
    environment:
      GF_SECURITY_ADMIN_USER: "creator@carta.healthcare"
      GF_AUTH_PROXY_ENABLED: "true"
      GF_AUTH_PROXY_HEADER_NAME: "X-WEBAUTH-USER"
      GF_AUTH_PROXY_HEADERS: "Name:X-WEBAUTH-NAME Email:X-WEBAUTH-EMAIL Groups:X-WEBAUTH-GROUPS"
      GF_SERVER_HTTP_PORT: "${CARTA_GRAFANA_PORT}"
      GF_SERVER_ROOT_URL: "${CARTA_GRAFANA_BASE_URL}"
      GF_SERVER_SERVE_FROM_SUB_PATH: "true"
      GF_DATABASE_TYPE: "postgres"
      GF_DATABASE_HOST: "${CARTA_ATLASDB_POSTGRES_HOSTNAME}:${CARTA_ATLASDB_POSTGRES_PORT}"
      GF_DATABASE_USER: "${CARTA_ATLASDB_POSTGRES_USERNAME}"
      GF_DATABASE_SSL_MODE: "${CARTA_POSTGRES_SSL_MODE}"
      GF_DATABASE_PASSWORD: "${CARTA_ATLASDB_POSTGRES_PASSWORD}"
      GF_DATABASE_NAME: "grafana"
    networks:
      default:
        aliases:
          - grafana.internal.carta.healthcare

  semaphore:
    image: "cr.carta.healthcare/carta/semaphore:${CARTA_DOCKER_TAG}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_SEMAPHORE_MEMORY_LIMIT:-1000m}"
    env_file: "${COMPOSE_ENV_FILE}"
    restart: "always"
    command: [ "semaphore", "server" ]
    labels:
      # Traefik 2.x
      - "traefik.enable=true"
      - "traefik.http.routers.semaphore.rule=PathPrefix(`${CARTA_SEMAPHORE_BASE_URL:-/semaphore}`)"
      - "traefik.http.routers.semaphore.entrypoints=websecure"
      - "traefik.http.routers.semaphore.tls=true"

      # Configure authentication
      - "traefik.http.middlewares.semaphore-auth.forwardauth.address=${CARTA_USERMGMT_INTERNAL_BASE_URL}/account/jwt/authorize?semaphore&redirect-to-login"
      - "traefik.http.routers.semaphore.middlewares=semaphore-auth"

      # Define the service parameters
      # - "traefik.http.services.semaphore.loadbalancer.server.scheme=https"
      - "traefik.http.services.semaphore.loadbalancer.server.port=9400"

      # HTTP -> HTTPS redirect
      - "traefik.http.routers.semaphore-http.middlewares=https-redirect@file"
      - "traefik.http.routers.semaphore-http.rule=PathPrefix(`${CARTA_SEMAPHORE_BASE_URL:-/semaphore}`)"
      - "traefik.http.routers.semaphore-http.entrypoints=web"
    environment:
      CARTA_DEPLOYMENT_TARGET: docker-compose
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:rw
      - semaphore-data:/home/semaphore:rw
    ports:
      - "9400"
    networks:
      default:
        aliases:
          - semaphore.internal.carta.healthcare

  mirth:
    image: "cr.carta.healthcare/carta/mirth:${CARTA_DOCKER_TAG}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_MIRTH_MEMORY_LIMIT:-1500m}"
    env_file: "${COMPOSE_ENV_FILE}"
    restart: "always"
    #entrypoint: ""
    #command: "/entrypoint.sh ./mcserver"
    labels:
      # Traefik 1.x
      - "traefik.enable=true"
      - "traefik.port=8081"
      - "traefik.frontend.rule=PathPrefix:${CARTA_MIRTH_BASE_URL}"

      # Traefik 2.x
      - "traefik.http.routers.mirth.rule=PathPrefix(`${CARTA_MIRTH_BASE_URL}`)"
      - "traefik.http.routers.mirth.entrypoints=websecure-local"
      - "traefik.http.routers.mirth.tls=true"
      - "traefik.http.services.mirth.loadbalancer.server.scheme=https"
      - "traefik.http.services.mirth.loadbalancer.server.port=8443"
      - "traefik.http.routers.mirth-http.middlewares=https-redirect@file"
      - "traefik.http.routers.mirth-http.rule=PathPrefix(`${CARTA_MIRTH_BASE_URL}`)"
      - "traefik.http.routers.mirth-http.entrypoints=web"
    environment:
      DATABASE: postgres
      DATABASE_URL: jdbc:postgresql://${CARTA_ATLASDB_POSTGRES_HOSTNAME}:5432/mirth?sslmode=${CARTA_POSTGRES_SSL_MODE}
      DATABASE_USERNAME: "${CARTA_ATLASDB_POSTGRES_USERNAME}"
      DATABASE_PASSWORD: "${CARTA_ATLASDB_POSTGRES_PASSWORD}"
      KEYSTORE_STOREPASS: password
      KEYSTORE_KEYPASS: password
      _MP_HTTP_PORT: 8081
      _MP_HTTPS_PORT: 8443
      _MP_HTTPS_SERVER_PROTOCOLS: 'TLSv1.3,TLSv1.2'
      _MP_HTTP_CONTEXTPATH: ${CARTA_MIRTH_BASE_URL}
      _MP_SERVER_INCLUDECUSTOMLIB: "true"
    ports:
      - "8443"
    networks:
      default:
        aliases:
          - mirth.internal.carta.healthcare

  astrolabe:
    # image: "cr.carta.healthcare/carta/trino:${CARTA_PLATFORM_VERSION}"
    image: "cr.carta.healthcare/carta/astrolabe:${CARTA_DOCKER_TAG}"
    logging:
      options:
        max-size: "333mb"
        max-file: "3"
    mem_limit: "${CARTA_ASTROLABE_MEMORY_LIMIT:-4000m}"
    env_file: "${COMPOSE_ENV_FILE}"
    restart: "always"
    #entrypoint: ""
    #command: "/entrypoint.sh ./mcserver"
    #    labels:
    #      # Traefik 1.x
    #      - "traefik.enable=true"
    #      - "traefik.port=8081"
    #      - "traefik.frontend.rule=PathPrefix:${CARTA_MIRTH_BASE_URL}"
    #
    #      # Traefik 2.x
    #      - "traefik.http.routers.mirth.rule=PathPrefix(`${CARTA_MIRTH_BASE_URL}`)"
    #      - "traefik.http.routers.mirth.entrypoints=websecure-local"
    #      - "traefik.http.routers.mirth.tls=true"
    #      - "traefik.http.services.mirth.loadbalancer.server.scheme=https"
    #      - "traefik.http.services.mirth.loadbalancer.server.port=8443"
    #      - "traefik.http.routers.mirth-http.middlewares=https-redirect@file"
    #      - "traefik.http.routers.mirth-http.rule=PathPrefix(`${CARTA_MIRTH_BASE_URL}`)"
    #      - "traefik.http.routers.mirth-http.entrypoints=web"
    environment:
      SERVICE: astrolabe
    ports:
      - "8086:8086"
    networks:
      default:
        aliases:
          - astrolabe.internal.carta.healthcare
          - trino.internal.carta.healthcare
          - trino

#
#  spark:
#    image: docker.io/bitnami/spark:3-debian-10
#    environment:
#      SPARK_MODE: master
#      SPARK_RPC_AUTHENTICATION_ENABLED: "no"
#      SPARK_RPC_ENCRYPTION_ENABLED: "no"
#      SPARK_LOCAL_STORAGE_ENCRYPTION_ENABLED: "no"
#      SPARK_SSL_ENABLED: "no"
#    ports:
#      - "8081:8080"
#
#  spark-worker-1:
#    image: docker.io/bitnami/spark:3-debian-10
#    environment:
#      SPARK_MODE: worker
#      SPARK_MASTER_URL: spark://spark:7077
#      SPARK_WORKER_MEMORY: 1G
#      SPARK_WORKER_CORES: "1"
#      SPARK_RPC_AUTHENTICATION_ENABLED: "no"
#      SPARK_RPC_ENCRYPTION_ENABLED: "no"
#      SPARK_LOCAL_STORAGE_ENCRYPTION_ENABLED: "no"
#      SPARK_SSL_ENABLED: "no"
#
#  spark-worker-2:
#    image: docker.io/bitnami/spark:3-debian-10
#    environment:
#      SPARK_MODE: worker
#      SPARK_MASTER_URL: spark://spark:7077
#      SPARK_WORKER_MEMORY: 1G
#      SPARK_WORKER_CORES: "1"
#      SPARK_RPC_AUTHENTICATION_ENABLED: "no"
#      SPARK_RPC_ENCRYPTION_ENABLED: "no"
#      SPARK_LOCAL_STORAGE_ENCRYPTION_ENABLED: "no"
#      SPARK_SSL_ENABLED: "no"

volumes:
  cartadb-data: {}
  carta-plugins: {}
  carta-datasets: {}
  carta-cache: {}
  carta-contents: {}
  carta-downloads: {}
  elastic-notes: {}
  elastic-umls: {}
  elastic-data: {}
  grafana-data: {}
  prometheus-data: {}
  semaphore-data: {}
  deployment-shared: {}

"""Unit tests for all of the `carta-client` command line interface commands

Ensure that functions are properly mapped and arguments are properly defined/defaulted
"""
import pytest
import unittest
import unittest.mock as mock

from carta_client.cli.parser import make_parser
from carta_client.utils import Dotfile, WatchableDict

TEST_BASE_URL = "https://local.carta.healthcare"
TEST_CUSTOMER_ID = None


@pytest.fixture(scope="session", autouse=True)
def mock_load_dotfile():
    mocked_dotfile = WatchableDict()
    mocked_dotfile.update(
        {
            "username": "some_user@carta.healthcare",
            "password": "some_password",
            "base_url": "https://local.carta.healthcare",
        }
    )
    with mock.patch.object(Dotfile, "load_dotfile") as f:
        f.return_value = mocked_dotfile
        yield f


class TestApiKeyCommands(unittest.TestCase):
    def test_api_key_create(self):
        parser = make_parser()
        options = parser.parse_args(["api-key", "create"])
        self.assertEqual(options.func.__name__, "api_key_create")
        self.assertFalse(options.name)
        self.assertEqual(options.scopes, ["trino"])
        self.assertFalse(options.username)
        self.assertFalse(options.auth)
        self.assertFalse(options.customer_id)
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_api_key_list(self):
        parser = make_parser()
        options = parser.parse_args(["api-key", "list"])
        self.assertEqual(options.func.__name__, "api_key_list")
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_api_key_delete(self):
        parser = make_parser()
        options = parser.parse_args(["api-key", "delete"])
        self.assertEqual(options.func.__name__, "api_key_delete")
        self.assertEqual(options.api_key_ids, [])
        self.assertEqual(options.base_url, TEST_BASE_URL)


class TestAstrolabeCommands(unittest.TestCase):
    def test_astrolabe_show_jdbc(self):
        parser = make_parser()
        options = parser.parse_args(["astrolabe", "show-jdbc"])
        self.assertEqual(options.func.__name__, "astrolabe_show_jdbc")
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_astrolabe_cli(self):
        parser = make_parser()
        options = parser.parse_args(["astrolabe", "cli"])
        self.assertEqual(options.func.__name__, "astrolabe_cli")
        self.assertEqual(options.statements, [])
        self.assertEqual(options.base_url, TEST_BASE_URL)


class TestAtlasCommands(unittest.TestCase):
    def test_encounter_import(self):
        encounter_jsons = ["path/to/some/file1.json", "path/to/some/file2.json"]

        parser = make_parser()
        options = parser.parse_args(
            ["encounter", "import", "--encounter_jsons"] + encounter_jsons
        )
        self.assertEqual(options.func.__name__, "encounter_import")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.encounter_jsons, encounter_jsons)

    def test_encounter_delete(self):
        encounter_ids = ["encounter-id-1", "encounter-id-2"]
        parser = make_parser()
        options = parser.parse_args(["encounter", "delete"] + encounter_ids)
        self.assertEqual(options.func.__name__, "encounter_delete")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.encounter_ids, encounter_ids)
        self.assertFalse(options.hard)

    def test_encounter_recommendations(self):
        encounter_id = "encounter-id-1"
        parser = make_parser()
        options = parser.parse_args(["encounter", "recommendations", encounter_id])
        self.assertEqual(options.func.__name__, "encounter_recommendations")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.encounter_id, encounter_id)


class TestBabelfishCommands(unittest.TestCase):
    def test_babelfish_ingest_inquiry(self):
        parser = make_parser()
        fake_job_id = "b2c5b80d-66cc-46b8-bc8c-2f86724df7d0"
        options = parser.parse_args(
            ["babelfish", "ingest-inquiry", "--job_id", fake_job_id]
        )
        self.assertEqual(options.func.__name__, "ingest_inquiry")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.job_id, fake_job_id)

    def test_babelfish_ingest_nlp(self):
        parser = make_parser()
        fake_note_id = "some-note"
        fake_note_path = "/path/to/some/note.html"
        options = parser.parse_args(
            ["babelfish", "ingest-nlp", "--note_id", fake_note_id, fake_note_path]
        )
        self.assertEqual(options.func.__name__, "ingest_nlp")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertFalse(options.synchronous)
        self.assertEqual(options.note_id, fake_note_id)
        self.assertEqual(options.note[0], fake_note_path)

    def test_babelfish_ingest_ocr(self):
        parser = make_parser()
        fake_note_id = "some-note"
        fake_note_path = "/path/to/some/note.png"

        options = parser.parse_args(
            ["babelfish", "ingest-ocr", "--note_id", fake_note_id, fake_note_path]
        )
        self.assertEqual(options.func.__name__, "ingest_ocr")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertFalse(options.synchronous)
        self.assertEqual(options.note_id, fake_note_id)
        self.assertEqual(options.note[0], fake_note_path)

    def test_babelfish_ingest_note(self):
        parser = make_parser()
        fake_note_id = "some-note"
        fake_note_path = "/path/to/some/note.pdf"
        fake_params = ["reliability=0.8", "patient_id=3234"]
        options = parser.parse_args(
            [
                "babelfish",
                "ingest-note",
                "--note_id",
                fake_note_id,
                fake_note_path,
                "--p",
                fake_params[0],
                "--p",
                fake_params[1],
            ]
        )
        self.assertEqual(options.func.__name__, "ingest_note")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertFalse(options.synchronous)
        self.assertEqual(options.note_id, fake_note_id)
        self.assertEqual(options.note[0], fake_note_path)
        self.assertEqual(options.p[0], fake_params[0])
        self.assertEqual(options.p[1], fake_params[1])


class TestConnectorCommands(unittest.TestCase):
    def test_connector_file(self):
        parser = make_parser()
        fake_source = "some/connector/id"
        fake_file_path = "/path/to/some/extract.csv"
        options = parser.parse_args(
            ["connector", "file", "--source", fake_source, fake_file_path]
        )
        self.assertEqual(options.func.__name__, "connector_file")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertFalse(options.synchronous)
        self.assertEqual(options.source, fake_source)
        self.assertFalse(options.preflight_check)
        self.assertIsNone(options.limit)
        self.assertEqual(options.inputs[0], fake_file_path)

    def test_connector_json(self):
        parser = make_parser()
        fake_source = "some/connector/id"
        fake_file_path = "/path/to/some/extract.json"
        options = parser.parse_args(
            ["connector", "json", "--source", fake_source, fake_file_path]
        )
        self.assertEqual(options.func.__name__, "connector_json")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertFalse(options.synchronous)
        self.assertEqual(options.source, fake_source)
        self.assertFalse(options.preflight_check)
        self.assertEqual(options.inputs[0], fake_file_path)

    def test_connector_npisheet(self):
        parser = make_parser()

        fake_customer_id = "some_customer"
        fake_sheet_url = "https://sheets.google.com/some-npi-sheet"

        options = parser.parse_args(
            [
                "connector",
                "npisheet",
                "--customer_id",
                fake_customer_id,
                "--sheet",
                fake_sheet_url,
            ]
        )
        self.assertEqual(options.func.__name__, "load_npi")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, fake_customer_id)
        self.assertFalse(options.synchronous)
        self.assertFalse(options.loadnpi)
        self.assertFalse(options.deletefiles)
        self.assertFalse(options.preflight_check)
        self.assertEqual(options.sheet, fake_sheet_url)


class TestCoreCommands(unittest.TestCase):
    def test_update(self):
        parser = make_parser()
        options = parser.parse_args(["update"])
        self.assertEqual(options.func.__name__, "update")
        self.assertFalse(options.experimental)

    def test_init(self):
        parser = make_parser()
        options = parser.parse_args(["init"])
        self.assertEqual(options.func.__name__, "init")

    def test_login(self):
        parser = make_parser()
        options = parser.parse_args(["login"])
        self.assertEqual(options.func.__name__, "login")
        self.assertFalse(options.show_token)
        self.assertFalse(options.store)
        self.assertEqual(options.username, [])


class TestCubeCommands(unittest.TestCase):
    def test_cube_update(self):
        parser = make_parser()
        fake_cube_id = "some/cube/id"
        fake_partitions = '[{"partition_key": "partition_value"}]'
        options = parser.parse_args(
            ["cube", "update", "--cube", fake_cube_id, "-", fake_partitions]
        )
        self.assertEqual(options.func.__name__, "cube_update")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertFalse(options.synchronous)
        self.assertFalse(options.complete)
        self.assertEqual(options.cube[0], fake_cube_id)
        self.assertEqual(options.partitions[0], "-")
        self.assertEqual(options.partitions[1], fake_partitions)

    def test_cube_prune(self):
        parser = make_parser()
        fake_cube_id = "some/cube/id"
        fake_partitions = '[{"partition_key": "partition_value"}]'
        options = parser.parse_args(
            ["cube", "prune", "--cube", fake_cube_id, "-", fake_partitions]
        )
        self.assertEqual(options.func.__name__, "cube_prune")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertFalse(options.prune_all)
        self.assertEqual(options.cube[0], fake_cube_id)
        self.assertEqual(options.partitions[0], "-")
        self.assertEqual(options.partitions[1], fake_partitions)

    def test_cube_drop(self):
        parser = make_parser()
        fake_cube_id = "some/cube/id"
        options = parser.parse_args(["cube", "drop", "--cube", fake_cube_id])
        self.assertEqual(options.func.__name__, "cube_drop")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.cube, fake_cube_id)

    def test_cube_list(self):
        parser = make_parser()
        options = parser.parse_args(["cube", "list"])
        self.assertEqual(options.func.__name__, "cube_list")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)

    def test_cube_info(self):
        parser = make_parser()
        fake_cube_id = "some/cube/id"
        options = parser.parse_args(["cube", "info", "--cube", fake_cube_id])
        self.assertEqual(options.func.__name__, "cube_info")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.cube, fake_cube_id)


class TestDatasetCommands(unittest.TestCase):
    def test_dataset_list(self):
        parser = make_parser()
        options = parser.parse_args(["dataset", "list"])
        self.assertEqual(options.func.__name__, "dataset_list")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.dataset_id, None)

    def test_dataset_info(self):
        parser = make_parser()
        fake_dataset_id = "some/dataset/id"
        fake_dataset_version = "1.2.3"
        options = parser.parse_args(
            [
                "dataset",
                "info",
                "--dataset_id",
                fake_dataset_id,
                "--version",
                fake_dataset_version,
            ]
        )
        self.assertEqual(options.func.__name__, "dataset_info")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.dataset_id, fake_dataset_id)
        self.assertEqual(options.version, fake_dataset_version)

    def test_dataset_install(self):
        parser = make_parser()
        fake_dataset_id = "some/dataset/id"
        fake_dataset_version = "1.2.3"
        fake_partitions = '[{"partition_key": "partition_value"}]'
        options = parser.parse_args(
            [
                "dataset",
                "install",
                "--dataset_id",
                fake_dataset_id,
                "--version",
                fake_dataset_version,
                fake_partitions,
            ]
        )
        self.assertEqual(options.func.__name__, "dataset_install")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertFalse(options.synchronous)
        self.assertFalse(options.wait)
        self.assertFalse(options.info)
        self.assertIsNone(options.limit)
        self.assertEqual(options.dataset_id, fake_dataset_id)
        self.assertEqual(options.version, fake_dataset_version)
        self.assertEqual(options.partitions[0], fake_partitions)

    def test_dataset_uninstall(self):
        parser = make_parser()
        fake_dataset_id = "some/dataset/id"
        options = parser.parse_args(
            ["dataset", "uninstall", "--dataset_id", fake_dataset_id]
        )
        self.assertEqual(options.func.__name__, "dataset_uninstall")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.dataset_id, fake_dataset_id)

    def test_dataset_evaluate(self):
        parser = make_parser()
        fake_dataset_id = "some/dataset/id"
        fake_evaluation_id = "some/evaluation/id"
        fake_dataset_version = "1.2.3"
        options = parser.parse_args(
            [
                "dataset",
                "evaluate",
                "--dataset_id",
                fake_dataset_id,
                "--version",
                fake_dataset_version,
                "--evaluation_ids",
                fake_evaluation_id,
            ]
        )
        self.assertEqual(options.func.__name__, "dataset_evaluate")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertFalse(options.synchronous)
        self.assertEqual(options.dataset_id, fake_dataset_id)
        self.assertEqual(options.version, fake_dataset_version)
        self.assertEqual(options.evaluation_ids[0], fake_evaluation_id)


class TestDeploymentCommands(unittest.TestCase):
    def test_dev(self):
        parser = make_parser()
        attach_services = ["ocean"]
        options = parser.parse_args(["dev"] + attach_services)
        self.assertEqual(options.func.__name__, "release_dev")
        self.assertIsNone(options.deployment)
        self.assertFalse(options.force_recreate)
        self.assertFalse(options.all)
        self.assertFalse(options.no_hostnet)
        self.assertIsNone(options.version)
        self.assertIsNone(options.code_root)
        self.assertFalse(options.pull)
        self.assertEqual(options.attach, attach_services)
        self.assertFalse(options.down)
        self.assertFalse(options.stop)
        self.assertFalse(options.separate_ocr_nlp)

    def test_release_deploy(self):
        parser = make_parser()
        options = parser.parse_args(["release", "deploy"])
        self.assertEqual(options.func.__name__, "release_deploy")
        self.assertIsNone(options.deployment)
        self.assertFalse(options.dry_run)
        self.assertFalse(options.force_recreate)
        self.assertFalse(options.clean_old_images)
        self.assertIsNone(options.version)
        self.assertIsNone(options.code_root)
        self.assertIsNone(options.attach)
        self.assertFalse(options.pull)
        self.assertFalse(options.no_version_matching)
        self.assertFalse(options.version_matching)
        self.assertFalse(options.backup)
        self.assertFalse(options.separate_ocr_nlp)

    def test_release_destroy(self):
        parser = make_parser()
        options = parser.parse_args(["release", "destroy"])
        self.assertEqual(options.func.__name__, "release_destroy")
        self.assertIsNone(options.deployment)
        self.assertIsNone(options.version)

    def test_deployment_setup(self):
        parser = make_parser()
        options = parser.parse_args(["deployment-setup"])
        self.assertEqual(options.func.__name__, "deployment_setup")
        self.assertIsNone(options.deployment)

    def test_refresh_certs(self):
        parser = make_parser()
        options = parser.parse_args(["refresh-certs"])
        self.assertEqual(options.func.__name__, "refresh_certs")
        self.assertIsNone(options.deployment)

    def test_refresh_wiseor_certs(self):
        parser = make_parser()
        options = parser.parse_args(["refresh-wiseor-certs"])
        self.assertEqual(options.func.__name__, "refresh_wiseor_certs")
        self.assertIsNone(options.deployment)

    def test_file_carta(self):
        parser = make_parser()
        fake_deployment = "some_deployment"
        options = parser.parse_args(["file-carta", "--deployment", fake_deployment])
        self.assertEqual(options.func.__name__, "file_carta")
        self.assertEqual(options.deployment, fake_deployment)
        self.assertFalse(options.force)

    def test_status_preflight_check(self):
        parser = make_parser()
        options = parser.parse_args(["status", "preflight-check"])
        self.assertEqual(options.func.__name__, "preflight_check")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertFalse(options.json)

    def test_status_doctor(self):
        parser = make_parser()
        options = parser.parse_args(["doctor"])
        self.assertEqual(options.func.__name__, "doctor")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertFalse(options.pre_only)


class TestGrafanaCommands(unittest.TestCase):
    def test_grafana_list_dashboards(self):
        parser = make_parser()
        options = parser.parse_args(["grafana", "dashboards"])
        self.assertEqual(options.func.__name__, "grafana_list_dashboards")
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_grafana_list_datasources(self):
        parser = make_parser()
        options = parser.parse_args(["grafana", "datasources"])
        self.assertEqual(options.func.__name__, "grafana_datasources")
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_grafana_create_dashboard(self):
        title = "Some-Dashboard"
        parser = make_parser()
        options = parser.parse_args(["grafana", "create", title])
        self.assertEqual(options.func.__name__, "grafana_create_dashboard")
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.title, [title])

    def test_grafana_load_default_dashboard(self):
        parser = make_parser()
        options = parser.parse_args(["grafana", "load"])
        self.assertEqual(options.func.__name__, "grafana_load_default")
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_grafana_remove_dashboard(self):
        dashboard_uid = "some-dashboard"
        parser = make_parser()
        options = parser.parse_args(["grafana", "delete", dashboard_uid])
        self.assertEqual(options.func.__name__, "grafana_remove_dashboard")
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.uid, [dashboard_uid])


class TestHL7Commands(unittest.TestCase):
    def test_hl7_mirth(self):
        parser = make_parser()
        options = parser.parse_args(["mirth"])
        self.assertEqual(options.func.__name__, "mirth")


class TestJobCommands(unittest.TestCase):
    def test_job_cancel(self):
        job_id = "some-job"
        customer_id = "some-customer"
        parser = make_parser()
        options = parser.parse_args(["job", "cancel", job_id])
        self.assertEqual(options.func.__name__, "job_cancel")
        self.assertEqual(options.customer_id, None)
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.job_id, [job_id])

        # Cancel all for a specific customer_id
        parser = make_parser()
        options = parser.parse_args(
            ["job", "cancel", "all", "--customer_id", customer_id]
        )
        self.assertEqual(options.func.__name__, "job_cancel")
        self.assertEqual(options.customer_id, [customer_id])
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.job_id, ["all"])

    def test_job_details(self):
        job_id = "some-job"
        parser = make_parser()
        options = parser.parse_args(["job", "details", job_id])
        self.assertEqual(options.func.__name__, "job_details")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.job_id, [job_id])
        self.assertEqual(options.graph, False)
        self.assertEqual(options.logs, False)


class TestKubernetesCommands(unittest.TestCase):
    def test_kubernetes_install_operators(self):
        parser = make_parser()
        options = parser.parse_args(["kubernetes", "install-operators"])
        self.assertEqual(options.func.__name__, "kubernetes_install_operators_function")
        self.assertEqual(options.namespace, None)


class TestPenguinsearchCommands(unittest.TestCase):
    def test_dictionary_search(self):
        query_string = "heart attack"
        parser = make_parser()
        options = parser.parse_args(["dictionary-search", query_string])
        self.assertEqual(options.func.__name__, "dictionary_search")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.query_string, query_string)
        self.assertEqual(options.limit, 10)

    def test_dictionary_get(self):
        query = "C12345678"
        parser = make_parser()
        options = parser.parse_args(["dictionary-get", query])
        self.assertEqual(options.func.__name__, "dictionary_get")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.query, query)


class TestPluginCommands(unittest.TestCase):
    def test_plugin_create(self):
        project_directory = "some/project/dir"
        project_name = "my-plugin"
        parser = make_parser()
        options = parser.parse_args(
            ["plugin", "create", project_name, "--project-directory", project_directory]
        )
        self.assertEqual(options.func.__name__, "plugin_create")
        self.assertEqual(options.project_directory, project_directory)
        self.assertEqual(options.project_name, [project_name])

    def test_plugin_fetch(self):
        plugin = "my-plugin"
        parser = make_parser()
        options = parser.parse_args(["plugin", "fetch", plugin])
        self.assertEqual(options.func.__name__, "plugin_fetch")
        self.assertIsNone(options.deployment)
        self.assertEqual(options.plugin, [plugin])

    def test_plugin_install(self):
        plugin = "my-plugin"
        parser = make_parser()
        options = parser.parse_args(["plugin", "install", plugin])
        self.assertEqual(options.func.__name__, "plugin_install")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.upload_ssh_key, False)
        self.assertEqual(options.pip, False)
        self.assertEqual(options.deployment, None)
        self.assertEqual(options.branch, None)
        self.assertEqual(options.plugins, [plugin])

    def test_plugin_list(self):
        parser = make_parser()
        options = parser.parse_args(["plugin", "list"])
        self.assertEqual(options.func.__name__, "plugin_list")
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_plugin_uninstall(self):
        plugin = "my-plugin"
        deployment = "some-deployment"
        parser = make_parser()
        options = parser.parse_args(
            ["plugin", "uninstall", plugin, "--deployment", deployment]
        )
        self.assertEqual(options.func.__name__, "plugin_uninstall")
        self.assertEqual(options.deployment, deployment)
        self.assertEqual(options.plugins, [plugin])

    def test_plugin_update(self):
        plugin = "my-plugin"
        parser = make_parser()
        options = parser.parse_args(["plugin", "update", plugin])
        self.assertEqual(options.func.__name__, "plugin_update")
        self.assertEqual(options.upload_ssh_key, False)
        self.assertEqual(options.plugins, [plugin])

    def test_plugin_update_all(self):
        parser = make_parser()
        options = parser.parse_args(["plugin", "update-all"])
        self.assertEqual(options.func.__name__, "plugin_update_all")
        self.assertEqual(options.force, False)
        self.assertEqual(options.verbose, False)

    def test_plugin_reload(self):
        parser = make_parser()
        options = parser.parse_args(["plugin", "reload"])
        self.assertEqual(options.func.__name__, "plugin_reload")
        self.assertEqual(options.base_url, TEST_BASE_URL)


class TestRedisCommands(unittest.TestCase):
    def test_redis_delete(self):
        delete_argument = "all"
        parser = make_parser()
        options = parser.parse_args(["redis", "delete", delete_argument])
        self.assertEqual(options.func.__name__, "redis")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.delete, delete_argument)
        self.assertEqual(options.init_schemas, False)
        self.assertEqual(options.valuesets, False)

    def test_redis_get_registries(self):
        registries_argument = "all"
        parser = make_parser()
        options = parser.parse_args(["redis", "registries", registries_argument])
        self.assertEqual(options.func.__name__, "redis")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.registries, registries_argument)


class TestRegistriesCommands(unittest.TestCase):
    def test_registry_upload(self):
        sheet = "https://sheets.google.com/some-npi-sheet"
        parser = make_parser()
        options = parser.parse_args(["registry", "upload", "--sheet", sheet])
        self.assertEqual(options.func.__name__, "registries_upload")
        self.assertEqual(options.sheet, [sheet])
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_registry_list(self):
        parser = make_parser()
        options = parser.parse_args(["registry", "list"])
        self.assertEqual(options.func.__name__, "registries_list_released")
        self.assertEqual(options.registry_name, None)
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_registry_load(self):
        registry_name = "some-registry"
        revision = "1.2.3"
        parser = make_parser()
        options = parser.parse_args(
            ["registry", "load", registry_name, "--revision", revision]
        )
        self.assertEqual(options.func.__name__, "registries_load_released")
        self.assertEqual(options.registry_name, registry_name)
        self.assertEqual(options.revision, revision)
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_registry_load_custom_config(self):
        sheet = "https://sheets.google.com/some-custom-config-sheet"

        parser = make_parser()
        options = parser.parse_args(
            ["registry", "load-custom-config", "--sheets", sheet]
        )
        self.assertEqual(options.func.__name__, "registry_load_configuration")
        self.assertEqual(options.sheets, sheet)

    def test_registry_list(self):
        parser = make_parser()
        options = parser.parse_args(["registry", "list"])
        self.assertEqual(options.func.__name__, "registries_list_released")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.registry_name, None)

    def test_valueset_download(self):
        save_path = "some/path/to_save.xlsx"
        parser = make_parser()
        options = parser.parse_args(["valueset", "download", "--save-path", save_path])
        self.assertEqual(options.func.__name__, "download_valuesets")
        self.assertEqual(options.save_path, save_path)
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_valueset_load(self):
        valueset_name = "some-valueset"
        revision = "1.2.3"
        parser = make_parser()
        options = parser.parse_args(
            [
                "valueset",
                "load",
                "--valueset_name",
                valueset_name,
                "--revision",
                revision,
            ]
        )
        self.assertEqual(options.func.__name__, "load_valueset_revision")
        self.assertEqual(options.valueset_name, valueset_name)
        self.assertEqual(options.revision, revision)
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_valueset_info(self):
        valueset_name = "some-valueset"
        revision = "1.2.3"
        parser = make_parser()
        options = parser.parse_args(
            [
                "valueset",
                "info",
                "--valueset_name",
                valueset_name,
                "--revision",
                revision,
            ]
        )
        self.assertEqual(options.func.__name__, "valueset_revision_info")
        self.assertEqual(options.valueset_name, valueset_name)
        self.assertEqual(options.revision, revision)
        self.assertEqual(options.base_url, TEST_BASE_URL)


class TestServerCommands(unittest.TestCase):
    def test_server_trust_proxy(self):
        parser = make_parser()
        options = parser.parse_args(["server", "trust-proxy"])
        self.assertEqual(options.func.__name__, "trust_proxy")
        self.assertEqual(options.get_os_cabundle, False)
        self.assertEqual(options.host, None)
        self.assertEqual(options.cabundle_path, None)

    def test_license_docker_login(self):
        profile = "some-profile"
        parser = make_parser()
        options = parser.parse_args(["license", "docker-login", "--profile", profile])
        self.assertEqual(options.func.__name__, "license_docker_login")
        self.assertEqual(options.profile, profile)

    def test_license_vault_login(self):
        parser = make_parser()
        options = parser.parse_args(["license", "vault-login"])
        self.assertEqual(options.func.__name__, "license_vault_login")

    def test_server_encrypt(self):
        cmd = "setup"
        device = "some-device"
        container = "some-container"
        mount_point = "some/path"

        parser = make_parser()
        options = parser.parse_args(
            [
                "encrypt",
                cmd,
                "--device",
                device,
                "--container",
                container,
                "--mount-point",
                mount_point,
            ]
        )
        self.assertEqual(options.func.__name__, "encrypt")
        self.assertEqual(options.device, device)
        self.assertEqual(options.container, container)
        self.assertEqual(options.provision, False)
        self.assertEqual(options.size, None)
        self.assertEqual(options.mount_point, mount_point)
        self.assertEqual(options.cmd, [cmd])


class TestUserCommands(unittest.TestCase):
    def test_user_create(self):
        username = "some-user"
        name = "First Last"
        customer_id = "some-customer"

        parser = make_parser()
        options = parser.parse_args(
            [
                "user",
                "create",
                "--username",
                username,
                "--name",
                name,
                "--customer_id",
                customer_id,
            ]
        )
        self.assertEqual(options.func.__name__, "user_create")
        self.assertEqual(options.customer_id, customer_id)
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.username, username)
        self.assertEqual(options.name, name)

    def test_user_password(self):
        username = "some-user"

        parser = make_parser()
        options = parser.parse_args(["user", "password", username])
        self.assertEqual(options.func.__name__, "user_password")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.username, [username])

    def test_user_bulk_upload(self):
        sheet = "https://sheets.google.com/some-npi-sheet"

        parser = make_parser()
        options = parser.parse_args(["user", "bulk-upload", "--sheet", sheet])
        self.assertEqual(options.func.__name__, "user_bulk_upload")
        self.assertEqual(options.sheet, sheet)
        self.assertEqual(options.path, None)
        self.assertEqual(options.authenticator, "local")
        self.assertEqual(options.create_role_for_existing_users, False)

    def test_identity_check_permissions(self):
        required_scopes = "/products/atlas/export:read"

        parser = make_parser()
        options = parser.parse_args(["identity", "check-permissions", required_scopes])
        self.assertEqual(options.func.__name__, "identity_check_perm")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.required_scopes, [required_scopes])

    def test_identity_delete(self):
        identity_id = "1"

        parser = make_parser()
        options = parser.parse_args(["identity", "delete", identity_id])
        self.assertEqual(options.func.__name__, "identity_delete")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.identity_id, [identity_id])

    def test_identity_add_role(self):
        role = "admin"
        username = "some-user"

        parser = make_parser()
        options = parser.parse_args(["identity", "add-role", role, username])
        self.assertEqual(options.func.__name__, "identity_add_role")
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.authenticator, "local")
        self.assertEqual(options.role, [role])
        self.assertEqual(options.username, [username])

    def test_identity_remove_role(self):
        role = "admin"
        username = "some-user"

        parser = make_parser()
        options = parser.parse_args(["identity", "remove-role", role, username])
        self.assertEqual(options.func.__name__, "identity_remove_role")
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.system_role, False)
        self.assertEqual(options.authenticator, "local")
        self.assertEqual(options.role, [role])
        self.assertEqual(options.username, [username])

    def test_role_list(self):
        parser = make_parser()
        options = parser.parse_args(["role", "list"])
        self.assertEqual(options.func.__name__, "role_list")
        self.assertEqual(options.base_url, TEST_BASE_URL)

    def test_role_set_grant(self):
        name = "admin"
        path = "some/path"
        grants = "sudo"

        parser = make_parser()
        options = parser.parse_args(["role", "set-grant", name, path, grants])
        self.assertEqual(options.func.__name__, "role_set_grant")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.system_role, False)
        self.assertEqual(options.name, [name])
        self.assertEqual(options.path, [path])
        self.assertEqual(options.grants, [grants])

    def test_role_remove_grant(self):
        name = "admin"
        path = "some/path"

        parser = make_parser()
        options = parser.parse_args(["role", "remove-grant", name, path])
        self.assertEqual(options.func.__name__, "role_remove_grant")
        self.assertEqual(options.base_url, TEST_BASE_URL)
        self.assertEqual(options.customer_id, TEST_CUSTOMER_ID)
        self.assertEqual(options.system_role, False)
        self.assertEqual(options.name, [name])
        self.assertEqual(options.path, [path])


class TestVaultCommands(unittest.TestCase):
    def test_vault_login(self):
        parser = make_parser()
        options = parser.parse_args(["vault", "login"])
        self.assertEqual(options.func.__name__, "vault_login")
        self.assertEqual(options.role, "developer")
        self.assertEqual(options.method, "aws")
        self.assertEqual(options.aws_profile, "master")

    def test_vault_ssh(self):
        parser = make_parser()
        options = parser.parse_args(["vault", "ssh"])
        self.assertEqual(options.func.__name__, "vault_ssh")
        self.assertEqual(options.role, "developer")
        self.assertEqual(options.method, "aws")
        self.assertEqual(options.aws_profile, "master")
        self.assertFalse(options.login)
        self.assertIsNone(options.principal)

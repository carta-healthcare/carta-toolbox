class TestCartaClient:
    def test_carta_client_imports(self):
        """Test all the internal imports at the top of the carta-client script to ensure
        this is a valid python package
        """
        from carta_client.utils import LOG
        from carta_client.deploy import (
            DeploymentManager,
            DockerComposeDeploymentManager,
            VaultSecretClient,
        )
        from carta_client.plugins import PluginManager
        from carta_client import VAULT_ADDR, DOCKER_REGISTRY
        from carta_client.cli import KubernetesCommands
        from carta_client import Dotfile

        pass

import os
import subprocess
import datetime
import docker


class DatabaseOps:
    def __init__(self, deployment=None):
        self.deployment = deployment or os.environ.get("CARTA_DEPLOYMENT", None)
        self.client = docker.from_env()
        self.db_containers = [
            c.name for c in self.client.containers.list(filters={"name": "cartadb"})
        ]

    def choose_deployments(self):
        deployments = [dbs.split("_")[0] for dbs in self.db_containers]

        if len(deployments) == 1:
            return deployments[0]

        raise Exception(
            "Found more than one deployment, please use the --deployment flag."
        )

    def backup(self):
        if self.deployment == "all":
            for c in self.db_containers:
                self.perform_backup(c)
        else:
            self.perform_backup(f"{self.deployment}_cartadb_1")

    def perform_backup(self, container_id):
        backup_file = datetime.datetime.now().strftime("%Y-%m-%d_%H:%m:%S")
        backup_file = f"/tmp/cartadb-{backup_file}.db.gz"
        container = self.client.containers.get(container_id)
        print("Backing up:", container_id)
        c = [
            "/bin/bash",
            "-c",
            f"pg_dumpall --clean -U $POSTGRES_USER | gzip > {backup_file}",
        ]
        backup_command = container.exec_run(c)
        if backup_command.exit_code != 0:
            raise ("Failed to backup", container_id)
        subprocess.run(["docker", "cp", f"{container_id}:{backup_file}", "."])
        print("Successfully backed up", container_id)
        print("Backup file created:", os.path.basename(backup_file))

    def restore(self, backup_file):
        if self.deployment == "all":
            raise Exception("Can't restore a file to all deployments.")

        container_id = f"{self.deployment}_cartadb_1"

        print("Restoring", backup_file, "to", container_id)
        backup_file_dest = f"/tmp/{backup_file}"

        container = self.client.containers.get(container_id)
        print("Copying backup file to container:", container_id)
        copy = subprocess.run(
            ["docker", "cp", f"./{backup_file}", f"{container_id}:{backup_file_dest}"]
        )
        if copy.returncode != 0:
            raise Exception("Failed to copy backup file")

        print("Decompressing backup file:", container_id)
        decompress_command = container.exec_run(["gzip", "-df", backup_file_dest])
        print(decompress_command)
        if decompress_command.exit_code != 0:
            raise Exception("Failed to decompress backup file")

        restore_script = """
#! /bin/bash
databases=$(cat $1  | grep 'DROP DATABASE' | sed 's/;//' | awk '{ print $3 }')
for db in $databases
do
    echo $db
    UPDATE pg_database SET datallowconn = 'false' WHERE datname = '$db';
    psql -U $POSTGRES_USER -d $db -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '$db' AND pid <> pg_backend_pid();"
    psql -U $POSTGRES_USER -d $db -c "ALTER DATABASE $db WITH CONNECTION LIMIT 0;"
done
cat $1 | psql -U $POSTGRES_USER

"""
        print("Copying restore script to container:", container_id)
        with open(os.path.join(os.getcwd(), "restore.sh"), "w") as f:
            f.write(restore_script)
        subprocess.run(
            ["docker", "cp", "restore.sh", f"{container_id}:/tmp/restore.sh"]
        )
        os.remove("restore.sh")

        print("Restoring backup file to container:", container_id)
        backup_file_dest = backup_file_dest.replace(".db.gz", ".db")
        c = ["bash", "-c", "chmod +x /tmp/restore.sh && /tmp/restore.sh"]
        restore_command = container.exec_run(c)
        if restore_command.exit_code != 0:
            raise Exception("Failed to restore", container_id)
        print("Cleaning up backup file in container:", container_id)
        restore_command = container.exec_run(["rm", backup_file_dest])
        restore_command = container.exec_run(["rm", "/tmp/restore.sh"])

        print("Successfully restored", container_id)

    def handle(self, dump_file=None):
        if not self.deployment:
            self.deployment = self.choose_deployments()

        if dump_file:
            self.restore(dump_file)
        else:
            self.backup()

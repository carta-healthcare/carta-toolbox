import os
import requests


class PrivatePypiServer:
    def __init__(self, deployment_manager):
        self.deployment_manager = deployment_manager
        self.session = requests.Session()
        self.session.timeout = (5.1, 120)

    def get_pypi_server(self):
        if os.environ.get("DEV_MODE"):
            # return "http://localhost:1818"
            return "http://vitor.dev:1818"
        return "https://carta-pypi.ops.carta.healthcare"

    def get_azure_headers(self):
        import adal

        tenant_id = self.deployment_manager.deployment_values["azure"]["tenant_id"]
        auth_uri = f"https://login.microsoftonline.com/{tenant_id}"
        context = adal.AuthenticationContext(auth_uri, api_version=2)

        client_id = self.deployment_manager.deployment_values["azure"]["principal_id"]
        client_password = self.deployment_manager.deployment_values["azure"]["password"]
        token = context.acquire_token_with_client_credentials(
            "https://management.core.windows.net/", client_id, client_password
        )
        authHeader = {
            "Authorization": "Bearer " + token["accessToken"],
            "Content-Type": "application/json",
        }
        return authHeader

    def fetch_pypi_token(self):
        pypi_host = self.get_pypi_server()
        azure_headers = self.get_azure_headers()
        if azure_headers:
            self.session.headers = azure_headers
        response = self.session.post(pypi_host + "/az_auth")
        if response.status_code != 200:
            raise Exception(
                f"Could authenticate with private pypi. Status: {response.status_code} Content: {str(response)}"
            )
        token = response.json().get("token", False)
        if not token:
            raise Exception("Could not fetch token.")
        return token

    def auth_url(self, token):
        username = self.deployment_manager.deployment
        auth_url = f"{self.get_pypi_server()}/".split("://")
        auth_url[1] = f"{username}:{token}@{auth_url[1]}"
        return "://".join(auth_url)

    def get_install_arguments(self):
        token = self.fetch_pypi_token()
        plugins = self.deployment_manager.deployment_values["plugins"]
        return self.auth_url(token), plugins

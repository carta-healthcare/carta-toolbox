import getpass
import json
import logging
import logging.config
import os
import platform
import subprocess
import sys
import uuid
from pathlib import Path

LOG = logging.getLogger(__name__)


def carta_popen(
    command,
    stdout=None,
    stderr=None,
    stdin=None,
    env=None,
    shell=False,
    cwd=None,
    check=False,
    quiet=False,
):
    if not quiet:
        if shell:
            LOG.info("Running command {}".format(command))
        else:
            LOG.info("Running command {}".format(" ".join(command)))
    runner = subprocess.Popen if not check else subprocess.check_call
    return runner(
        command,
        stdout=stdout,
        stderr=stderr,
        stdin=stdin,
        env=env,
        shell=shell,
        cwd=cwd,
    )


def check_output(popen, failure_message):
    if popen.wait() != 0:
        raise RuntimeError(failure_message)


def dockerize_deployment(deployment):
    return deployment.replace("_", "")  # .replace("-", "")


class WatchableDict(dict):
    def __init__(self, *args, **kwargs):
        self.updated = False
        super(WatchableDict, self).__init__(*args, **kwargs)

    def __setitem__(self, *args, **kwargs):
        self.updated = True
        return super(WatchableDict, self).__setitem__(*args, **kwargs)


class Dotfile(object):
    def __init__(self, store_path: str = None):
        self.store_path = store_path or os.path.join(
            os.environ["HOME"], ".carta-client"
        )
        self.dotfile = self.load_dotfile()

    def load_dotfile(self) -> WatchableDict:
        new_dotfile = WatchableDict()
        if os.path.exists(self.store_path):
            with open(self.store_path) as f:
                try:
                    data = json.load(f)
                except json.JSONDecodeError:
                    raise ValueError(f"{self.store_path} dotfile is badly formatted!")
                new_dotfile.update(data)
        else:
            raise ValueError(f"{self.store_path} dotfile does not exist!")

        return new_dotfile

    def get_access_token(self):
        return self.dotfile.get("access_token")

    def set_access_token(self, access_token):
        self.dotfile["access_token"] = access_token

    def get_refresh_token(self):
        return self.dotfile.get("refresh_token")

    def set_refresh_token(self, refresh_token):
        self.dotfile["refresh_token"] = refresh_token

    def get_base_url(self):
        return self.dotfile.get("base_url")

    def set_base_url(self, base_url):
        self.dotfile["base_url"] = base_url

    def get_username(self):
        return self.dotfile.get("username")

    def set_username(self, username):
        self.dotfile["username"] = username

    def get_password(self):
        return self.dotfile.get("password")

    def set_password(self, password):
        self.dotfile["password"] = password

    def get_customer_id(self):
        return self.dotfile.get("customer_id")

    def set_customer_id(self, customer_id):
        self.dotfile["customer_id"] = customer_id

    def commit(self):
        if self.dotfile.updated:
            self.save()

    def save(self):
        with open(self.store_path, "w") as f:
            json.dump(self.dotfile, f, indent=4)

    def get_ssh_filename(self):
        return self.dotfile.get("ssh_fname") or "id_rsa"


def get_base_url_and_token(options, dotfile):
    base_url = None
    if hasattr(options, "base_url"):
        base_url = options.base_url
    if not base_url:
        base_url = dotfile.get_base_url()

    if not base_url:
        LOG.error(
            "Base URL must be specified either in ~/.carta-client or in --base-url. Exiting."
        )
        sys.exit(1)
    access_token = dotfile.get_access_token()
    if not access_token:
        LOG.error("Log in with carta-client login first. Exiting.")
        sys.exit(1)

    return base_url, access_token


def get_username_password(dotfile, username=None):
    if username is None:
        username, password = dotfile.get_username(), dotfile.get_password()
        if username and password:
            return username, password
    else:
        stored_username, password = dotfile.get_username(), dotfile.get_password()
        if username == stored_username and password:
            return username, password

    if sys.stdin.isatty():
        print("Enter credentials")
        if not username:
            username = input("Username: ")
        password = getpass.getpass("Password: ")
    else:
        username = sys.stdin.readline().rstrip()
        password = sys.stdin.readline().rstrip()

    return username, password


def get_interface(options, interface_class):
    from carta_client import Dotfile

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    if not base_url:
        print(
            "Must specify base url in either ~/.carta-client or with --base-url",
            file=sys.stderr,
        )
        sys.exit(1)

    access_token = dotfile.get_access_token()
    if not access_token:
        print("No access token found. Please log in first", file=sys.stderr)

    interface_class_instance = interface_class(base_url, access_token)
    return interface_class_instance


class BaseCommands:
    def __init__(self, subparsers, dotfile: Dotfile):
        self.subparsers = subparsers
        self.dotfile = dotfile
        self.defaults = self.dotfile.dotfile

        #
        # Reusable Args
        #

        self.base_url_arg = lambda x: x.add_argument(
            "--base-url",
            default=self.defaults.get("base_url"),
            help="The base url that points to the deployment",
        )
        self.synchronous_arg = lambda x: x.add_argument(
            "--synchronous",
            "--sync",
            action="store_true",
            default=False,
            help="Run the command's events synchronously in ocean instead of async in worker nodes",
        )
        self.wait_arg = lambda x: x.add_argument(
            "--wait",
            action="store_true",
            default=False,
            help="Run the command's events asynchronously and wait",
        )
        self.info_arg = lambda x: x.add_argument(
            "--info",
            action="store_true",
            default=False,
            help="Installed dataset information",
        )
        self.deployment_arg = lambda x: x.add_argument(
            "--deployment",
            action="store",
            default=self.defaults.get("deployment"),
            required=False,
            help="The deployment to operate upon",
        )
        self.customer_id_arg = lambda x: x.add_argument(
            "--customer_id",
            action="store",
            default=self.defaults.get("customer_id"),
            required=False,
            help="The customer_id",
        )


def get_machine_uuid():
    default_uuid_folder = Path.home()
    nodemid_path = os.path.join(default_uuid_folder, ".nodemid")
    system = platform.system()
    uuid_data = None
    if system.lower() == "linux":
        if os.path.exists("/var/lib/dbus/machine-id"):
            with open("/var/lib/dbus/machine-id") as f:
                uuid_data = f.read().strip()
                if len(uuid_data) < 20:
                    uuid_data = None
                else:
                    uuid_data = (
                        uuid_data[0:8]
                        + "-"
                        + uuid_data[8:12]
                        + "-"
                        + uuid_data[12:16]
                        + "-"
                        + uuid_data[16:20]
                        + "-"
                        + uuid_data[20:]
                    )

    elif system.lower() == "darwin":
        # TODO: Implement
        pass
    elif system.lower() == "windows":
        # TODO: Implement
        pass
    else:
        pass
    if uuid_data is None:
        if os.path.exists(nodemid_path):
            with open(nodemid_path) as f:
                uuid_data = f.read().strip()
        if uuid_data is None:
            uuid_data = str(uuid.uuid4())
            with open(nodemid_path, "w") as f:
                f.write(uuid_data)
    return uuid_data


def print_response(response):
    print(json.dumps(response, sort_keys=True, indent=4))


def get_active_branch_name(repository_path):
    head_dir = Path(repository_path) / ".git" / "HEAD"
    with head_dir.open("r") as f:
        content = f.read().splitlines()

    for line in content:
        if line[0:4] == "ref:":
            return line.partition("refs/heads/")[2]


def configure_logging(level="INFO"):
    logging_dict = {
        "disable_existing_loggers": False,
        "version": 1,
        "formatters": {
            "full": {
                "format": "[%(levelname)-6s {(%(asctime)s)} : %(name)-12s :: %(filename)s:%(lineno)d#%(funcName)s]   %(message)s"
            }
        },
        "root": {"level": level, "handlers": ["console"]},
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "formatter": "full",
                "level": "DEBUG",
                "stream": "ext://sys.stderr",
            }
        },
    }
    logging.config.dictConfig(logging_dict)
    LOG.info("Logging configured")


configure_logging()

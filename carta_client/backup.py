import os
import shutil
import subprocess
import sys
import tarfile
import uuid
import json
from datetime import datetime
from carta_client.utils import LOG, dockerize_deployment, carta_popen


def popen_helper(cmd, stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE ,decode_stdout=True):
    proc = carta_popen(command=cmd, stdout=stdout, stderr=stderr, stdin=stdin)
    status_code = proc.wait()
    error = ""
    if status_code:
        error = proc.stderr.read().decode("utf-8") if decode_stdout else proc.stderr
    output = proc.stdout.read().decode("utf-8") if decode_stdout else proc.stdout
    return status_code, output, error


def _get_deployment_name():
    _, docker_ps_output, _ = popen_helper(
        cmd = ["docker", "ps", "--format", "{{.Names}}"], decode_stdout=False
    )

    _, grep_output, _ = popen_helper(
        cmd = ["grep", "ocean"], stdin = docker_ps_output
    )
    contianer_name = grep_output.split()[0]
    if contianer_name:
        deployment = contianer_name.split("ocean")[0].split("_")[0]
    else:
        LOG.error(f"Error getting deployment name, please check if it is running.")
        sys.exit(1)
    return deployment


def _get_container_id(deployment, container_function):
    deployment_name = dockerize_deployment(deployment)
    contianer_name = f"{deployment_name}_{container_function}_1"
    return contianer_name


class BackupManager:
    customer_id = None
    cartadb_system_schemas = [
        "carta_{}",
        "carta_cartographer_{}",
        "carta",
        "carta_cartographer_demo",
        "carta_cartographer_testing",
        "carta_compass",
        "carta_demo",
        "carta_system",
    ]

    def __init__(self, deployment, path, tmp_dir):
        self.deployment = deployment if deployment else _get_deployment_name()
        self.backup_dir = os.path.join(tmp_dir, str(uuid.uuid4()))
        self.path = path

    def _backup_minio(self):
        LOG.info(f"Starting backup for minio container.")
        minio_contianer_name = _get_container_id(self.deployment, "minio")
        path = os.path.join(self.backup_dir, "minio")
        os.mkdir(path)
        docker_cp_status_code, _ , docker_cp_error = popen_helper(
            ["docker", "cp", f"{minio_contianer_name}:/var/lib/minio/data/.", path]
        )
        if not docker_cp_status_code:
            LOG.info(f"Done backup for minio container.")
            return True
        else:
            LOG.error(
                f"Can't backup minio container.\n{docker_cp_error}"
            )
            return False

    def _backup_cartadb(self):
        LOG.info(f"Starting backup for cartadb container.")
        cartadb_contianer_name = _get_container_id(self.deployment, "cartadb")
        path = os.path.join(self.backup_dir, "cartadb")
        os.mkdir(path)

        if self.customer_id:
            with open(os.path.join(path, "pgdump"), "ab") as out:
                for schema in self.cartadb_system_schemas:
                    docker_exec_status_code, _ , docker_exec_error = popen_helper(
                        cmd = [
                            "docker",
                            "exec",
                            cartadb_contianer_name,
                            "sh",
                            "-c",
                            f"pg_dump -U $CARTA_ATLASDB_POSTGRES_USERNAME -d {schema.format(self.customer_id)} --create --clean --if-exists",
                        ],
                        stdout=out,
                    )
            
                    if not docker_exec_status_code:
                        LOG.info(f"Backing up for cartadb.{schema} container.")
                    else:
                        LOG.error(
                            f"Can't backup cartadb container.\n{docker_exec_error}"
                        )
                        return False
        else:
            with open(os.path.join(path, "pgdump"), "wb") as out:
                docker_exec_status_code, _ , docker_exec_error = popen_helper(
                    cmd = [
                        "docker",
                        "exec",
                        cartadb_contianer_name,
                        "sh",
                        "-c",
                        "pg_dumpall -U $CARTA_ATLASDB_POSTGRES_USERNAME --clean --if-exists",
                    ],
                    stdout=out,
                    decode_stdout=False
                )
        
            if not docker_exec_status_code:
                LOG.info(f"Done backup for cartadb container.")
                return True
            else:
                LOG.error(
                    f"Can't backup cartadb container.\n{docker_exec_error}"
                )
                return False

    def _backup_elasticsearch(self):
        LOG.info(f"Starting backup for elasticsearch container.")
        elasticsearch_contianer_name = _get_container_id(
            self.deployment, "elasticsearch"
        )
        path = os.path.join(self.backup_dir, "elasticsearch")
        os.mkdir(path)
        docker_exec_status_code, _ , docker_exec_error = popen_helper(
            [
                "docker",
                "exec",
                elasticsearch_contianer_name,
                "sh",
                "-c",
                'curl -s -XPUT localhost:9200/_snapshot/carta_backup -H "Content-Type: application/json" -d \'{"type": "fs","settings": {"location": "/backup/"}}\'',
            ]
        )
        if docker_exec_status_code:
            LOG.error(
                f"Can't backup elasticsearch container.\n{docker_exec_error}"
            )
            return False

        docker_exec_status_code, _ , docker_exec_error = popen_helper(
            [
                "docker",
                "exec",
                elasticsearch_contianer_name,
                "sh",
                "-c",
                'curl -s -XPUT localhost:9200/_snapshot/carta_backup/snapshot?wait_for_completion=true -H "Content-Type: application/json" -d \'{"indices": "*,-.*,-ilm*","ignore_unavailable": true,"include_global_state": false}\'',
            ]
        )
        if docker_exec_status_code:
            LOG.error(
                f"Can't backup elasticsearch container.\n{docker_exec_error}"
            )
            return False
        
        docker_cp_status_code, _ , docker_cp_error = popen_helper(
            ["docker", "cp", f"{elasticsearch_contianer_name}:/backup/.", path]
        )
        if docker_cp_status_code:
            LOG.error(
                f"Can't backup elasticsearch container.\n{docker_cp_error}"
            )
            return False
        
        docker_exec_status_code, _ , docker_exec_error = popen_helper(
            [
                "docker",
                "exec",
                elasticsearch_contianer_name,
                "sh",
                "-c",
                "curl -s -XDELETE localhost:9200/_snapshot/carta_backup/snapshot",
            ]
        )
        total_status_code = docker_exec_status_code
        docker_exec_status_code, _ , docker_exec_error = popen_helper(
            [
                "docker",
                "exec",
                elasticsearch_contianer_name,
                "sh",
                "-c",
                "curl -s -XDELETE localhost:9200/_snapshot/carta_backup",
            ]
        )
        total_status_code += docker_exec_status_code
               
        docker_exec_status_code, _ , docker_exec_error = popen_helper(
            [
                "docker",
                "exec",
                elasticsearch_contianer_name,
                "sh",
                "-c",
                "rm -rf /backup/*",
            ]
        )
        total_status_code += docker_exec_status_code
        if not total_status_code:
            LOG.info(f"Done backup for elasticsearch container.")
        else:
            LOG.warn(
                f"Elasticsearch container backup done, but dumps can't be cleaned.\n{docker_exec_error}"
            )
        return True

    def _backup_plugins(self):
        LOG.info(f"Getting plugins list.")
        ocean_contianer_name = _get_container_id(self.deployment, "ocean")
        path = os.path.join(self.backup_dir, "plugins.json")
        plugin_list_status_code, plugin_list_output, plugin_list_error = popen_helper(
            [
                "docker",
                "exec",
                ocean_contianer_name,
                "sh",
                "-c",
                "cd /carta/plugins && echo *",
            ],
        )
        if not plugin_list_status_code:
            LOG.info(f"Done Getting plugins list.")
        else:
            LOG.error(
                f"Can't get plugins list.\n{plugin_list_error}"
            )
            return False
        plugin_list = plugin_list_output.strip()
        plugins_json = {}
        if plugin_list != "*":
            with open(path, "w") as out:
                for plugin in plugin_list.split():
                    _ , plugin_git_output, _ = popen_helper(
                        [
                            "docker",
                            "exec",
                            ocean_contianer_name,
                            "sh",
                            "-c",
                            f"cd /carta/plugins/{plugin} && git rev-parse --short HEAD",
                        ],
                    )
                    plugin_version = (
                        plugin_git_output.strip()
                    )
                    plugins_json[plugin] = plugin_version
                out.write(json.dumps(plugins_json))
            path = os.path.join(self.backup_dir, "plugins")
            os.mkdir(path)
            popen_helper(
                ["docker", "cp", f"{ocean_contianer_name}:/carta/plugins/.", path]
            )
        return plugin_list != "*"

    def _backup_config(self):
        LOG.info(f"Starting configuration backup.")
        ocean_contianer_name = _get_container_id(self.deployment, "ocean")
        path = os.path.join(self.backup_dir, "config")
        os.mkdir(path)
        with open(os.path.join(path, "config.json"), "wb") as out:
            docker_exec_status_code, _ , docker_exec_error = popen_helper(
                [
                    "docker",
                    "exec",
                    ocean_contianer_name,
                    "sh",
                    "-c",
                    "carta show-config --deployment $CARTA_DEPLOYMENT",
                ],
                stdout=out,
                decode_stdout=False
            )
        if docker_exec_status_code:
            LOG.error(
                f"Can't perform configuration backup.\n{docker_exec_error}"
            )
            return False
        with open(os.path.join(path, "values.json"), "wb") as out:
            docker_exec_status_code, _ , docker_exec_error = popen_helper(
                [
                    "docker",
                    "exec",
                    ocean_contianer_name,
                    "sh",
                    "-c",
                    "carta show-config --deployment $CARTA_DEPLOYMENT --values",
                ],
                stdout=out,
                decode_stdout=False
            )
        if not docker_exec_status_code:
            LOG.info(f"Done configuration backup.")
        else:
            LOG.error(
                f"Can't perform configuration backup.\n{docker_exec_error}"
            )
            return False
        return True

    def _backup_semaphore(self):
        LOG.info(f"Starting backup for semaphore containers.")
        total_status_code = 0
        semaphore_volume_list_status_code, semaphore_volume_list_output, semaphore_volume_list_error = popen_helper(
            [
                "docker",
                "volume",
                "list",
                "--format",
                '{{.Name}}',
                "--filter",
                "name=jupyterhub-user",
            ],
        )
        total_status_code += semaphore_volume_list_status_code

        if semaphore_volume_list_status_code:
            LOG.error(
                f"Can't list semaphore volumes\n{semaphore_volume_list_error}"
            )
            return False


        semaphore_volume_list = semaphore_volume_list_output.strip().split()

        semaphore_volume_list_status_code, semaphore_volume_list_output, semaphore_volume_list_error = popen_helper(
            [
                "docker",
                "volume",
                "list",
                "--format",
                '{{.Name}}',
                "--filter",
                "name=shared-development",
            ],
        )
        total_status_code += semaphore_volume_list_status_code

        if semaphore_volume_list_status_code:
            LOG.error(
                f"Can't list shared development volumes\n{semaphore_volume_list_error}"
            )
            return False


        semaphore_volume_list.extend(semaphore_volume_list_output.strip().split())

        if len(semaphore_volume_list) == 0:
            return False
        
        path = os.path.join(self.backup_dir, "semaphore")
        os.mkdir(path)

        for backup_volume in semaphore_volume_list:
            backup_volume_path = os.path.join(path, backup_volume)
            os.mkdir(backup_volume_path)
            contianer_backup_status_code, _ , contianer_backup_error = popen_helper(
                [
                    "docker",
                    "run",
                    "--rm",
                    "-v",
                    f"{backup_volume}:/vol:ro",
                    "-v", 
                    f"{backup_volume_path}:/vol_backup",
                    "-w",
                    "/vol",
                    "ubuntu",
                    "tar",
                    "cf", 
                    f"/vol_backup/backup.tar",
                    "."
                ],
            )
            total_status_code += contianer_backup_status_code
            if contianer_backup_status_code:
                LOG.error(
                    f"Can't backup {backup_volume} semaphore volume.\n{contianer_backup_error}"
                )
                break
        if not total_status_code:
            LOG.info(f"Done backup for semaphore containers.")
        else:
            LOG.error(
                "Can't backup semaphore containers."
            )
            shutil.rmtree(path)
            return False
        return True
        
    def backup(self, components):

        if os.path.exists(self.backup_dir) and os.path.isdir(self.backup_dir):
            if os.listdir(self.backup_dir):
                LOG.error(
                    f"Can't backup using {self.backup_dir}, directory is not empty!"
                )
                sys.exit(1)
            else:
                shutil.rmtree(self.backup_dir)

        os.makedirs(self.backup_dir)

        if components.get("minio"):
            components["minio"] = self._backup_minio()

        if components.get("cartadb"):
            components["cartadb"] = self._backup_cartadb()

        if components.get("elasticsearch"):
            components["elasticsearch"] = self._backup_elasticsearch()

        if components.get("plugins"):
            components["plugins"] = self._backup_plugins()

        if components.get("config"):
            components["config"] = self._backup_config()
        
        if components.get("semaphore"):
            components["semaphore"] = self._backup_semaphore()
        
        path = os.path.join(self.backup_dir, "components.json")
        with open(path, "w") as out:
            out.write(json.dumps(components))

        now = datetime.now()
        dt_string = now.strftime("%d_%m_%YT%H_%M_%S")

        if not os.path.exists(self.path):
            os.makedirs(self.path)
        else:
            if os.path.isfile(self.path):
                LOG.error(
                    f"Can't create folder with name {self.path}, file already exists with the same name."
                )
                sys.exit(1)
        file_name = os.path.join(
            self.path, f"carta_{self.deployment}_backup_{dt_string}.tar.bz2"
        )
        LOG.info(f"Start archiving {file_name}")
        with tarfile.open(file_name, "w:bz2") as tar:
            tar.add(self.backup_dir, arcname="")
        LOG.info(f"Done archiving {file_name}")
        if os.path.exists(self.backup_dir) and os.path.isdir(self.backup_dir):
            shutil.rmtree(self.backup_dir)
        LOG.info(f"Backup status {json.dumps(components, indent=2)}")


class RestoreManager:
    def __init__(self, deployment, path, tmp_dir):
        self.deployment = deployment if deployment else _get_deployment_name()
        self.restore_dir = os.path.join(tmp_dir, str(uuid.uuid4()))
        self.path = path[0]
        if not os.path.exists(self.path):
            LOG.error(f"Can't restore from {self.path}. File does not exist!")
            sys.exit(1)
        self.restore_tar = tarfile.open(self.path, "r")
        self.restore_tar.extractall(self.restore_dir)

    def _restore_minio(self):
        LOG.info(f"Restoring minio container.")
        minio_contianer_name = _get_container_id(self.deployment, "minio")
        path = os.path.join(self.restore_dir, "minio")
        docker_cp_status_code, _ , docker_cp_error = popen_helper(
            ["docker", "cp", f"{path}/.", f"{minio_contianer_name}:/var/lib/minio/data"]
        )
        if not docker_cp_status_code:
            LOG.info(f"Done restoring minio container.")
        else:
            LOG.error(
                f"Can't restore minio container.\n{docker_cp_error}"
            )
            return False
        return True

    def _restore_cartadb(self):
        LOG.info(f"Restoring cartadb container.")
        cartadb_contianer_name = _get_container_id(self.deployment, "cartadb")
        path = os.path.join(self.restore_dir, "cartadb")
        popen_helper(
            [
                "docker",
                "exec",
                cartadb_contianer_name,
                "sh",
                "-c",
                "psql -d postgres -U $CARTA_ATLASDB_POSTGRES_USERNAME -c \"SELECT pg_terminate_backend(pid) FROM pg_stat_activity where application_name != 'psql';\"",
            ],
        )
        with open(os.path.join(path, "pgdump"), "r") as restore:
            docker_exec_status_code, _ , docker_exec_error = popen_helper(
                [
                    "docker",
                    "exec",
                    "-i",
                    cartadb_contianer_name,
                    "sh",
                    "-c",
                    "psql -d postgres -U $CARTA_ATLASDB_POSTGRES_USERNAME",
                ],
                stdin=restore,
                stdout=subprocess.DEVNULL,
                decode_stdout=False
            )
        if not docker_exec_status_code:
            LOG.info(f"Done restoring cartadb container.")
        else:
            LOG.error(
                f"Can't restore cartadb container.\n{docker_exec_error}"
            )
            return False
        return True

    def _restore_elasticsearch(self):
        LOG.info(f"Restoring elasticsearch container.")
        elasticsearch_contianer_name = _get_container_id(
            self.deployment, "elasticsearch"
        )
        path = os.path.join(self.restore_dir, "elasticsearch")
        docker_cp_status_code, _, docker_cp_error = popen_helper(
            ["docker", "cp", path + "/.", f"{elasticsearch_contianer_name}:/backup"]
        )
        if docker_cp_status_code:
            LOG.error(
                f"Can't backup elasticsearch container.\n{docker_cp_error}"
            )
            return False
        docker_exec_status_code, _, docker_cp_error = popen_helper(
            [
                "docker",
                "exec",
                elasticsearch_contianer_name,
                "sh",
                "-c",
                'curl -s -XPUT localhost:9200/_snapshot/carta_backup -H "Content-Type: application/json" -d \'{"type": "fs","settings": {"location": "/backup/"}}\'',
            ]
        )
        if docker_exec_status_code:
            LOG.error(
                f"Can't backup elasticsearch container.\n{docker_cp_error}"
            )
            return False
        docker_exec_status_code, _, docker_cp_error = popen_helper(
            [
                "docker",
                "exec",
                elasticsearch_contianer_name,
                "sh",
                "-c",
                "curl -s -XDELETE localhost:9200/*,-.*",
            ]
        )
        if docker_exec_status_code:
            LOG.error(
                f"Can't backup elasticsearch container.\n{docker_cp_error}"
            )
            return False
        docker_exec_status_code, _, docker_cp_error = popen_helper(
            [
                "docker",
                "exec",
                elasticsearch_contianer_name,
                "sh",
                "-c",
                'curl -s -XPOST localhost:9200/_snapshot/carta_backup/snapshot/_restore?wait_for_completion=true -H "Content-Type: application/json" -d \'{"indices": "*,-.*,-ilm*"}\'',
            ]
        )
        if docker_exec_status_code:
            LOG.error(
                f"Can't backup elasticsearch container.\n{docker_cp_error}"
            )
            return False
        
        
        docker_exec_status_code, _, _ = popen_helper(
            [
                "docker",
                "exec",
                elasticsearch_contianer_name,
                "sh",
                "-c",
                "curl -s -XDELETE localhost:9200/_snapshot/carta_backup/snapshot",
            ]
        )
        total_status_code = docker_exec_status_code
        docker_exec_status_code, _, _ = popen_helper(
            [
                "docker",
                "exec",
                elasticsearch_contianer_name,
                "sh",
                "-c",
                "curl -s -XDELETE localhost:9200/_snapshot/carta_backup",
            ]
        )
        total_status_code += docker_exec_status_code
        docker_exec_status_code, _, docker_cp_error = popen_helper(
            [
                "docker",
                "exec",
                elasticsearch_contianer_name,
                "sh",
                "-c",
                "rm -rf /backup/*",
            ]
        )
        total_status_code += docker_exec_status_code
        if not total_status_code:
            LOG.info(f"Done backup for elasticsearch container.")
        else:
            LOG.warn(
                f"Can't backup elasticsearch container.\n{docker_cp_error}"
            )
        return True

    def _restore_plugins(self):
        LOG.info(f"Restoring Plugins.")
        ocean_contianer_name = _get_container_id(self.deployment, "ocean")
        path = os.path.join(self.restore_dir, "plugins")
        docker_cp_status_code, _, docker_cp_error = popen_helper(
            ["docker", "cp", f"{path}/.", f"{ocean_contianer_name}:/carta/plugins",],
            stderr=subprocess.PIPE,
        )
        if not docker_cp_status_code:
            LOG.info(f"Done restoring plugins.")
        else:
            LOG.error(
                f"Can't restore plugins.\n{docker_cp_error}"
            )

    def _restore_semaphore(self):
        LOG.info(f"Restoring semaphore containers.")
        _ , semaphore_contianers_list_output, _ = popen_helper(
            [
                "docker",
                "ps",
                "--format",
                '{{.Names}}',
                "--filter",
                "name=semaphore-user",
            ],
        )

        semaphore_contianers_list = semaphore_contianers_list_output.strip().split()

        for container in semaphore_contianers_list:
            contianer_stop_status_code, _ , _ = popen_helper(
                [
                    "docker",
                    "stop",
                    f"{container}"
                ],
            )
            if contianer_stop_status_code != 0:
                LOG.error("Failed to stop running semaphore containers")
                return False
        
        path = os.path.join(self.restore_dir, "semaphore")

        for volume in os.listdir(path):
            popen_helper(
                [
                    "docker",
                    "volume",
                    "rm",
                    "-f",
                    f"{volume}",
                ],
            )

            popen_helper(
                [
                    "docker",
                    "volume",
                    "create",
                    f"{volume}",
                ],
            )

            restore_path = os.path.join(path, volume)

            contianer_restore_status_code, _ , _ = popen_helper(
                [
                    "docker",
                    "run",
                    "--rm",
                    "-v",
                    f"{volume}:/vol",
                    "-v", 
                    f"{restore_path}:/vol_backup",
                    "-w",
                    "/vol",
                    "ubuntu",
                    "tar",
                    "xf", 
                    f"/vol_backup/backup.tar",
                    "--strip-components", 
                    "1"
                ],
            )
        
            if not contianer_restore_status_code:
                LOG.info(f"Done restoring semaphore containers.")
            else:
                LOG.error(
                    f"Can't restore semaphore containers."
                )
                return False
        return True

    def _get_contianers_without_cartadb_elasticsearch(self):
        cartadb_contianer_name = _get_container_id(self.deployment, "cartadb")
        elasticsearch_contianer_name = _get_container_id(
            self.deployment, "elasticsearch"
        )
        container_list_status_code, container_list_output , container_list_error = popen_helper(
            ["docker", "ps", "--format", "{{.Names}}"], stdout=subprocess.PIPE,
        )
        if container_list_status_code:
            LOG.error(
                f"Can't get container list.\n{container_list_error}"
            )
            sys.exit(1)
        container_list = container_list_output.strip().split()
        container_list.remove(cartadb_contianer_name)
        container_list.remove(elasticsearch_contianer_name)
        return container_list

    def _stop_containers(self):
        LOG.info(f"Stoping application containers.")
        contianers = self._get_contianers_without_cartadb_elasticsearch()
        container_stop_command_list = ["docker", "stop"]
        container_stop_command_list.extend(contianers)
        container_stop_status_code, _, container_stop_error = popen_helper(
            container_stop_command_list
        )
        if container_stop_status_code:
            LOG.error(
                f"Can't stop application containers.\n{container_stop_error}"
            )
        for contianer in contianers:
            if contianer.startswith("semaphore-user-"):
                contianers.remove(contianer)
        return contianers

    def _start_containers(self, contianers):
        LOG.info(f"Starting application containers.")
        container_start_command_list = ["docker", "start"]
        container_start_command_list.extend(contianers)

        container_start_status_code, _ , container_start_error = popen_helper(
            container_start_command_list
        )
        if container_start_status_code:
            LOG.error(
                f"Can't start application containers.\n{container_start_error}"
            )

    def restore(self, components=None):

        components_file = open(os.path.join(self.restore_dir, "components.json"), "r")

        existing_components_file = json.loads(components_file.read())
        LOG.info(json.dumps(existing_components_file, sort_keys=True, indent=4))

        if not components:
            components = existing_components_file

        if components.get("minio"):
            if existing_components_file.get("minio"):
                self._restore_minio()
            else:
                LOG.error(
                    f"Backup file {self.restore_dir}, does not include backup for minio!"
                )

        if components.get("plugins"):
            components["plugins"] = self._restore_plugins()

        contianers = self._stop_containers()

        if components.get("cartadb"):
            if existing_components_file.get("cartadb"):
                components["cartadb"] = self._restore_cartadb()
            else:
                LOG.error(
                    f"Backup file {self.restore_dir}, does not include backup for cartadb!"
                )

        if components.get("elasticsearch"):
            if existing_components_file.get("elasticsearch"):
                components["elasticsearch"] = self._restore_elasticsearch()
            else:
                LOG.error(
                    f"Backup file {self.restore_dir}, does not include backup for elasticsearch!"
                )

        if components.get("semaphore"):
            if existing_components_file.get("semaphore"):
                components["semaphore"] = self._restore_semaphore()
            else:
                LOG.error(
                    f"Backup file {self.restore_dir}, does not include backup for semaphore!"
                )

        self._start_containers(contianers)

        LOG.info(f"Done restoring {self.path}")
        if os.path.exists(self.restore_dir) and os.path.isdir(self.restore_dir):
            shutil.rmtree(self.restore_dir)
        LOG.info(f"Restore status {json.dumps(components, indent=2)}")

from carta_client.api import BabelfishInterface
from carta_client.utils import BaseCommands, Dotfile, get_base_url_and_token


def ingest_nlp(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    babelfish_interface = BabelfishInterface(base_url, access_token)
    response = babelfish_interface.parse_ingest_nlp(
        note_id=options.note_id, sync=options.synchronous, note=options.note
    )
    print(response)


def ingest_inquiry(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    babelfish_interface = BabelfishInterface(base_url, access_token)
    response = babelfish_interface.parse_ingest_inquiry(job_id=options.job_id)
    print(response)


def ingest_ocr(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    babelfish_interface = BabelfishInterface(base_url, access_token)

    response = babelfish_interface.parse_ingest_ocr(
        note_id=options.note_id, sync=options.synchronous, note=options.note
    )
    print(response)


def ingest_note(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    babelfish_interface = BabelfishInterface(base_url, access_token)

    response = babelfish_interface.parse_ingest_note(
        note_id=options.note_id,
        sync=options.synchronous,
        note=options.note,
        params=options.p,
    )
    print(response)


class BabelfishCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)
        self.babelfish_parser = self.subparsers.add_parser(
            "babelfish", help="Babelfish Helpers"
        )
        self.babelfish_subparsers = self.babelfish_parser.add_subparsers(
            help="Babelfish Helpers"
        )

        # Add subcommands
        self.babelfish_ingest_inquiry_parser()
        self.babelfish_ingest_nlp_parser()
        self.babelfish_ingest_note_parser()
        self.babelfish_ingest_ocr_parser()

    def babelfish_ingest_inquiry_parser(self):
        # Ingest inquiry command
        ingest_inquiry_parser = self.babelfish_subparsers.add_parser(
            "ingest-inquiry",
            help="Ingest Inquiry command to see the status of previous ingest requests",
        )
        ingest_inquiry_parser.set_defaults(func=ingest_inquiry)
        self.base_url_arg(ingest_inquiry_parser)
        ingest_inquiry_parser.add_argument(
            "--job_id",
            action="store",
            required=True,
            default=None,
            help="The status of this Job Id will be returned",
        )

    def babelfish_ingest_nlp_parser(self):
        # Ingest nlp command
        ingest_nlp_parser = self.babelfish_subparsers.add_parser(
            "ingest-nlp", help="Ingest command for NLP processing of a certain file"
        )
        ingest_nlp_parser.set_defaults(func=ingest_nlp)
        self.base_url_arg(ingest_nlp_parser)
        self.synchronous_arg(ingest_nlp_parser)

        ingest_nlp_parser.add_argument(
            "--note_id",
            action="store",
            default=None,
            help="Note and Job Id for the note to be processed. If specified, make sure it is unique",
        )
        ingest_nlp_parser.add_argument(
            "note",
            nargs="+",
            help="Path to note file containing data. Must be a text (txt, html, rtf, ...) file",
        )

    def babelfish_ingest_ocr_parser(self):
        # Ingest ocr command
        ingest_ocr_parser = self.babelfish_subparsers.add_parser(
            "ingest-ocr", help="Ingest command to process a file through OCR"
        )
        ingest_ocr_parser.set_defaults(func=ingest_ocr)
        self.base_url_arg(ingest_ocr_parser)
        self.synchronous_arg(ingest_ocr_parser)

        ingest_ocr_parser.add_argument(
            "--note_id",
            action="store",
            default=None,
            help="Note and Job Id for the note to be processed. If specified, make sure it is unique",
        )
        ingest_ocr_parser.add_argument(
            "note",
            nargs="+",
            help="Path to note file containing data. Must be a acceptable for OCR (pdf, jpeg, png, ...)",
        )

    def babelfish_ingest_note_parser(self):
        # Ingest note command
        ingest_note_parser = self.babelfish_subparsers.add_parser(
            "ingest-note", help="Ingest a file note."
        )
        ingest_note_parser.set_defaults(func=ingest_note)
        self.base_url_arg(ingest_note_parser)
        self.synchronous_arg(ingest_note_parser)

        ingest_note_parser.add_argument(
            "--note_id",
            action="store",
            default=None,
            help="Note and Job Id for the note to be processed. If specified, make sure it is unique",
        )
        ingest_note_parser.add_argument(
            "--p",
            action="append",
            default=[],
            help="Add parameters for ingesting the note. e.g. --p reliability=0.8 --p patient_id=3234",
        )

        ingest_note_parser.add_argument(
            "note", nargs="+", help="Path to note file containing data.",
        )

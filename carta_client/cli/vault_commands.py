import json
import os
import subprocess
import sys
import time

from carta_client.utils import BaseCommands, Dotfile, carta_popen, LOG
from carta_client import VAULT_ADDR
import botocore.session
import base64
import requests
from urllib.parse import urljoin


def headers_to_go_style(headers: dict) -> dict:
    retval = {}
    for key, values in headers.items():
        try:
            retval[key] = values.decode()
        except:
            retval[key] = values
    return retval


def b64e(s: str) -> str:
    return base64.b64encode(s.encode()).decode()


def generate_vault_request(awsIamServerId: str, payload) -> dict:
    session = botocore.session.get_session()
    credentials = payload["Credentials"]
    session.set_credentials(
        access_key=credentials["AccessKeyId"],
        secret_key=credentials["SecretAccessKey"],
        token=credentials["SessionToken"],
    )
    client = session.create_client("sts", region_name="us-west-2")
    endpoint = client._endpoint
    operation_model = client._service_model.operation_model("GetCallerIdentity")
    try:
        request_dict = client._convert_to_request_dict({}, operation_model)
    except TypeError as e:
        request_dict = client._convert_to_request_dict({}, operation_model, endpoint.host)

    request_dict["headers"]["X-Vault-AWS-IAM-Server-ID"] = awsIamServerId

    request = endpoint.create_request(request_dict, operation_model)

    return {
        "iam_http_request_method": request.method,
        "iam_request_url": b64e(request.url),
        "iam_request_body": b64e(request.body),
        "iam_request_headers": b64e(
            json.dumps(headers_to_go_style(dict(request.headers)))
        ),
    }


def vault_login(options):
    if options.method == "azure":
        cmd = ["az", "account", "get-access-token", "-o", "json"]
    elif options.method == "aws":
        # aws sts assume-role --role-arn 'arn:aws:iam::546450435877:role/Deployment' --role-session-name="deployment-vault-login-$now"
        if options.role == "external-developer":
            role = "non-hipaa-developer"
        elif options.role == "developer":
            role = "hipaa-developer"
        elif options.role == "production-engineer":
            role = "hipaa-developer"
        elif options.role == "devops":
            role = "hipaa-devops"
        else:
            role = options.role

        role_arn = f"arn:aws:iam::118098345423:role/{role}"
        now = int(time.time())
        session_name = f"carta-client-vault-login-{now}"
        cmd = [
            "aws",
            "sts",
            "assume-role",
            "--role-arn",
            role_arn,
            "--role-session-name",
            session_name,
            "--profile",
            options.aws_profile,
        ]
    else:
        print(f"Method {options.method} unsupported. Choose azure or aws.")
        sys.exit(1)

    token_generator = carta_popen(cmd, stdout=subprocess.PIPE)
    retval = token_generator.wait()
    if retval != 0:
        print(
            f"{options.method.capitalize()} Login Token Generation Failed. Make sure you have logged in with {options.method.capitalize()}. Exiting."
        )
        sys.exit(retval)

    payload = json.load(token_generator.stdout)

    if options.method == "azure":
        jwt = payload["accessToken"]

        # Login with vault client

        cmd = [
            "vault",
            "write",
            "auth/azure/login",
            "-format=json",
            f"role={options.role}",
            f"jwt={jwt}",
        ]
        vault = carta_popen(cmd, stdout=subprocess.PIPE, quiet=True)
    elif options.method == "aws":
        """
        Obtain the following payload data 

        curl -X POST "https:vault.carta.healthcare/v1/auth/aws/login" -d 
        '{"role":"dev", "iam_http_request_method": "POST", 
        "iam_request_url": "base64 encoded", 
        "iam_request_body": "base64 encoded", 
        "iam_request_headers": "base64 encoded" }'
        """
        vault_payload = generate_vault_request("vault.carta.healthcare", payload)
        # This role denotes the name of the vault aws/role rather than the STS
        # assumed role
        role = options.role
        if options.role == "devops":
            # For compatibility with the `vault ssh --login` command, the
            # devops ssh/role is mapped to the aws/role that grants access
            role = "hipaa-devops"

        url = urljoin(VAULT_ADDR, "v1/auth/aws/login")
        vault_payload["role"] = role
        response = requests.post(url, json=vault_payload)

        if response.status_code != 200:
            print(
                f"Failed to login to vault with {options.method.capitalize()}. Make sure you're part of the appropriate group--reach out to support@carta.healthcare if you're still having trouble. Exiting.",
                file=sys.stderr,
            )
            print(response.content)
            sys.exit(1)

        token = response.json()

    with open(os.path.join(os.environ["HOME"], ".vault-token"), "w") as f:
        f.write(token["auth"]["client_token"])

    print("Login successful")


def vault_ssh(options):
    vault_token_path = os.path.join(os.environ["HOME"], ".vault-token")
    # If there's no vault-token file, login to obtain it.
    if options.login or not os.path.exists(vault_token_path):
        vault_login(options)
    ssh_path = os.path.join(os.environ["HOME"], ".ssh", "id_rsa.pub")
    if not os.path.exists(ssh_path):
        LOG.error(
            f"No SSH key is preset at {ssh_path}. Use ssh-keygen to create one, and then run command again. Exiting."
        )
        sys.exit(1)

    # To maintain backwards compatibility with the old workflow, roles from
    # vault for developers/external developers are mapped to their "shared"
    # counterparts. The --role argument began life as the Vault role to use, but
    # for backwards compatibility, this mapping is needed
    if options.role == "developer":
        role = "shared-developer"
    elif options.role == "external-developer":
        role = "shared-external-developer"
    else:
        role = options.role

    payload = {}
    public_key = ""
    token = None

    with open(os.path.join(os.environ["HOME"], ".vault-token"), "r") as file:
        token = file.read().rstrip("\n")

    with open(os.path.join(os.environ["HOME"], ".ssh/id_rsa.pub"), "r") as file:
        public_key = file.read().rstrip("\n")

    if token == None:
        print(
            f"Failed to obtain the vault token", file=sys.stderr,
        )

    # attach required "public key" payload
    payload["public_key"] = public_key
    payload["name"] = role

    if options.principal is not None:
        # Additional payload
        payload["valid_principals"] = options.principal

    url = urljoin(VAULT_ADDR, f"v1/ssh/sign/{role}")
    # Authenticate using vault token
    headers = {"X-Vault-Token": token}
    response = requests.put(url, json=payload, headers=headers)

    if response.status_code != 200:
        print(
            f"Vault SSH cerificate signing failed", file=sys.stderr,
        )
        print(response.content)
        sys.exit(1)

    # Obtain the signed key and store it locally.
    result = response.json()
    rsa_certificate = result["data"]["signed_key"]
    with open(os.path.join(os.environ["HOME"], ".ssh/id_rsa-cert.pub"), "w") as file:
        # Write new content to the file
        file.write(rsa_certificate)


class VaultCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)
        vault_parser = subparsers.add_parser("vault", help="Interact with our vault")
        vault_subparsers = vault_parser.add_subparsers(help="Interact with our vault")

        # Add subcommands

        # Vault Login
        vault_login_parser = vault_subparsers.add_parser("login", help="Login to vault")
        vault_login_parser.set_defaults(func=vault_login)
        vault_login_parser.add_argument(
            "--role",
            action="store",
            default="developer",
            help="The role to log in with.",
        )
        vault_login_parser.add_argument(
            "--method",
            action="store",
            default="aws",
            help="The method to use to authenticate (azure | aws)",
        )
        vault_login_parser.add_argument(
            "--aws-profile",
            action="store",
            default="master",
            help="AWS profile to use to authenticate with vault",
        )

        # Vault ssh
        vault_ssh_parser = vault_subparsers.add_parser(
            "ssh", help="Sign into SSH with vault (creates ~/.ssh/id_rsa-cert.pub)"
        )
        vault_ssh_parser.set_defaults(func=vault_ssh)
        vault_ssh_parser.add_argument(
            "--role",
            action="store",
            default="developer",
            help="The role to log in with. Defaults to developer.",
        )
        vault_ssh_parser.add_argument(
            "--login",
            action="store_true",
            default=False,
            help="Login before getting SSH token. "
            "This is the same as carta-client vault login --role role && carta-client vault ssh --role role",
        )
        vault_ssh_parser.add_argument(
            "--method",
            action="store",
            default="aws",
            help="The method to use to authenticate (azure | aws)",
        )
        vault_ssh_parser.add_argument(
            "--aws-profile",
            action="store",
            default="master",
            help="AWS profile to use to authenticate with vault",
        )
        vault_ssh_parser.add_argument(
            "--principal", action="store", help="SSH principal to add to the cert",
        )

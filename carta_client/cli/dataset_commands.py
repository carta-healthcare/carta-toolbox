import json
import os
import sys

from carta_client.utils import BaseCommands, Dotfile, get_base_url_and_token


def dataset_list(options):
    from carta_client import DatasetInterface, Dotfile

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    dataset_interface = DatasetInterface(base_url, access_token)
    dataset_interface.list_versions(dataset_id=options.dataset_id)


def dataset_info(options):
    from carta_client import DatasetInterface, Dotfile

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    dataset_interface = DatasetInterface(base_url, access_token)
    dataset_interface.info(dataset_id=options.dataset_id, version=options.version)


def dataset_install(options):
    from carta_client import DatasetInterface, Dotfile

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    dataset_interface = DatasetInterface(base_url, access_token)
    partitions = []
    for partition_input in options.partitions:
        if partition_input == "-":
            partition_data = json.load(sys.stdin)

        elif os.path.exists(partition_input):
            with open(partition_input) as f:
                partition_data = json.load(f)
        else:
            print(f"Partition input {partition_input} not found. Skipping.")
        if not isinstance(partition_data, list):
            print("Partition source {partition_input} did not produce a partition list")
            sys.exit(1)
        partitions += partition_data

    dataset_interface.install(
        dataset_id=options.dataset_id,
        version=options.version,
        partitions=partitions,
        synchronous=options.synchronous,
        limit=options.limit,
        wait=options.wait,
        info=options.info,
    )


def dataset_uninstall(options):
    from carta_client import DatasetInterface, Dotfile

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    dataset_interface = DatasetInterface(base_url, access_token)
    dataset_interface.uninstall(dataset_id=options.dataset_id)


def dataset_evaluate(options):
    from carta_client import DatasetInterface, Dotfile

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    dataset_interface = DatasetInterface(base_url, access_token)
    dataset_interface.evaluate(
        dataset_id=options.dataset_id,
        evaluation_ids=options.evaluation_ids,
        synchronous=options.synchronous,
    )


class DatasetCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)

        # Add subcommands
        # Dataset Subcommand
        dataset_parser = subparsers.add_parser(
            "dataset", help="Deployment dataset Management"
        )
        dataset_subparsers = dataset_parser.add_subparsers(help="dataset subcommands")
        dataset_id_arg = lambda x: x.add_argument(
            "--dataset_id",
            action="store",
            default=None,
            required=False,
            help="The dataset id (defaults to UMLS)",
        )
        dataset_version_arg = lambda x: x.add_argument(
            "--version",
            action="store",
            default=None,
            required=False,
            help="The dataset version (defaults to most recent)",
        )

        # Dataset List subcommand
        dataset_list_parser = dataset_subparsers.add_parser(
            "list", help="List all dataset versions available for deployment"
        )
        dataset_list_parser.set_defaults(func=dataset_list)
        self.base_url_arg(dataset_list_parser)
        dataset_id_arg(dataset_list_parser)

        # Dataset Info subcommand
        dataset_info_parser = dataset_subparsers.add_parser(
            "info", help="Gives info on current dataset such as version #"
        )
        dataset_info_parser.set_defaults(func=dataset_info)
        self.base_url_arg(dataset_info_parser)
        dataset_id_arg(dataset_info_parser)
        dataset_version_arg(dataset_info_parser)

        # Dataset install subcommand
        dataset_install_parser = dataset_subparsers.add_parser(
            "install", help="Install dataset for deployment"
        )
        dataset_install_parser.set_defaults(func=dataset_install)
        self.base_url_arg(dataset_install_parser)
        dataset_id_arg(dataset_install_parser)
        dataset_version_arg(dataset_install_parser)
        self.synchronous_arg(dataset_install_parser)
        self.wait_arg(dataset_install_parser)
        self.info_arg(dataset_install_parser)
        dataset_install_parser.add_argument(
            "--limit",
            action="store",
            default=None,
            type=int,
            help="Option to limit the dataset installation (what this means depends on the dataset)",
        )
        dataset_install_parser.add_argument(
            "partitions", nargs="*", help="Dataset partitions"
        )

        # Dataset uninstall/remove
        dataset_uninstall_parser = dataset_subparsers.add_parser(
            "uninstall", help="Remove dataset for deployment"
        )
        dataset_uninstall_parser.set_defaults(func=dataset_uninstall)
        self.base_url_arg(dataset_uninstall_parser)
        dataset_id_arg(dataset_uninstall_parser)

        # Dataset evaluate
        dataset_evaluate_parser = dataset_subparsers.add_parser(
            "evaluate", help="Evaluate dataset for deployment"
        )
        dataset_evaluate_parser.set_defaults(func=dataset_evaluate)
        self.base_url_arg(dataset_evaluate_parser)
        dataset_id_arg(dataset_evaluate_parser)
        dataset_version_arg(dataset_evaluate_parser)
        self.synchronous_arg(dataset_evaluate_parser)
        dataset_evaluate_parser.add_argument("--evaluation_ids", nargs="+", default=[])

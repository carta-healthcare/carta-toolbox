from carta_client.api import PenguinSearchInterface
from carta_client.utils import BaseCommands, Dotfile, get_base_url_and_token


def dictionary_search(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    penguinsearch_interface = PenguinSearchInterface(base_url, access_token)
    penguinsearch_interface.dictionary_search(options.query_string, limit=options.limit)


def dictionary_get(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    penguinsearch_interface = PenguinSearchInterface(base_url, access_token)
    penguinsearch_interface.dictionary_get(options.query)


class PenguinSearchCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)

        # Add subcommands
        # Dictionary search command
        dictionary_search_parser = subparsers.add_parser(
            "dictionary-search", help="Search UMLS dictionary for term or CUI"
        )
        dictionary_search_parser.set_defaults(func=dictionary_search)
        self.base_url_arg(dictionary_search_parser)
        dictionary_search_parser.add_argument(
            "query_string", help="Search for specified query"
        )
        dictionary_search_parser.add_argument("--limit", nargs="?", default=10)

        # Dictionary get command
        dictionary_get_parser = subparsers.add_parser(
            "dictionary-get", help="Get UMLS dictionary for term or CUI"
        )
        dictionary_get_parser.set_defaults(func=dictionary_get)
        self.base_url_arg(dictionary_get_parser)
        dictionary_get_parser.add_argument("query", help="get for specified query")

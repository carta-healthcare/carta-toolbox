import platform
import subprocess
import sys

from carta_client import TOOLBOX_ROOT
from carta_client.utils import (
    BaseCommands,
    Dotfile,
    carta_popen,
    get_username_password,
)


def _get_installed_utilities():
    has = {}
    commands = [
        "conda",
        "helm",
        "docker",
        "mc",
        "salt-call",
        "docker-compose",
        "microk8s.docker",
        "kubectl",
        "microk8s.kubectl",
        "git",
        "telepresence",
        "npm",
        "java",
        "brew",
        "snap",
        "carta",
        "vault",
        "terraform",
        "jq",
        "curl",
    ]
    for cmd in commands:
        has[cmd] = (
            carta_popen(f"which {cmd}", shell=True, stdout=subprocess.DEVNULL).wait()
            == 0
        )

    # Has Python
    # Python is obviously installed since this is running, but is the version on the path python 3?
    python = carta_popen(["python", "-V"], stdout=subprocess.PIPE)
    version = python.stdout.read().decode("utf-8").split(" ")[-1]
    has["python3"] = version.split(".")[0] == "3"

    return has


def update(options):
    branch = options.experimental or "master"
    carta_popen(
        f"cd {TOOLBOX_ROOT} && git fetch && git reset --hard && git checkout {branch} && git pull",
        shell=True,
    ).wait()


def init(options):
    """
    Init's main goal is to get salt up and running and then bring the machine up to the appropriate state
    """
    has = _get_installed_utilities()

    system = platform.platform().lower()

    # Brew if applicable
    if not has["brew"] and sys.platform == "darwin":
        print(
            'Run `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`'
        )
        input("Once installed, hit enter to continue")

    # Git
    if not has["git"]:
        if "darwin" in system:
            print("git is not installed. Installing with brew.")
            carta_popen(["brew", "install", "git"]).wait()
        elif "centos" in system:
            print("git is not installed. Installing with yum.")
            carta_popen(["yum", "install", "git"]).wait()
        elif "ubuntu" in system or "debian" in system:
            print("git is not installed. Installing with apt-get.")

            carta_popen(["apt-get", "update"]).wait()
            carta_popen(["apt-get", "install", "-y", "git"]).wait()

    # Curl
    if not has["curl"]:
        if "darwin" in system:
            print("curl is not installed. Installing with brew.")
            carta_popen(["brew", "install", "curl"]).wait()
        elif "centos" in system:
            print("curl is not installed. Installing with yum.")
            carta_popen(["yum", "install", "git"]).wait()
        elif "ubuntu" in system or "debian" in system:
            print("curl is not installed. Installing with apt-get.")
            carta_popen(["apt-get", "update"]).wait()
            carta_popen(["apt-get", "install", "-y", "curl"]).wait()

    # Initialize salt
    if not has["salt-call"]:
        carta_popen(
            "curl -L https://bootstrap.saltstack.com -o bootstrap_salt.sh && sh bootstrap_salt.sh git v2019.2.2",
            shell=True,
        ).wait()

    # Salt initialization
    print("Configuring system. This will take a few minutes...")
    carta_popen(
        f"cd {TOOLBOX_ROOT}/scripts/salt && salt-call state.apply", shell=True
    ).wait()


def login(options):
    from carta_client import Dotfile
    from carta_client.api.base import BaseInterface

    dotfile = Dotfile()
    base_url = options.base_url or dotfile.get_base_url()

    if options.show_token:
        if not dotfile.get_access_token():
            print("No access token found. Please log in first", file=sys.stderr)
        else:
            print(dotfile.get_access_token())
        sys.exit(0)

    if not base_url:
        print(
            "Must specify base url in either ~/.carta-client or with --base-url",
            file=sys.stderr,
        )
        sys.exit(1)

    username = None
    if options.username:
        username = options.username[0]
    username, password = get_username_password(dotfile, username)
    interface = BaseInterface(base_url, jwt=None)
    token = interface.login(username, password)

    dotfile.set_access_token(token["access_token"])
    if token.get("refresh_token"):
        dotfile.set_refresh_token(token["refresh_token"])
    if options.store:
        dotfile.set_username(username)
        dotfile.set_password(password)
        dotfile.set_base_url(base_url)

    dotfile.commit()


class CoreCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)

        # Update
        update_parser = subparsers.add_parser("update", help="Update Carta Client")
        update_parser.add_argument(
            "--experimental",
            default=None,
            required=False,
            action="store",
            help="Test an experimental version of carta-client",
        )
        update_parser.set_defaults(func=update)

        # Init Commmand
        init_parser = subparsers.add_parser("init", help="Initialize Carta Client")
        init_parser.set_defaults(func=init)

        # Login command
        login_parser = subparsers.add_parser("login", help="Login")
        login_parser.set_defaults(func=login)
        login_parser.add_argument(
            "--show-token",
            action="store_true",
            default=False,
            help="Print auth token to console",
        )
        login_parser.add_argument(
            "--store",
            action="store_true",
            default=False,
            help="Store username/password in dotfile",
        )
        self.base_url_arg(login_parser)
        login_parser.add_argument(
            "username", nargs="*", default=[], help="Username to log in with"
        )

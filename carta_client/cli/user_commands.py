import getpass

from carta_client.utils import (
    LOG,
    BaseCommands,
    Dotfile,
    get_base_url_and_token,
    print_response,
)


def user_create(options):

    from carta_client import Dotfile, UserInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)

    customer_id = options.customer_id
    username = options.username
    name = options.name

    valid_password = False

    while not valid_password:
        password = getpass.getpass("Create User Password: ").strip()
        password_confirm = getpass.getpass("Confirm Password: ").strip()
        valid_password = password == password_confirm

    user_interface = UserInterface(base_url, access_token)
    response = user_interface.create_user(customer_id, username, password, name)
    print(response)


def user_password(options):
    from carta_client import Dotfile, UserInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)

    username = options.username[0]
    valid_password = False
    password = None
    while not valid_password:
        password = getpass.getpass("Create User Password: ").strip()
        password_confirm = getpass.getpass("Confirm Password: ").strip()
        valid_password = password == password_confirm

    user_interface = UserInterface(base_url, access_token)
    response = user_interface.set_user_password(username, password)
    print_response(response)


def download_google_sheet(
    base_url, file_format="csv",
):
    import posixpath
    import pandas as pd
    from urllib.error import HTTPError

    """Download a Google sheet to a local (or minio) file"""
    try:
        import posixpath
        import pandas as pd
        from urllib.error import HTTPError

        """Download a Google sheet to a local (or minio) file"""
        try:
            df = pd.read_csv(posixpath.join(base_url, f"export?format={file_format}"))
            return df
        except HTTPError as err:
            print(err, f" for url {base_url} ")
            LOG.error(err)
    except:
        err = "No pandas available in this environment"
        print(err)
        LOG.error(err)


def user_bulk_upload(options):
    from carta_client import Dotfile, IdentityInterface, UserInterface
    import random

    use_pandas = True
    _iterator = None
    users_file = None
    try:
        import pandas as pd
    except:
        err = "Unable to import pandas in this environment"
        import csv

        use_pandas = False
        LOG.error(err)

    def safe_get(_dict, _key):
        result = None
        try:
            result = _dict[_key]
        except:
            result = _dict[_dict._fields.index(_key)]
        return result

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    user_interface = UserInterface(base_url, access_token)
    identity_interface = IdentityInterface(base_url, access_token)
    sheet = options.sheet
    path = options.path
    if not sheet and not path:
        err = "Need a link for a google sheet or path to a local file"
        print(err)
        LOG.error(err)

    if sheet and use_pandas:
        df = download_google_sheet(sheet)
        if df is not None and not df.empty:
            _iterator = df.itertuples()
    elif path:
        if use_pandas:
            try:
                df = pd.read_csv(path)
            except:
                df = pd.read_excel(path)
            df.drop_duplicates(inplace=True)
            _iterator = df.itertuples()
        else:
            users_file = open(path, "r")
            _iterator = csv.DictReader(users_file)

    for row in _iterator:
        # User Creation
        customer_id = safe_get(row, "customer_id")
        username = safe_get(row, "email")
        name = safe_get(row, "name")
        role = safe_get(row, "role")
        password = "%032x" % random.getrandbits(128)

        # Check to make sure the role set for the user doesn't have administrator privileges
        if role.upper() in ["CREATOR", "CARTA-SYSADM", "ADMIN", "CARTA-SYSTEM"]:
            LOG.error(
                f"Unable to set user '{username}' to role '{role.upper()}'. Admin roles must be set manually"
            )
            continue
        # Attempt to create the user
        try:
            create_user_response = user_interface.create_user(
                customer_id, username, password, name
            )
            if create_user_response == "ObjectAlreadyExistsError":
                LOG.info(f"{username} email address already exists")
                email_already_exists = True
            else:
                LOG.info(create_user_response)

        except Exception as e:
            LOG.error(e)
            if not options.create_role_for_existing_users:
                continue
        # Attempt to set user role
        try:
            add_role_response = identity_interface.add_identity_role(
                authenticator=options.authenticator,
                system_role=False,
                role_name=role,
                username=username,
                customer_id=customer_id,
            )
            LOG.info(add_role_response)
        except Exception as e:
            LOG.error(e)

    if users_file:
        users_file.close()


def identity_check_perm(options):
    from carta_client import Dotfile, IdentityInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    identity_interface = IdentityInterface(base_url, access_token)
    response = identity_interface.check_permissions(options.required_scopes)
    print_response(response)


def identity_delete(options):
    from carta_client import Dotfile, IdentityInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    identity_interface = IdentityInterface(base_url, access_token)
    deleted = identity_interface.delete_identity(options.identity_id[0])
    print_response(deleted)


def identity_add_role(options):
    from carta_client import Dotfile, IdentityInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    identity_interface = IdentityInterface(base_url, access_token)
    username = options.username[0] if options.username else None
    response = identity_interface.add_identity_role(
        authenticator=options.authenticator,
        system_role=options.system_role,
        role_name=options.role[0],
        username=username,
        customer_id=options.customer_id,
    )
    print_response(response)


def identity_remove_role(options):
    from carta_client import Dotfile, IdentityInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    identity_interface = IdentityInterface(base_url, access_token)
    identity = identity_interface.find_identity(
        options.authenticator, options.username[0], options.customer_id
    )
    response = identity_interface.remove_identity_role(
        identity_id=identity["id"],
        role_name=options.role[0],
        system=options.system_role,
    )
    print_response(response)


def role_list(options):
    from carta_client import Dotfile, RoleInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    role_interface = RoleInterface(base_url, access_token)
    response = role_interface.get_roles()
    print_response(response)


def role_set_grant(options):
    from carta_client import Dotfile, RoleInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    role_interface = RoleInterface(base_url, access_token)
    response = role_interface.create_or_edit_role(
        options.customer_id if not options.system_role else None,
        options.name[0],
        {options.path[0]: options.grants[0].split(",")},
    )
    print_response(response)


def role_remove_grant(options):
    from carta_client import Dotfile, RoleInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    role_interface = RoleInterface(base_url, access_token)
    response = role_interface.remove_role_grants(
        options.customer_id if not options.system_role else None,
        options.name[0],
        options.path,
    )
    print_response(response)


class UserCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)
        # User management command
        user_parser = subparsers.add_parser("user", help="Manage users")
        user_subparsers = user_parser.add_subparsers(help="Manage users")

        # Add subcommands

        # User create
        user_create_parser = user_subparsers.add_parser("create", help="Create user")
        user_create_parser.set_defaults(func=user_create)
        user_create_parser.add_argument(
            "--customer_id", required=True, help="New user's customer id"
        )
        user_create_parser.add_argument(
            "--username", required=True, help="New user's username"
        )
        user_create_parser.add_argument("--name", required=True, help="New user's name")
        self.base_url_arg(user_create_parser)

        # User password
        user_password_parser = user_subparsers.add_parser(
            "password", help="Set user password"
        )
        user_password_parser.set_defaults(func=user_password)
        user_password_parser.add_argument(
            "username", nargs=1, help="New user's username"
        )
        self.base_url_arg(user_password_parser)

        # Bulk Upload Users
        user_bulk_upload_parser = user_subparsers.add_parser(
            "bulk-upload", help="Create multiple users from a csv file"
        )
        user_bulk_upload_parser.add_argument(
            "--sheet", required=False, help="Upload data from a Google Sheet",
        )
        user_bulk_upload_parser.add_argument(
            "--path", required=False, help="Path to upload data from a local file"
        )
        user_bulk_upload_parser.add_argument(
            "--authenticator",
            default="local",
            help="Authenticator for identity. Defaults to local.",
        )
        user_bulk_upload_parser.add_argument(
            "--create-role-for-existing-users",
            action="store_true",
            help="Create the role even if the user alredy exists.",
        )
        user_bulk_upload_parser.set_defaults(func=user_bulk_upload)

        # Identity management command
        identity_parser = subparsers.add_parser("identity", help="Manage identities")
        identity_subparsers = identity_parser.add_subparsers(help="Manage identities")

        # Identity check-permissions
        identity_check_perm_parser = identity_subparsers.add_parser(
            "check-permissions", help="Check permissions for user"
        )
        identity_check_perm_parser.set_defaults(func=identity_check_perm)
        self.base_url_arg(identity_check_perm_parser)
        identity_check_perm_parser.add_argument(
            "required_scopes", nargs="*", help="Scopes required for permissions"
        )

        # Identity delete
        identity_delete_parser = identity_subparsers.add_parser(
            "delete", help="Check permissions for user"
        )
        identity_delete_parser.set_defaults(func=identity_delete)
        self.base_url_arg(identity_delete_parser)
        identity_delete_parser.add_argument(
            "identity_id", nargs=1, help="Identity ID to remove"
        )

        # Identity add-role
        identity_add_role_parser = identity_subparsers.add_parser(
            "add-role", help="Add role to user"
        )
        identity_add_role_parser.set_defaults(func=identity_add_role)
        self.base_url_arg(identity_add_role_parser)
        identity_add_role_parser.add_argument(
            "--customer-id",
            help="Customer ID for identity. Defaults to identity of logged in user.",
        )
        identity_add_role_parser.add_argument(
            "--customer_id",
            action="store_true",
            default=False,
            help="Add or remove a system role.",
        )
        identity_add_role_parser.add_argument(
            "--authenticator",
            default="local",
            help="Authenticator for identity. Defaults to local.",
        )
        identity_add_role_parser.add_argument("role", nargs=1, help="Role to add")
        identity_add_role_parser.add_argument(
            "username", nargs="*", help="Username for identity"
        )

        # Identity remove-role
        identity_remove_role_parser = identity_subparsers.add_parser(
            "remove-role", help="Remove role from user"
        )
        identity_remove_role_parser.set_defaults(func=identity_remove_role)
        self.base_url_arg(identity_remove_role_parser)
        identity_remove_role_parser.add_argument(
            "--customer-id",
            help="Customer ID for identity. Defaults to identity of logged in user.",
        )
        identity_remove_role_parser.add_argument(
            "--system_role",
            action="store_true",
            default=False,
            help="Add or remove a system role.",
        )
        identity_remove_role_parser.add_argument(
            "--authenticator",
            default="local",
            help="Authenticator for identity. Defaults to local.",
        )
        identity_remove_role_parser.add_argument("role", nargs=1, help="Role to remove")
        identity_remove_role_parser.add_argument(
            "username", nargs=1, help="Username for identity"
        )

        # Role management command
        role_parser = subparsers.add_parser("role", help="Manage roles")
        role_subparsers = role_parser.add_subparsers(help="Manage roles")

        # Role list command
        role_list_parser = role_subparsers.add_parser("list", help="List roles")
        role_list_parser.set_defaults(func=role_list)
        self.base_url_arg(role_list_parser)

        # Role set-grant
        role_set_grant_parser = role_subparsers.add_parser(
            "set-grant", help="Set grant"
        )
        role_set_grant_parser.set_defaults(func=role_set_grant)
        self.base_url_arg(role_set_grant_parser)
        role_set_grant_parser.add_argument(
            "--customer-id", required=False, help="Customer id"
        )
        role_set_grant_parser.add_argument(
            "--system-role",
            action="store_true",
            default=False,
            help="Add or remove a system role.",
        )
        role_set_grant_parser.add_argument("name", nargs=1, help="Name of role")
        role_set_grant_parser.add_argument("path", nargs=1, help="Path for grant")
        role_set_grant_parser.add_argument(
            "grants",
            nargs=1,
            help="Comma separated list of grants, e.g. write,read,submit,update",
        )

        # Role remove-grant
        role_remove_grant_parser = role_subparsers.add_parser(
            "remove-grant", help="Remove grant"
        )
        role_remove_grant_parser.set_defaults(func=role_remove_grant)
        self.base_url_arg(role_remove_grant_parser)
        role_remove_grant_parser.add_argument(
            "--customer-id", required=False, help="Customer id"
        )
        role_remove_grant_parser.add_argument(
            "--system-role",
            action="store_true",
            default=False,
            help="Add or remove a system role.",
        )
        role_remove_grant_parser.add_argument("name", nargs=1, help="Name of role")
        role_remove_grant_parser.add_argument(
            "path", nargs="+", help="Paths for grants to remove"
        )

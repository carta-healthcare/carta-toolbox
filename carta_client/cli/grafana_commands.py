from carta_client.api import GrafanaInterface
from carta_client.utils import (
    BaseCommands,
    Dotfile,
    get_base_url_and_token,
    print_response,
)


def grafana_datasources(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    grafana_interface = GrafanaInterface(base_url, access_token)
    response = grafana_interface.get_datasource()
    print_response(response)


def grafana_list_dashboards(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    grafana_interface = GrafanaInterface(base_url, access_token)
    response = grafana_interface.get_search()
    print_response(response)


def grafana_create_dashboard(options):
    title = options.title[0]
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    grafana_interface = GrafanaInterface(base_url, access_token)
    response = grafana_interface.create(title)
    print_response(response)


def grafana_load_default(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    grafana_interface = GrafanaInterface(base_url, access_token)
    response = grafana_interface.load_dynamic_grafana_configuration()
    print_response(response)


def grafana_remove_dashboard(options):
    uid = options.uid[0]
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    grafana_interface = GrafanaInterface(base_url, access_token)
    response = grafana_interface.delete(uid)
    print_response(response)


class GrafanaCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)

        # Grafana command
        grafana_parser = subparsers.add_parser(
            "grafana", help="Carta grafana operations"
        )
        grafana_subparser = grafana_parser.add_subparsers(
            help="carta grafana operations"
        )

        # Add subcommands

        # grafana search
        grafana_search_parser = grafana_subparser.add_parser(
            "dashboards", help="list all dashboards"
        )
        self.base_url_arg(grafana_search_parser)
        self.customer_id_arg(grafana_search_parser)
        grafana_search_parser.set_defaults(func=grafana_list_dashboards)

        # grafana datasources
        grafana_datasources_parser = grafana_subparser.add_parser(
            "datasources", help="list all datasources"
        )
        self.base_url_arg(grafana_datasources_parser)
        self.customer_id_arg(grafana_datasources_parser)
        grafana_datasources_parser.set_defaults(func=grafana_datasources)

        # grafana create
        grafana_create_parser = grafana_subparser.add_parser(
            "create", help="Create a dashboard"
        )
        self.base_url_arg(grafana_create_parser)
        self.customer_id_arg(grafana_create_parser)
        grafana_create_parser.set_defaults(func=grafana_create_dashboard)
        grafana_create_parser.add_argument(
            "title", nargs=1, help="Title of new dashboard"
        )

        # grafana load_default
        grafana_load_default_parser = grafana_subparser.add_parser(
            "load", help="load dashboard"
        )
        self.base_url_arg(grafana_load_default_parser)
        self.customer_id_arg(grafana_load_default_parser)
        grafana_load_default_parser.set_defaults(func=grafana_load_default)

        # grafana remove
        grafana_remove_parser = grafana_subparser.add_parser(
            "delete", help="Remove/Delete a dashboard"
        )
        self.base_url_arg(grafana_remove_parser)
        self.customer_id_arg(grafana_remove_parser)
        grafana_remove_parser.set_defaults(func=grafana_remove_dashboard)
        grafana_remove_parser.add_argument(
            "uid", nargs=1, help="UID of dashboard to delete"
        )

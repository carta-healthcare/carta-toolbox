import sys

from carta_client.api import RegistryInterface, ValuesetInterface
from carta_client.utils import (
    BaseCommands,
    Dotfile,
    get_base_url_and_token,
    get_interface,
    print_response,
)


def registries_upload(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    if not base_url:
        print(
            "Must specify base url in either ~/.carta-client or with --base-url",
            file=sys.stderr,
        )
        sys.exit(1)

    access_token = dotfile.get_access_token()
    if not access_token:
        print("No access token found. Please log in first", file=sys.stderr)

    registry_interface = RegistryInterface(base_url, access_token)
    response = registry_interface.upload_registry(options.sheet)
    print_response(response)


def registries_list_released(options):
    """Load an officially release revision of a Carta registry"""
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    if not base_url:
        print(
            "Must specify base url in either ~/.carta-client or with --base-url",
            file=sys.stderr,
        )
        sys.exit(1)

    access_token = dotfile.get_access_token()
    if not access_token:
        print("No access token found. Please log in first", file=sys.stderr)

    registry_interface = RegistryInterface(base_url, access_token)
    response = registry_interface.list_registry_releases(options.registry_name)
    print_response(response)


def registries_load_released(options):
    """Load an officially release revision of a Carta registry"""
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    if not base_url:
        print(
            "Must specify base url in either ~/.carta-client or with --base-url",
            file=sys.stderr,
        )
        sys.exit(1)

    access_token = dotfile.get_access_token()
    if not access_token:
        print("No access token found. Please log in first", file=sys.stderr)

    registry_interface = RegistryInterface(base_url, access_token)
    response = registry_interface.load_released_registry(
        options.registry_name, options.revision
    )
    print_response(response)


def version_list(options):
    """Will list all plugins installed and latest registries versions"""
    registry_interface_instance = get_interface(options, RegistryInterface)
    response = registry_interface_instance.list_version()
    print_response(response)


def registry_load_configuration(options):
    """Will load a registry custom configuration"""
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    if not base_url:
        print(
            "Must specify base url in either ~/.carta-client or with --base-url",
            file=sys.stderr,
        )
        sys.exit(1)

    access_token = dotfile.get_access_token()
    if not access_token:
        print("No access token found. Please log in first", file=sys.stderr)

    registry_interface = RegistryInterface(base_url, access_token)
    response = registry_interface.load_registry_configuration(options.sheets)
    print_response(response)


def download_valuesets(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)

    save_path = options.save_path

    valueset_interface = ValuesetInterface(base_url, access_token)
    response = valueset_interface.download_valuesets(save_path)
    print_response(response)


def load_valuesets(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)

    sheet = options.sheet

    valueset_interface = ValuesetInterface(base_url, access_token)
    response = valueset_interface.load_sheet(sheet)
    print_response(response)


def list_valueset_revisions(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    valueset_interface = ValuesetInterface(base_url, access_token)
    valueset_interface.list_revisions(valueset_name=options.valueset_name)


def valueset_revision_info(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    valueset_interface = ValuesetInterface(base_url, access_token)
    valueset_interface.info(
        valueset_name=options.valueset_name, revision=options.revision
    )


def load_valueset_revision(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    valueset_interface = ValuesetInterface(base_url, access_token)
    valueset_interface.load_revision(
        valueset_name=options.valueset_name, revision=options.revision,
    )


def uninstall_valueset_revision(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    valueset_interface = ValuesetInterface(base_url, access_token)
    valueset_interface.uninstall(dataset_id=options.dataset_id)


class RegistriesCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)
        self.registries_parser = subparsers.add_parser(
            "registry", help="Registry Management"
        )
        self.registries_subparsers = self.registries_parser.add_subparsers(
            help="Registry Management"
        )

        # Registries upload subcommand
        registries_upload_subcommand = self.registries_subparsers.add_parser("upload")
        registries_upload_subcommand.set_defaults(func=registries_upload)
        registries_upload_subcommand.add_argument(
            "--sheet", action="append", default=[], help="Upload from google sheets"
        )
        self.base_url_arg(registries_upload_subcommand)

        registries_list_released_subcommand = self.registries_subparsers.add_parser(
            "list"
        )
        registries_list_released_subcommand.set_defaults(func=registries_list_released)
        registries_list_released_subcommand.add_argument(
            "registry_name",
            nargs="?",
            default=None,
            help="List only the revisions for this registry.  Otherwise list all registries.",
        )
        self.base_url_arg(registries_list_released_subcommand)

        registries_load_released_subcommand = self.registries_subparsers.add_parser(
            "load"
        )
        registries_load_released_subcommand.set_defaults(func=registries_load_released)
        registries_load_released_subcommand.add_argument(
            "registry_name", nargs="?", default=None, help="Registry to load"
        )
        registries_load_released_subcommand.add_argument(
            "--revision",
            required=False,
            help="Load this specified officially released revision of the registry.  Otherwise load the most recent revision.",
        )
        self.base_url_arg(registries_load_released_subcommand)

        # Load Custom Config
        # This just forwards to registry_load_configuration() like the normal sheet loading
        # does, just adding this as an alias for usability in the prodeng team. :)
        custom_config_data = self.registries_subparsers.add_parser(
            "load-custom-config", help="Load a client's custom registry configuration"
        )
        custom_config_data.set_defaults(func=registry_load_configuration)
        custom_config_data.add_argument(
            "--sheets", required=True, help="Publicly available Google Sheet URL"
        )

        # Registry version parser
        version_parser = subparsers.add_parser(
            "version",
            help="Registry Versions available in revisions-registries.yaml and Plugins Installed",
        )
        version_parser.set_defaults(func=version_list)
        self.base_url_arg(version_parser)

        # --------------------------------------------------------------------------
        # Valuesets
        # --------------------------------------------------------------------------

        valueset_parser = subparsers.add_parser("valueset", help="Manage Valuesets")
        valueset_subparsers = valueset_parser.add_subparsers(help="Manage Valuesets")

        load_valueset_parser = valueset_subparsers.add_parser(
            "load-valueset", help="Load Valuesets"
        )
        load_valueset_parser.set_defaults(func=load_valuesets)

        load_valueset_parser.add_argument(
            "--sheet", required=True, help="Google Sheet URL"
        )
        load_valueset_parser.add_argument(
            "--base-url",
            default=self.defaults.get("base_url"),
            help="The base url that points to the deployment",
        )

        list_valuesets_parser = valueset_subparsers.add_parser(
            "list", help="Load released Valuesets revisions"
        )
        list_valuesets_parser.set_defaults(func=list_valueset_revisions)

        list_valuesets_parser.add_argument(
            "--valueset_name",
            required=False,
            default=None,
            help="List valueset revisions",
        )
        self.base_url_arg(list_valuesets_parser)

        load_valuesets_parser = valueset_subparsers.add_parser(
            "load", help="Load released Valuesets revisions"
        )
        load_valuesets_parser.set_defaults(func=load_valueset_revision)
        load_valuesets_parser.add_argument(
            "--valueset_name", required=True, help="ID of the valueset to load"
        )
        load_valuesets_parser.add_argument(
            "--revision",
            required=False,
            default=None,
            help="Semantic revision number of the valueset to load (e.g. 1.2.3)",
        )
        self.base_url_arg(load_valuesets_parser)

        valueset_info_parser = valueset_subparsers.add_parser(
            "info", help="Get info for Valueset revision (or installed valueset)"
        )
        valueset_info_parser.set_defaults(func=valueset_revision_info)
        valueset_info_parser.add_argument(
            "--valueset_name", required=True, help="ID of the valueset to get info for"
        )
        valueset_info_parser.add_argument(
            "--revision",
            required=False,
            default=None,
            help="Semantic revision number of the valueset to get info for.  Leave empty to get info for currently installed revision",
        )
        self.base_url_arg(valueset_info_parser)

        # Download Valueset
        download_valueset_parser = valueset_subparsers.add_parser(
            "download",
            help="Download Valuesets File from deployment.  Was previously called with `carta-client download download-valuesets`",
        )
        download_valueset_parser.set_defaults(func=download_valuesets)
        download_valueset_parser.add_argument(
            "--save-path", required=True, help="Local path to save the content"
        )
        self.base_url_arg(download_valueset_parser)

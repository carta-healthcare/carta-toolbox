import sys
from argparse import ArgumentParser
from carta_client.cli import (
    APIKeyCommands,
    AstrolabeCommands,
    AtlasCommands,
    BabelfishCommands,
    ConnectorCommands,
    CoreCommands,
    CubeCommands,
    DatasetCommands,
    DeploymentCommands,
    GrafanaCommands,
    HL7Commands,
    JobCommands,
    KubernetesCommands,
    PenguinSearchCommands,
    PluginCommands,
    RedisCommands,
    RegistriesCommands,
    ServerCommands,
    UserCommands,
    VaultCommands,
    BackupCommands,
)
from carta_client.utils import Dotfile


def no_command(options):
    print("No command specified. Exiting.")
    sys.exit(1)


def make_parser():
    # Initial parser and environment setup for the `carta-client` CLI
    dotfile = Dotfile()
    parser = ArgumentParser()
    parser.set_defaults(func=no_command)
    subparsers = parser.add_subparsers(help="sub-command help")

    # --------------------------------------------------------------------------
    # Core Toolbox management commands
    # --------------------------------------------------------------------------
    CoreCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Platform deployment management commands
    # --------------------------------------------------------------------------
    DeploymentCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Deployment Cartographer Plugin management commands
    # --------------------------------------------------------------------------
    PluginCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Cartographer Job management commands
    # --------------------------------------------------------------------------
    JobCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Registries Management commands
    # --------------------------------------------------------------------------
    RegistriesCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Atlas Encounter Management commands
    # --------------------------------------------------------------------------
    AtlasCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Connector commands for external data ingest via Cartographer
    # --------------------------------------------------------------------------
    ConnectorCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # API Key Management commands
    # --------------------------------------------------------------------------
    APIKeyCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Redis Management commands
    # --------------------------------------------------------------------------
    RedisCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Dataset Management commands
    # --------------------------------------------------------------------------
    DatasetCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Babelfish Management commands
    # --------------------------------------------------------------------------
    BabelfishCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # PenguinSearch Management commands
    # --------------------------------------------------------------------------
    PenguinSearchCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Cartographer OLAP Cube Management commands
    # --------------------------------------------------------------------------
    CubeCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Kubernetes Management command
    # --------------------------------------------------------------------------
    KubernetesCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Vault Management commands
    # --------------------------------------------------------------------------
    VaultCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # User Management commands
    # --------------------------------------------------------------------------
    UserCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # HL7 / Mirth Management commands
    # --------------------------------------------------------------------------
    HL7Commands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Grafana Dashboard Management commands
    # --------------------------------------------------------------------------
    GrafanaCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Astrolabe Management commands
    # --------------------------------------------------------------------------
    AstrolabeCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Server/Licence/Encryption Management commands
    # --------------------------------------------------------------------------
    ServerCommands(subparsers, dotfile)

    # --------------------------------------------------------------------------
    # Containers Backup/Restore Management commands
    # --------------------------------------------------------------------------
    BackupCommands(subparsers, dotfile)

    return parser

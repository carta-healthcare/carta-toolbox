import json
import os
import sys

from carta_client.utils import LOG, BaseCommands, Dotfile, get_base_url_and_token


def cube_update(options):
    from carta_client import CubeUpdateType, Dotfile, KnowledgeGraphInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    if not options.customer_id:
        LOG.error("customer_id argument required. Exiting.")
        sys.exit(1)

    graph_interface = KnowledgeGraphInterface(base_url, access_token)
    if options.complete:
        update_type = CubeUpdateType.COMPLETE
    else:
        update_type = CubeUpdateType.INCREMENTAL

    partitions = []
    for partition_input in options.partitions:
        if partition_input == "-":
            partition_data = json.load(sys.stdin)

        elif os.path.exists(partition_input):
            with open(partition_input) as f:
                partition_data = json.load(f)
        else:
            print(f"Partition input {partition_input} not found. Skipping.")
            sys.exit(1)

        if not isinstance(partition_data, list):
            print(
                f"Partition source {partition_input} did not produce a partition list"
            )
            sys.exit(1)
        partitions += partition_data
    response = graph_interface.cube_update(
        options.customer_id,
        cube_ids=options.cube,
        partitions=partitions,
        update_type=update_type,
        sync=options.synchronous,
    )
    print(response)


def cube_prune(options):
    from carta_client import Dotfile, KnowledgeGraphInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    if not options.customer_id:
        LOG.error("customer_id argument required. Exiting.")
        sys.exit(1)

    graph_interface = KnowledgeGraphInterface(base_url, access_token)

    partitions = []
    for partition_input in options.partitions:
        if partition_input == "-":
            partition_data = json.load(sys.stdin)

        elif os.path.exists(partition_input):
            with open(partition_input) as f:
                partition_data = json.load(f)
        else:
            print(f"Partition input {partition_input} not found. Skipping.")
            sys.exit(1)

        if not isinstance(partition_data, list):
            print(
                f"Partition source {partition_input} did not produce a partition list"
            )
            sys.exit(1)
        partitions += partition_data

    response = graph_interface.cube_prune(
        options.customer_id,
        cube_id=options.cube,
        partitions=partitions,
        prune_all=options.prune_all,
    )

    print(response)


def cube_drop(options):
    from carta_client import Dotfile, KnowledgeGraphInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    if not options.customer_id:
        LOG.error("customer_id argument required. Exiting.")
        sys.exit(1)

    graph_interface = KnowledgeGraphInterface(base_url, access_token)

    response = graph_interface.cube_drop(options.customer_id, cube_id=options.cube,)

    print(f"\nThe following cube table was dropped:\n")
    print(f"\tCube ID: {response['data']['_id']}")
    print(f"\tCube name: {response['data']['class_name']} \n")

    # response_json = json.dumps(response, indent=4)
    # print(response_json)


def cube_list(options):
    from carta_client import Dotfile, KnowledgeGraphInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    if not options.customer_id:
        LOG.error("customer_id argument required. Exiting.")
        sys.exit(1)

    graph_interface = KnowledgeGraphInterface(base_url, access_token)

    response = graph_interface.cube_list(options.customer_id)

    print("\nList of Cubes: \n")
    for cube in response["data"]:
        print("\t" + f"{cube}")
    print("\n")


def cube_info(options):
    from carta_client import Dotfile, KnowledgeGraphInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    if not options.customer_id:
        LOG.error("customer_id argument required. Exiting.")
        sys.exit(1)

    graph_interface = KnowledgeGraphInterface(base_url, access_token)

    response = graph_interface.cube_info(options.customer_id, cube_id=options.cube)

    measures = response["data"]["cube_schema"]["measures"]
    dimensions = response["data"]["cube_schema"]["dimensions"]

    print(f"\nCube's information\n")
    print(f"Cube ID: {response['data']['_id']}")
    print(f"Cube name: {response['data']['class_name']} \n")
    print(f"Measures:\n")

    for measure in measures:
        try:
            measure_name = measures[measure]["display_name"]
        except:
            measure_name = measure

        print(f"\t- {measure_name} ({measures[measure]['type']})")
    print(f"\nDimensions:\n")
    for dim in dimensions:
        print(f"\t- {dimensions[dim]['display_name']} ({dimensions[dim]['type']})")


class CubeCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)
        # Cube command
        cube_parser = subparsers.add_parser("cube", help="Interact with cubes")
        cube_subparsers = cube_parser.add_subparsers(help="Interact with cubes")

        # Add subcommands

        # cube update command
        cube_update_parser = cube_subparsers.add_parser("update", help="Update cubes")
        cube_update_parser.set_defaults(func=cube_update)
        self.base_url_arg(cube_update_parser)
        self.customer_id_arg(cube_update_parser)
        self.synchronous_arg(cube_update_parser)
        cube_update_parser.add_argument(
            "--cube", default=[], action="append", help="Cube IDs"
        )
        cube_update_parser.add_argument("partitions", nargs="*", help="Cube partitions")
        cube_update_parser.add_argument(
            "--complete",
            action="store_true",
            default=False,
            help="Run an complete update",
        )

        # cube prune command
        cube_prune_parser = cube_subparsers.add_parser(
            "prune", help="Remove cube entries"
        )
        cube_prune_parser.set_defaults(func=cube_prune)
        self.base_url_arg(cube_prune_parser)
        self.customer_id_arg(cube_prune_parser)
        cube_prune_parser.add_argument("partitions", nargs="*", help="Cube partitions")
        cube_prune_parser.add_argument(
            "--cube", default=[], action="append", help="Cube ID"
        )
        cube_prune_parser.add_argument(
            "--prune-all",
            action="store_true",
            default=False,
            required=False,
            help="Prune all partitions in the cube that are not in Atlas",
        )

        # cube drop command
        cube_drop_parser = cube_subparsers.add_parser(
            "drop",
            help="Drop table on the postgres cubes, and delete all .parquet files for the parquet cubes",
        )
        cube_drop_parser.set_defaults(func=cube_drop)
        self.base_url_arg(cube_drop_parser)
        self.customer_id_arg(cube_drop_parser)
        cube_drop_parser.add_argument("--cube", help="Cube ID")

        # cube list command
        cube_list_parser = cube_subparsers.add_parser(
            "list", help="List of all cube ids and basic info."
        )
        cube_list_parser.set_defaults(func=cube_list)
        self.base_url_arg(cube_list_parser)
        self.customer_id_arg(cube_list_parser)

        # cube info command
        cube_info_parser = cube_subparsers.add_parser(
            "info", help="Return processor info and cube schema, code location, etc."
        )
        cube_info_parser.set_defaults(func=cube_info)
        self.base_url_arg(cube_info_parser)
        self.customer_id_arg(cube_info_parser)
        cube_info_parser.add_argument("--cube", help="Cube ID")

import os
from carta_client.utils import BaseCommands, Dotfile, carta_popen


def mirth(options):
    root_path = (
        "/usr/local/bin/mirth-administrator/Mirth Connect Administrator Launcher"
    )

    carta_popen(["bash", os.path.join(root_path, "launcher")], cwd=root_path).wait()


class HL7Commands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)

        # Mirth Subcommand
        mirth_parser = subparsers.add_parser("mirth", help="Launch Mirth administrator")
        mirth_parser.set_defaults(func=mirth)

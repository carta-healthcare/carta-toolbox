from carta_client.api import JobInterface
from carta_client.utils import (
    BaseCommands,
    Dotfile,
    get_base_url_and_token,
    print_response,
)


def job_cancel(options):
    job_id = options.job_id[0]
    if options.customer_id is not None:
        customer_id = options.customer_id[0]
    else:
        customer_id = None

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)

    job_interface = JobInterface(base_url, access_token)
    response = job_interface.cancel_job(job_id, customer_id)
    print_response(response)


def job_details(options):
    job_id = options.job_id[0]
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)

    job_interface = JobInterface(base_url, access_token)

    response = job_interface.get_job_details(
        job_id, logs=options.logs, graph=options.graph
    )
    print_response(response)


class JobCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)

        # Job Command
        job_parser = subparsers.add_parser("job", help="Manage jobs")
        job_subparsers = job_parser.add_subparsers(help="Manage jobs")

        # Add subcommands

        # job cancel
        job_cancel_parser = job_subparsers.add_parser("cancel", help="cancel job")
        job_cancel_parser.set_defaults(func=job_cancel)
        self.base_url_arg(job_cancel_parser)
        job_cancel_parser.add_argument("job_id", nargs="+", help="Job id to cancel")
        job_cancel_parser.add_argument(
            "--customer_id",
            nargs="+",
            help="Customer ID to cancel jobs for. Used when job_id==all",
            required=False,
            default=None,
        )

        # job get
        job_details_parser = job_subparsers.add_parser("details", help="cancel job")
        job_details_parser.set_defaults(func=job_details)
        self.base_url_arg(job_details_parser)
        job_details_parser.add_argument(
            "--logs", action="store_true", help="Whether to get logs or not"
        )
        job_details_parser.add_argument(
            "--graph", action="store_true", help="Whether to get job graph"
        )
        job_details_parser.add_argument(
            "job_id", nargs="+", help="Job id for which to get details"
        )

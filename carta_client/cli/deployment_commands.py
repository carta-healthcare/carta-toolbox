import json
import os
import subprocess
import sys

from carta_client.deploy import DeploymentManager, DockerComposeDeploymentManager
from carta_client.utils import (
    LOG,
    BaseCommands,
    Dotfile,
    carta_popen,
    dockerize_deployment,
    get_active_branch_name,
    get_base_url_and_token,
    print_response,
)


def release_dev(options):
    carta_env = os.environ.get("CARTA_ENVIRONMENT", None)
    if carta_env not in ["development", "staging"]:
        print("\033[92m")
        print("Cant run carta-client dev on non-dev machines.")
        print(
            "to fix this, add export CARTA_ENVIRONMENT=development to your .bashrc or .zshrc"
        )
        print("\033[0m")
        return

    code_root = options.code_root or os.path.join(os.environ["HOME"], "dev", "carta")
    deployment = (
        options.deployment or os.environ.get("CARTA_DEPLOYMENT") or "development"
    )
    host_net = not options.no_hostnet
    force_recreate = False
    version = options.version or get_active_branch_name(code_root)
    attach = options.attach
    if options.all:
        attach = set(attach)
        attach.add("ocean")
        attach.add("cartographer-worker")
        attach.add("cartographer-cron")
        attach.add("mustang")
        attach = list(attach)

    config_override = None
    deployment_values_override = None
    if code_root:
        config_override_candidate = os.path.join(code_root, "carta-configuration.yml")
        if os.path.exists(config_override_candidate):
            if not host_net:
                print(
                    "Not using local configuration yaml file since we are not attaching to the host network"
                )
            else:
                config_override = config_override_candidate

        deployment_values_override_candidate = os.path.join(
            code_root, "helm-values.yml"
        )
        if os.path.exists(deployment_values_override_candidate):
            deployment_values_override = deployment_values_override_candidate
    deployment_manager = DockerComposeDeploymentManager(
        deployment,
        configuration_override=config_override,
        deployment_values_override=deployment_values_override,
        host_net=host_net,
        development_mode=True,
    )

    if options.down:
        deployment_manager.destroy()
    elif options.stop:
        deployment_manager.stop()
    else:
        deployment_manager.deploy(
            version,
            attach,
            code_root,
            force_recreate=force_recreate,
            pull=options.pull,
            separate_ocr_nlp=options.separate_ocr_nlp,
        )


def release_deploy(options):
    deployment = options.deployment
    if os.environ.get("CARTA_DEPLOYMENT"):
        deployment = os.environ.get("CARTA_DEPLOYMENT")
    if deployment is None:
        print(
            "Deployment must be specified either in the --deployment arg or in the CARTA_DEPLOYMENT env var."
        )
        sys.exit(2)

    config_override = None
    deployment_values_override = None
    if options.code_root:
        config_override_candidate = os.path.join(
            options.code_root, "carta-configuration.yml"
        )
        if os.path.exists(config_override_candidate):
            config_override = config_override_candidate

        deployment_values_override_candidate = os.path.join(
            options.code_root, "helm-values.yml"
        )
        if os.path.exists(deployment_values_override_candidate):
            deployment_values_override = deployment_values_override_candidate

    deployment_manager = DockerComposeDeploymentManager(
        deployment,
        configuration_override=config_override,
        deployment_values_override=deployment_values_override,
        ignore_version_matching=not options.version_matching,
        version_override=options.version,
    )

    containers_to_exclude = [] if options.exclude_containers is None else options.exclude_containers.split(',')

    deployment_manager.deploy(
        options.version,
        options.attach,
        options.code_root,
        force_recreate=options.force_recreate,
        pull=options.pull,
        separate_ocr_nlp=options.separate_ocr_nlp,
        containers_to_exclude=containers_to_exclude
    )

    if options.clean_old_images:
        deployment_manager.cleanup(deployment)

    if options.backup:
        from carta_client.backup import BackupManager

        bm = BackupManager(deployment, ".", "/tmp/")
        bm.backup(
            components={
                "minio": True,
                "cartadb": True,
                "elasticsearch": True,
                "plugins": True,
                "config": True,
            }
        )


def release_destroy(options):
    deployment = options.deployment
    if os.environ.get("CARTA_DEPLOYMENT"):
        deployment = os.environ.get("CARTA_DEPLOYMENT")
    if deployment is None:
        print(
            "Deployment must be specified either in the --deployment arg or in the CARTA_DEPLOYMENT env var."
        )
        sys.exit(2)

    deployment_manager = DockerComposeDeploymentManager(deployment)
    deployment_manager.destroy(options.version)


def deployment_setup(options):
    from carta_client.deployment import DeploymentSetup

    DeploymentSetup(options.deployment).run()


def preflight_check(options):
    from carta_client import CartographerStatusInterface, Dotfile

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)

    status_interface = CartographerStatusInterface(base_url, access_token)

    response = status_interface.get_preflight_check()
    response_data = response["data"]
    if options.json:
        print_response(response)
    else:

        def print_status(group):

            ready = []
            not_ready = []
            stat = response_data.get(group, {})
            for processor_id, status in stat.items():
                if status.get("ready"):
                    ready.append(processor_id)
                else:
                    not_ready.append(processor_id)

            print(f"  Ready: {', '.join(ready)}")
            if not_ready:
                print(f"  Not Ready:")
                for processor_id in not_ready:
                    print(f"    * {processor_id}: {stat[processor_id].get('check')}")

        print("Plugins: ")
        print_status("plugins")
        print()
        print("Cubes: ")
        print_status("cubes")
        print()
        print("Connectors: ")
        print_status("connectors")


def refresh_certs(options):
    deployment_name = options.deployment
    if deployment_name:
        deployment_name = dockerize_deployment(deployment_name)
        lb_name = f"{deployment_name}_load-balancer_1"
    else:
        docker_ps_command = carta_popen(["docker", "ps"], stdout=subprocess.PIPE)
        docker_ps_command.wait()

        grep_command = carta_popen(
            ["grep", "load-balancer"],
            stdin=docker_ps_command.stdout,
            stdout=subprocess.PIPE,
        )
        grep_command.wait()
        lb_cut_command = carta_popen(
            ["cut", "-d", " ", "-f", "1"],
            stdin=grep_command.stdout,
            stdout=subprocess.PIPE,
        )
        lb_cut_command.wait()
        lb_name = lb_cut_command.stdout.read().decode("utf-8")
        if lb_name:
            lb_name = lb_name.split()[0]
        else:
            LOG.error("No load balancer container is running. Exiting.")
            sys.exit(1)

    carta_popen(
        [
            "docker",
            "exec",
            "-it",
            lb_name,
            "/bin/bash",
            "-c",
            "refresh-certs.sh && touch /etc/traefik.d/traefik-provider.yml",
        ]
    ).wait()


def refresh_wiseor_certs(options):

    # Delete old keystore
    carta_popen(["rm", "-f", "/tmp/keystore.jks"], stdout=subprocess.PIPE).wait()

    # Get WiseOR certificate and key from vault
    wiseor_certs_cmd = carta_popen(
        ["vault", "read", "-format", "json", "secret/certs/wiseor.com"],
        stdout=subprocess.PIPE,
    )
    wiseor_certs_retval = wiseor_certs_cmd.wait()
    if wiseor_certs_retval != 0:
        print("Failed to retruve wiseor certificates from vault")
        sys.exit(wiseor_certs_retval)

    # Get WiseOR SSO certificate from vault
    wiseor_sso_cert_cmd = carta_popen(
        ["vault", "read", "-format", "json", "secret/certs/wiseor-keystore"],
        stdout=subprocess.PIPE,
    )
    wiseor_sso_cert_retval = wiseor_sso_cert_cmd.wait()
    if wiseor_sso_cert_retval != 0:
        print("Vault read failed")
        sys.exit(wiseor_sso_cert_retval)

    # Write WiseOR certificate and key to /tmp
    wiseor_certs = json.load(wiseor_certs_cmd.stdout)
    wiseor_fullchain = wiseor_certs["data"]["fullchain.pem"]
    wiseor_privkey = wiseor_certs["data"]["privkey.pem"]

    with open(os.path.join("/tmp", "fullchain.pem"), "w") as f:
        f.write(wiseor_fullchain)

    with open(os.path.join("/tmp", "privkey.pem"), "w") as f:
        f.write(wiseor_privkey)

    # Write WiseOR SSO certificate to /tmp
    sso_cert = json.load(wiseor_sso_cert_cmd.stdout)
    uvm_cert = sso_cert["data"]["uvm-saml"]
    with open(os.path.join("/tmp", "msft.cer"), "w") as f:
        f.write(uvm_cert)

    # Create pkcs12 file from WiseOR certificate and key to import in keystore
    pkcs_cmd = carta_popen(
        [
            "openssl",
            "pkcs12",
            "-export",
            "-in",
            "/tmp/fullchain.pem",
            "-inkey",
            "/tmp/privkey.pem",
            "-out",
            "/tmp/wild.wiseor.com.p12",
            "-name",
            "server",
            "-passout",
            "pass:ousuy&ohp3quuS",
        ],
        stdout=subprocess.PIPE,
    )
    pkcs_retval = pkcs_cmd.wait()
    if pkcs_retval != 0:
        print(
            "PKCS command failed. Please check if openssl is installed, and configured"
        )
        sys.exit(pkcs_retval)

    # Import pkcs12 wiseor file to keystore
    keystore_wiseor_cert_cmd = carta_popen(
        [
            "keytool",
            "-noprompt",
            "-importkeystore",
            "-alias",
            "server",
            "-srckeystore",
            "/tmp/wild.wiseor.com.p12",
            "-srcstorepass",
            "ousuy&ohp3quuS",
            "-srcstoretype",
            "PKCS12",
            "-destkeystore",
            "/tmp/keystore.jks",
            "-deststoretype",
            "PKCS12",
            "-deststorepass",
            "ousuy&ohp3quuS",
        ]
    )
    keystore_wiseor_cert_retval = keystore_wiseor_cert_cmd.wait()
    if keystore_wiseor_cert_retval != 0:
        print("Importing certs into keystore failed")
        sys.exit(keystore_wiseor_cert_retval)

    # Import pkcs12 wiseor file to keystore
    keystore_wiseor_sso_cert_cmd = carta_popen(
        [
            "keytool",
            "-noprompt",
            "-importcert",
            "-trustcacerts",
            "-alias",
            "Microsoft Azure Federated SSO Certificate",
            "-file",
            "/tmp/msft.cer",
            "-destkeystore",
            "/tmp/keystore.jks",
            "-deststoretype",
            "PKCS12",
            "-deststorepass",
            "ousuy&ohp3quuS",
        ]
    )
    keystore_wiseor_sso_cert_retval = keystore_wiseor_sso_cert_cmd.wait()
    if keystore_wiseor_sso_cert_retval != 0:
        print("Importing UVM certs into keystore failed")
        sys.exit(keystore_wiseor_sso_cert_retval)

    print(
        "Keystore Updated, with latest certificates. New Keystore found at /tmp/keystore.jks"
    )


def file_carta(options):
    """Carta-client sub command that creates a .carta-client file in home directory with values from vault. If .carta-client file already exists,
    file-carta command does nothing unless "--force" flag is used, if "--force" flag used, file-carta overwrites current .carta-client file in home directory.
    Requires deployment flag.  If "--deployment development" then, vault values are ingored and default values used to write .carta-client file.
    default values :
        base_url = 'https://local.carta.healthcare:4430/'
        default_password = '<check Slack / internal documentation>'
        username = 'creator@carta.healthcare'

    .carta-client file used for credentials/login


    Args:
        requires --deployment flag
        options (argparse.Namespace): optional arguments to be parsed: '--force'  forces overwrite of .carta-client file even if it exsists
    """

    file_exists = os.path.exists("~/.carta-client")
    print("file exists")
    deployment = options.deployment or os.environ.get("CARTA_DEPLOYMENT")
    if deployment is None:
        LOG.error(
            "Deployment must be specified either in the --deployment arg or in the CARTA_DEPLOYMENT env var."
        )
        sys.exit(2)

    deployment_manager = DeploymentManager(deployment)
    vault_secret_client = deployment_manager.vault_client
    deployment_prefix = f"secret/deployments/{options.deployment}"
    connection_key = deployment_prefix + "/carta-configuration"
    connection_info = vault_secret_client.read_secret(connection_key)
    if deployment != "development":
        base_url = connection_info.get("external_base_url")
        default_password = connection_info.get("default_password")
        username = "creator@carta.healthcare"
    else:
        base_url = "https://local.carta.healthcare:4430/"
        default_password = None
        username = "creator@carta.healthcare"

    try:
        if file_exists and not options.force:
            print("~/.carta-client file already exists")
        else:
            carta_client_dictionary = {
                "base_url": base_url,
                "username": username,
                "password": default_password,
            }
            print(carta_client_dictionary)
            with open("~/.carta-client", "w") as outfile:
                json.dump(carta_client_dictionary, outfile)
            print("~/.carta-client file written")
            if default_password is None:
                print("You must fill the development default password manually")
    except Exception as e:
        print(e)
        print("Error in carta-client file-client subcommand processing")


def doctor(options):
    from carta_client.doctor import Doctor

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)

    pre_only = options.pre_only

    print("Running Pre-flight Checks")
    carta_doctor = Doctor(base_url)
    for name, result in carta_doctor.preflight_check():
        print(f"...{name} -> {result.result} / {result.context}")
    if not pre_only:
        carta_doctor = Doctor(base_url, jwt=access_token)
        print("Running Post-Deployment Checks")
        for name, result in carta_doctor.post_deployment_check():
            print(f"...{name} -> {result.result} / {result.context}")


class DeploymentCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)

        # Release command
        release_parser = subparsers.add_parser(
            "release", help="Manage a released deployment"
        )
        release_subparsers = release_parser.add_subparsers(
            help="Manage a released deployment"
        )

        # Add subcommands
        dev_parser = subparsers.add_parser("dev", help="Development wraper")
        dev_parser.set_defaults(func=release_dev)
        dev_parser.add_argument(
            "--deployment",
            default=None,
            required=False,
            action="store",
            help="The name of the deployment to install",
        )
        dev_parser.add_argument(
            "--force-recreate",
            default=False,
            action="store_true",
            help="Force service recreation",
        )
        dev_parser.add_argument(
            "--all",
            default=False,
            action="store_true",
            help="Put all core services in dev mode",
        )
        dev_parser.add_argument(
            "--no-hostnet",
            default=False,
            action="store_true",
            help="Don't run dev mode on the host network",
        )
        dev_parser.add_argument(
            "--version", help="Version to deploy (overrides vault configuration)",
        )
        dev_parser.add_argument(
            "--code-root",
            help="Code root to mount into any containers that are attached to (see `attach` arg)",
        )
        dev_parser.add_argument(
            "--pull",
            action="store_true",
            default=False,
            help="Pull images before launching",
        )
        dev_parser.add_argument(
            "attach", nargs="*", help="Services to attach to once launched",
        )
        dev_parser.add_argument(
            "--down", action="store_true", default=False, help="Destroy dev environment"
        )
        dev_parser.add_argument(
            "--stop", action="store_true", default=False, help="Pause dev environment"
        )
        dev_parser.add_argument(
            "--separate-ocr-nlp",
            action="store_true",
            default=False,
            help="have ocr and nlp workers run separately",
        )

        # Release Deploy command
        release_deploy_parser = release_subparsers.add_parser(
            "deploy", help="Deploy release to kubernetes and set relevant domain names"
        )
        release_deploy_parser.set_defaults(func=release_deploy)
        release_deploy_parser.add_argument(
            "--deployment",
            default=None,
            required=False,
            action="store",
            help="The name of the deployment to install",
        )
        release_deploy_parser.add_argument(
            "--dry-run", default=False, action="store_true", help="Do a dry run",
        )
        release_deploy_parser.add_argument(
            "--force-recreate",
            default=False,
            action="store_true",
            help="Force service recreation",
        )
        release_deploy_parser.add_argument(
            "--clean-old-images",
            default=False,
            action="store_true",
            help="Prevents reclaiming disk spaced used by images not bound to containers",
        )
        release_deploy_parser.add_argument(
            "--version", help="Version to deploy (overrides vault configuration)",
        )
        release_deploy_parser.add_argument(
            "--code-root",
            help="Code root to mount into any containers that are attached to (see `attach` arg)",
        )
        release_deploy_parser.add_argument(
            "--attach", nargs="*", help="Services to attach to once launched",
        )
        release_deploy_parser.add_argument(
            "--pull",
            action="store_true",
            default=False,
            help="Pull images before launching",
        )
        release_deploy_parser.add_argument(
            "--no-version-matching",
            action="store_true",
            default=False,
            help="Ignore carta-client version matching",
        )
        release_deploy_parser.add_argument(
            "--version-matching",
            action="store_true",
            default=False,
            help="Perform carta-client version matching",
        )
        release_deploy_parser.add_argument(
            "--backup",
            action="store_true",
            default=False,
            help="Get backup for the current deployment",
        )
        release_deploy_parser.add_argument(
            "--separate-ocr-nlp",
            action="store_true",
            default=False,
            help="have ocr and nlp workers run separately",
        )
        release_deploy_parser.add_argument(
            "--exclude-containers",
            help="comma separated list of containers to exclude",
            action="store"
        )

        # Release Destroy Command
        release_destroy_parser = release_subparsers.add_parser(
            "destroy", help="Tear down release and reclaim resources"
        )
        release_destroy_parser.set_defaults(func=release_destroy)
        release_destroy_parser.add_argument(
            "--deployment",
            default=None,
            required=False,
            action="store",
            help="The name of the deployment to install",
        )
        release_destroy_parser.add_argument(
            "--version", help="Version to deploy (overrides vault configuration)",
        )

        deployment_setup_parser = subparsers.add_parser(
            "deployment-setup", help="Manage deployments"
        )
        deployment_setup_parser.set_defaults(func=deployment_setup)
        deployment_setup_parser.add_argument(
            "--deployment", default=None, help="The deployment to setup"
        )

        # Refresh Certs Subcommand
        refresh_certs_parser = subparsers.add_parser(
            "refresh-certs", help="refresh certs"
        )
        refresh_certs_parser.set_defaults(func=refresh_certs)
        self.deployment_arg(refresh_certs_parser)

        # Refresh WiseOR Certs Subcommand
        refresh_wiseor_certs_parser = subparsers.add_parser(
            "refresh-wiseor-certs", help="refresh wiseor certs"
        )
        refresh_wiseor_certs_parser.set_defaults(func=refresh_wiseor_certs)
        self.deployment_arg(refresh_wiseor_certs_parser)

        # Create .carta-client file
        create_carta_client_file_parser = subparsers.add_parser(
            "file-carta",
            help="creates ~/.carta-client file with vault values for base_url, username, password",
        )
        create_carta_client_file_parser.set_defaults(func=file_carta)
        create_carta_client_file_parser.add_argument(
            "--force",
            default=False,
            action="store_true",
            help="Force Flag overwrite current ~/.carta-client file",
        )
        create_carta_client_file_parser.add_argument(
            "--deployment", required=True, help="Deployment to show"
        )

        # Status Command
        status_parser = subparsers.add_parser("status", help="Check System Status")
        status_subparsers = status_parser.add_subparsers(help="Check System Status")

        # Status preflight-check
        preflight_check_parser = status_subparsers.add_parser(
            "preflight-check", help="Perform pre-flight check"
        )
        preflight_check_parser.set_defaults(func=preflight_check)
        self.base_url_arg(preflight_check_parser)
        preflight_check_parser.add_argument(
            "--json", default=False, action="store_true", help="Output in JSON format"
        )

        # Doctor Subcommand
        doctor_parser = subparsers.add_parser("doctor", help="check deployment health")
        doctor_parser.set_defaults(func=doctor)
        doctor_parser.add_argument(
            "--pre-only", action="store_true", help="Only run pre-deployment checks"
        )
        self.base_url_arg(doctor_parser)

import json
import os
import sys

from carta_client.utils import (
    LOG,
    BaseCommands,
    Dotfile,
    get_base_url_and_token,
    print_response,
)


def connector_file(options):
    from carta_client import Dotfile, KnowledgeGraphInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    graph_interface = KnowledgeGraphInterface(
        base_url, access_token, refresh_token=dotfile.get_refresh_token()
    )

    files = []
    for f in options.inputs:
        filename = os.path.split(f)[1]
        files.append((filename, open(f, "rb")))

    response = graph_interface.connector_file(
        options.customer_id,
        source=options.source,
        limit=options.limit,
        files=files,
        synchronous=options.synchronous,
        preflight_check=options.preflight_check,
    )
    print_response(response)


def connector_json(options):
    from carta_client import Dotfile, KnowledgeGraphInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    graph_interface = KnowledgeGraphInterface(base_url, access_token)
    if len(options.inputs) == 0:
        data = {}
    elif options.inputs[0] == "-":
        data = json.load(sys.stdin)
    else:
        with open(options.inputs[0]) as f:
            data = json.load(f)

    response = graph_interface.connector_json(
        options.customer_id,
        source=options.source,
        payload=data,
        synchronous=options.synchronous,
        preflight_check=options.preflight_check,
    )
    print_response(response)


def load_npi(options):
    from carta_client.services.npi import LoadNPiSheetData

    # Initialize
    lnsd = LoadNPiSheetData(
        customer_id=options.customer_id,
        google_sheet=options.sheet,
        load_npi=options.loadnpi,
        delete_files=options.deletefiles,
    )
    lnsd.process_request()

    if lnsd.load_npi:
        options.source = "carta/cardio/provider/ncdr/npi/file"
        for npi_file in lnsd.npi_files:
            options.inputs = [npi_file]
            options.limit = 20

            # Execute connector for loading NPI data
            LOG.info(f"Loading NPI data from file {npi_file}")
            connector_file(options)
    else:
        LOG.info(
            "Skipping loading of the NPI files as '--loadnpi' parameter was not passed"
        )

    # Delete Files
    lnsd.delete_generated_files()


class ConnectorCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)

        # Connector command
        connector_parser = subparsers.add_parser(
            "connector", help="Connect data to graph"
        )
        connector_subparsers = connector_parser.add_subparsers(
            help="Connect data to graph"
        )

        # Add subcommands

        # Connector file command
        connector_file_parser = connector_subparsers.add_parser(
            "file", help="Connect data by uploading a file to connector"
        )
        connector_file_parser.set_defaults(func=connector_file)
        self.base_url_arg(connector_file_parser)

        self.customer_id_arg(connector_file_parser)
        self.synchronous_arg(connector_file_parser)
        connector_file_parser.add_argument(
            "--source",
            required=True,
            help="Unique ID for the data. "
            "Used to route data to appropriate connector",
        )
        connector_file_parser.add_argument(
            "--preflight-check",
            action="store_true",
            default=False,
            help="Run preflight check on event instead of actual processing",
        )
        connector_file_parser.add_argument(
            "--limit",
            required=False,
            default=None,
            help="Send a limit to the connector API",
        )
        connector_file_parser.add_argument("inputs", nargs="*", help="Files to upload")

        connector_json_parser = connector_subparsers.add_parser(
            "json", help="Connect data by uploading json to connector"
        )
        connector_json_parser.set_defaults(func=connector_json)
        self.base_url_arg(connector_json_parser)
        self.customer_id_arg(connector_json_parser)
        self.synchronous_arg(connector_json_parser)
        connector_json_parser.add_argument(
            "--source",
            required=True,
            help="Unique ID for the data. "
            "Used to route data to appropriate connector",
        )
        connector_json_parser.add_argument(
            "--preflight-check",
            default=False,
            action="store_true",
            help="Preflight check",
        )

        connector_json_parser.add_argument("inputs", nargs="*", help="Files to upload")

        # -----------------------------------------------------------------------------
        # Load npi command
        # -----------------------------------------------------------------------------
        load_npi_parser = connector_subparsers.add_parser(
            "npisheet", help="Connect data by uploading NPI googlesheet to connector"
        )
        load_npi_parser.set_defaults(func=load_npi)
        self.base_url_arg(load_npi_parser)
        self.synchronous_arg(load_npi_parser)
        load_npi_parser.add_argument(
            "--customer_id",
            action="store",
            default=self.defaults.get("customer_id"),
            required=True,
            help="The customer_id",
        )
        load_npi_parser.add_argument(
            "--sheet", required=True, help="Google sheet for loading npi data",
        )
        load_npi_parser.add_argument(
            "--loadnpi",
            action="store_true",
            default=False,
            help="upload new npi dataset",
        )
        load_npi_parser.add_argument(
            "--deletefiles",
            action="store_true",
            default=False,
            help="delete generated files",
        )
        load_npi_parser.add_argument(
            "--preflight-check",
            default=False,
            action="store_true",
            help="Preflight check (applicable if '--loadnpi' is specified)",
        )

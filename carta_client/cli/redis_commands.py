from carta_client.api import RedisInterface
from carta_client.utils import BaseCommands, Dotfile, print_response, get_interface


def redis(options):
    redis_interface_instance = get_interface(options, RedisInterface)

    if hasattr(options, "delete") and options.delete:
        delete_argument = options.delete
        if delete_argument == "all":
            init_schemas = (
                True
                if hasattr(options, "init_schemas") and options.init_schemas
                else False
            )
            response = redis_interface_instance.kill_em_all(init_schemas)
        elif str(delete_argument).isnumeric():
            reg_sch_id = delete_argument
            response = redis_interface_instance.delete_redis_data_by_reg_sch_id(
                reg_sch_id
            )
        else:
            raise ValueError(
                "Invalid Arg. Must be 'all' or an integer 'registry schema id'"
            )
    elif hasattr(options, "registries") and options.registries:
        registries_argument = options.registries
        if registries_argument == "all":
            response = redis_interface_instance.get_list_redis_data()
        elif str(registries_argument).isnumeric():
            reg_sch_id = registries_argument
            response = redis_interface_instance.get_redis_data_by_reg_sch_id(reg_sch_id)
        else:
            raise ValueError(
                "Invalid Arg. Must be 'all' or an integer 'registry schema id'"
            )
    elif hasattr(options, "valuesets") and options.valuesets:
        valuesets_arg = options.valuesets[0]
        if valuesets_arg == "all":
            response = redis_interface_instance.delete_all_redis_valuesets()
        elif str(valuesets_arg).isnumeric():
            reg_sch_id = int(valuesets_arg)
            response = redis_interface_instance.delete_redis_valuesets_by_reg_sch_id(
                reg_sch_id
            )
        else:
            raise ValueError(
                "Invalid Arg. Must be 'all' or an integer 'registry schema id'"
            )
    else:
        raise NotImplementedError
    print_response(response)


class RedisCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)
        # Redis parser
        self.redis_parser = subparsers.add_parser("redis", help="Redis Stuff")
        self.redis_parser.set_defaults(func=redis)
        self.base_url_arg(self.redis_parser)
        self.redis_subparsers = self.redis_parser.add_subparsers(
            help="Redis DB Management"
        )

        # Add subcommands

        # Redis Delete
        redis_delete_parser = self.redis_subparsers.add_parser(
            "delete", help="delete all of it"
        )
        redis_delete_parser.add_argument(
            "delete", nargs="?", default=False, help="Delete or not delete",
        )
        redis_delete_parser.add_argument(
            "--init-schemas",
            action="store_true",
            default=False,
            help="Init all schemas after delete all",
        )
        redis_delete_parser.add_argument(
            "--valuesets",
            action="store",
            nargs=1,
            default=False,
            help="Delete or not delete",
        )

        # Redis Get Registries
        redis_get = self.redis_subparsers.add_parser(
            "registries", help="Get Registry Schema Redis Data"
        )
        redis_get.add_argument(
            "registries", nargs="?", default=False, help="Get Data Registry",
        )

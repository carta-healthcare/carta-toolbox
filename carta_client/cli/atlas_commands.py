import json

from carta_client.api import AtlasInterface
from carta_client.utils import (
    BaseCommands,
    Dotfile,
    get_base_url_and_token,
    print_response,
)


def encounter_delete(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)

    atlas_interface = AtlasInterface(base_url, access_token)
    retval = {}
    for encounter_id in options.encounter_ids:
        response = atlas_interface.delete_encounter(encounter_id, hard=options.hard)
        encounter_deletion = response
        retval[encounter_id] = encounter_deletion
    print_response(retval)


def encounter_import(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)

    atlas_interface = AtlasInterface(base_url, access_token)
    retval = {}

    for encounter_json_file in options.encounter_jsons:
        with open(encounter_json_file, "r") as encounter_data_file:
            raw_encounter_data = encounter_data_file.read()
            raw_encounter_data = json.loads(raw_encounter_data)

            # TODO (EN-3302): Format raw_encounter_data to be posted to create encounter API

            create_encounter_dict = {
                "input_encounter_data": raw_encounter_data,
                "data": {},
                "validate": False,
            }

            retval[encounter_json_file] = atlas_interface.create_encounter(
                encounter_data=create_encounter_dict
            )

    print_response(retval)


def encounter_recommendations(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    atlas_interface = AtlasInterface(base_url, access_token)
    response = atlas_interface.get_recommendations(options.encounter_id)
    print_response(response)


def encounter_remove_sor_meta(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    atlas_interface = AtlasInterface(base_url, access_token)
    registry_program = options.registry_program
    version_number = options.version_number
    customer_id = options.customer_id
    mrns = options.mrns
    response = atlas_interface.remove_sor_meta(
        mrns, registry_program, version_number, customer_id
    )
    print_response(response)


def import_patient_encounter(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    atlas_interface = AtlasInterface(base_url, access_token)
    registry_program = options.registry_program
    file_path = options.file_path
    customer_id = options.customer_id
    response = atlas_interface.import_patient_encounter(
        registry_program, file_path, customer_id
    )
    print_response(response)


class AtlasCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)

        # --------------------------------------------------------------------------
        # Atlas encounter import/exports
        # --------------------------------------------------------------------------

        # Encounter Subcommand
        encounter_parser = subparsers.add_parser(
            "encounter", help="Encounter Management"
        )
        encounter_subparsers = encounter_parser.add_subparsers(
            help="Encounter Management"
        )

        # Encounter import subcommand
        # TODO (EN-3302): Add params for importing encounter here
        encounter_import_subcommand = encounter_subparsers.add_parser(
            "import", help="Import an encounter from JSON file"
        )
        encounter_import_subcommand.set_defaults(func=encounter_import)
        encounter_import_subcommand.add_argument(
            "--encounter_jsons", required=True, nargs="+", help="JSON file to import"
        )
        self.base_url_arg(encounter_import_subcommand)
        self.customer_id_arg(encounter_import_subcommand)

        # Encounter delete subcommand
        encounter_delete_subcommand = encounter_subparsers.add_parser(
            "delete", help="Delete an encounter by UUID"
        )
        encounter_delete_subcommand.set_defaults(func=encounter_delete)
        encounter_delete_subcommand.add_argument(
            "--hard", action="store_true", default=False, help="Hard delete"
        )
        encounter_delete_subcommand.add_argument(
            "encounter_ids", nargs="+", help="Encounter IDs to delete"
        )
        self.base_url_arg(encounter_delete_subcommand)
        self.customer_id_arg(encounter_delete_subcommand)

        encounter_recommendations_subcommand = encounter_subparsers.add_parser(
            "recommendations",
            help="Get the Cartographer recommendations for an Atlas encounter",
        )
        encounter_recommendations_subcommand.set_defaults(
            func=encounter_recommendations
        )
        encounter_recommendations_subcommand.add_argument(
            "encounter_id", help="Atlas encounter UUID to get recommendations for"
        )
        self.base_url_arg(encounter_recommendations_subcommand)
        self.customer_id_arg(encounter_recommendations_subcommand)

        # Encounter remove sor meta command
        encounter_remove_sor_meta_subcommand = encounter_subparsers.add_parser(
            "remove-sor-meta", help="Remove sor meta for an encounter by UUID"
        )
        encounter_remove_sor_meta_subcommand.set_defaults(
            func=encounter_remove_sor_meta
        )
        encounter_remove_sor_meta_subcommand.add_argument(
            "--mrns", required=True, nargs="+", help="MRN"
        )
        encounter_remove_sor_meta_subcommand.add_argument(
            "--registry-program", required=True, help="Registry Program name"
        )
        encounter_remove_sor_meta_subcommand.add_argument(
            "--version_number", required=True, help="Registry Program version"
        )
        self.base_url_arg(encounter_remove_sor_meta_subcommand)
        self.customer_id_arg(encounter_remove_sor_meta_subcommand)

        # Import Patient Encounter
        encounter_import_patient_subcommand = encounter_subparsers.add_parser(
            "import-patient", help="Import Patient Encounters"
        )
        encounter_import_patient_subcommand.set_defaults(func=import_patient_encounter)
        encounter_import_patient_subcommand.add_argument(
            "--registry-program", required=True, help="Registry Program name"
        )
        encounter_import_patient_subcommand.add_argument(
            "--file-path", required=True, help="CSV File Path"
        )
        self.base_url_arg(encounter_import_patient_subcommand)
        self.customer_id_arg(encounter_import_patient_subcommand)

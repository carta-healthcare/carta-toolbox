import os
import sys

SCRIPT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
if SCRIPT_ROOT not in sys.path:
    sys.path.append(SCRIPT_ROOT)

from carta_client.utils import BaseCommands, Dotfile, carta_popen


class KubernetesCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)
        self.kubernetes_parser = self.subparsers.add_parser(
            "kubernetes", help="Kubernetes Helpers"
        )
        self.kubernetes_subparser = self.kubernetes_parser.add_subparsers(
            help="Kubernetes Helpers"
        )

        # Add subcommands
        self.kubernetes_install_operators_parser()

    def kubernetes_install_operators_parser(self):
        kubernetes_operator_installer_parser = self.kubernetes_subparser.add_parser(
            "install-operators", help="Install Operators"
        )
        kubernetes_operator_installer_parser.set_defaults(
            func=self.kubernetes_install_operators_function
        )

        kubernetes_operator_installer_parser.add_argument(
            "--namespace", required=False, help="Namespace, defaults to kube-system"
        )

    def kubernetes_install_operators_function(self, options):
        ns = options.namespace or "carta-operators"

        commands = []

        helm_repos = {
            "stable": "https://kubernetes-charts.storage.googleapis.com/",
            "prometheus-community": "https://prometheus-community.github.io/helm-charts",
            "appscode": "https://charts.appscode.com/stable/",
            "banzaicloud-stable": "https://kubernetes-charts.banzaicloud.com/",
        }

        commands += [
            "helm repo add {} {}".format(name, repo)
            for name, repo in helm_repos.items()
        ]

        commands += ["helm repo update"]

        commands += ["kubectl create ns {} 2> /dev/null".format(ns)]

        # Kubedb - redis, postgres, mysql, mongo, elasticsearch, memcached
        # https://kubedb.com/docs/0.12.0/setup/install/
        commands += [
            "helm install kubedb-operator appscode/kubedb -n {}".format(ns),
            "kubectl rollout status -w deployment/kubedb-operator -n {}".format(ns),
            "helm install kubedb-catalog appscode/kubedb-catalog -n {} --wait".format(
                ns
            ),
        ]

        # StorageOS Operator
        commands += [
            f"kubectl apply -f {SCRIPT_ROOT}/carta_client/kubernetes/operators/storageos/storageos.yml",
            "kubectl rollout status -w deployment/storageos-cluster-operator -n storageos-operator",
        ]

        for cmd in commands:
            carta_popen(["bash", "-c", cmd]).wait()

        # Uninstalling Kubedb
        # curl -fsSL https://raw.githubusercontent.com/kubedb/cli/0.12.0/hack/deploy/kubedb.sh | bash -s -- --uninstall --purge
        # kubectl delete podsecuritypolicies -l "app=kubedb-catalog"

import os
import subprocess
import sys

from carta_client.deploy import ContainerRegistryManager, VaultSecretClient
from carta_client.proxy import trust_proxy
from carta_client.utils import LOG, BaseCommands, Dotfile, carta_popen


def license_docker_login(options):
    crm = ContainerRegistryManager()
    crm.docker_login(options.profile)


def license_vault_login(options):
    # Read server config
    vault_role_id = os.environ.get("VAULT_ROLE_ID", "None")
    vault_secret_id = os.environ.get("VAULT_SECRET_ID", "None")
    deployment = os.environ.get("CARTA_DEPLOYMENT", "None")

    if "None" in [vault_role_id, vault_secret_id, deployment]:
        raise Exception(
            f"Missing environment variables - "
            + f"VAULT_ROLE_ID: {vault_role_id}, "
            + f"VAULT_SECRET_ID: {vault_secret_id}, "
            + f"CARTA_DEPLOYMENT: {deployment}"
        )

    # Vault authentication using role and secret
    vault = VaultSecretClient(deployment, vault_role_id, vault_secret_id,)

    # Store token
    os.environ["VAULT_TOKEN"] = vault.token

    # Log into vault
    v_login = subprocess.Popen(["vault", "login", vault.token], stdout=subprocess.PIPE)

    retval = v_login.wait()
    if retval != 0:
        print("Failed to login to vault with approle.")
        sys.exit(retval)

    result = v_login.stdout.read(35).decode("utf-8")
    LOG.info(result)


def encrypt(options):
    device = options.device  # The name of the source device
    container_name = options.container  # The name of the encrypted device
    mount_point = options.mount_point

    if options.cmd[0] == "setup":
        # Optionally provision a file to use as the encrypted FS
        if options.provision:
            if not options.size:
                print("--provision requires you to also specify the --size option")
                sys.exit(1)

            megabytes = options.size
            cmd = [
                "sudo",
                "dd",
                "if=/dev/zero",
                "bs=1M",
                f"count={megabytes}",
                f"of={options.device}",
            ]
            try:
                os.makedirs(os.path.dirname(device))
            except OSError:
                pass
            carta_popen(cmd, check=True)

        # Encrypt it
        cmd = ["sudo", "cryptsetup", "-v", "luksFormat", device]
        carta_popen(cmd, check=True)

        # Open it
        cmd = ["sudo", "cryptsetup", "luksOpen", device, container_name]
        carta_popen(cmd, check=True)

        # Format it
        cmd = ["sudo", "mkfs.ext4", f"/dev/mapper/{container_name}"]
        carta_popen(cmd, check=True)

        # Close it
        cmd = ["sudo", "cryptsetup", "luksClose", container_name]
        carta_popen(cmd, check=True)

    elif options.cmd[0] == "mount":
        if not mount_point:
            print("--mount-point required for mount command. Exiting.")
            sys.exit(2)

        # Open it
        cmd = ["sudo", "cryptsetup", "luksOpen", device, container_name]
        carta_popen(cmd, check=True)

        try:
            os.makedirs(mount_point)
        except OSError:
            pass

        # Mount it
        cmd = ["sudo", "mount", f"/dev/mapper/{container_name}", mount_point]
        carta_popen(cmd, check=True)

    elif options.cmd[0] == "unmount":
        if not mount_point:
            print("--mount-point required for mount command. Exiting.")
            sys.exit(2)

        # Unmount
        cmd = ["sudo", "umount", mount_point]
        carta_popen(cmd).wait()

        # Close it
        cmd = ["sudo", "cryptsetup", "luksClose", container_name]
        carta_popen(cmd, check=True)

    else:
        print(f"Unknown command {options.cmd[0]}. Exiting.")
        sys.exit(2)


class ServerCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)

        # Server command
        server_parser = subparsers.add_parser("server", help="Manage Server")
        server_subparsers = server_parser.add_subparsers(help="Manage Server")

        # License command
        license_parser = subparsers.add_parser(
            "license", help="Manage platform licenses"
        )
        license_subparser = license_parser.add_subparsers(
            help="Manage platform licenses"
        )

        # Add subcommands

        # Server trust proxy
        server_trust_proxy_parser = server_subparsers.add_parser(
            "trust-proxy", help="Trust proxy root certificate"
        )
        server_trust_proxy_parser.set_defaults(func=trust_proxy)
        server_trust_proxy_parser.add_argument(
            "--get-os-cabundle",
            action="store_true",
            help="""
                figures out and returns the cabundle path by OS
                """,
        )
        server_trust_proxy_parser.add_argument(
            "--host",
            help="""
                custom host to check ssl against
                """,
        )
        server_trust_proxy_parser.add_argument(
            "--cabundle-path",
            help="""
                Trusted certificates file path. Required if env VAULT_ACCESS_CERTIFICATE_PEM/CARTA_DEPLOYMENT_CA_BUNDLE_PEM is not defined.
                Usually $(python -c 'import certifi;print(certifi.where())')
                """,
        )

        license_docker_login_parser = license_subparser.add_parser(
            "docker-login", help="Log docker daemon into Carta's container registry"
        )
        license_docker_login_parser.set_defaults(func=license_docker_login)
        license_docker_login_parser.add_argument(
            "--profile", default=None, help="The AWS profile to use"
        )

        license_vault_login_parser = license_subparser.add_parser(
            "vault-login", help="Log into vault"
        )
        license_vault_login_parser.set_defaults(func=license_vault_login)

        # Encrypt Subcommand
        encrypt_parser = subparsers.add_parser("encrypt", help="encrypt")
        encrypt_parser.set_defaults(func=encrypt)
        encrypt_parser.add_argument("--device", required=True, help="Device to mount")
        encrypt_parser.add_argument(
            "--container", required=True, help="Name of encrypted container"
        )
        encrypt_parser.add_argument(
            "--provision",
            action="store_true",
            default=False,
            help="Provision a block of storage",
        )
        encrypt_parser.add_argument(
            "--size", required=False, default=None, help="Size of provisioned block"
        )
        encrypt_parser.add_argument(
            "--mount-point",
            required=True,
            default=None,
            help="Mount point for encrypted volume",
        )
        encrypt_parser.add_argument("cmd", nargs=1, help="setup | mount | unmount")

from carta_client.api import BaseInterface
from carta_client.utils import BaseCommands, Dotfile, get_base_url_and_token


def api_key_list(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)

    interface = BaseInterface(base_url, jwt=access_token)
    token = interface.get_api_keys()

    print(token)


def api_key_delete(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)

    interface = BaseInterface(base_url, jwt=access_token)
    interface.delete_api_key(options.api_key_ids)


def api_key_create(options):
    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    user_config = None
    if any([options.username, options.customer_id, options.auth]):
        if not all([options.username, options.customer_id, options.auth]):
            raise ValueError(
                f" we need to either pass all of the args for  username, auth and customer_id or None of those"
            )
        user_config = {
            "username": options.username,
            "authenticator": options.auth,
            "customer_id": options.customer_id,
        }

    interface = BaseInterface(base_url, jwt=access_token)
    token = interface.create_api_key(
        options.name, options.scopes, user_config=user_config
    )

    print(token)


class APIKeyCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)
        # api-key command
        self.api_key_parser = self.subparsers.add_parser("api-key", help="API Key")
        self.api_key_subparsers = self.api_key_parser.add_subparsers(
            help="API Key Management"
        )

        # Add subcommands

        # api-key create
        api_key_create_parser = self.api_key_subparsers.add_parser(
            "create", help="Create API key"
        )
        api_key_create_parser.set_defaults(func=api_key_create)
        api_key_create_parser.add_argument(
            "--name", default=False, help="Friendly name of the API key",
        )
        api_key_create_parser.add_argument(
            "--scopes", nargs="*", default=["trino"], help="Scopes for the API key",
        )
        api_key_create_parser.add_argument(
            "--username",
            default=False,
            help="Username for api-key access",  # ex: test_user@customer_domain.org
        )
        api_key_create_parser.add_argument(
            "--auth",
            default=False,
            help="Authenticator for api-key access",  # ex: local
        )
        api_key_create_parser.add_argument(
            "--customer_id",
            default=False,
            help="customer_id for api-key access",  # ex: local
        )
        self.base_url_arg(api_key_create_parser)

        # api-key get
        api_key_list_parser = self.api_key_subparsers.add_parser(
            "list", help="List API key"
        )
        api_key_list_parser.set_defaults(func=api_key_list)
        self.base_url_arg(api_key_list_parser)

        # api-key delete
        api_key_delete_parser = self.api_key_subparsers.add_parser(
            "delete", help="Get API key"
        )
        api_key_delete_parser.set_defaults(func=api_key_delete)
        api_key_delete_parser.add_argument(
            "api_key_ids", nargs="*", help="API key ids to delete"
        )
        self.base_url_arg(api_key_delete_parser)

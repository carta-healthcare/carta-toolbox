import os
import sys

try:
    import yaml
except ImportError:
    pass

from carta_client.deploy import DeploymentManager
from carta_client.plugins import PluginManager
from carta_client.utils import (
    BaseCommands,
    Dotfile,
    carta_popen,
    dockerize_deployment,
    get_base_url_and_token,
    print_response,
)


def read_ssh_keys(dotfile: Dotfile, private=True, public=False):
    file_name = dotfile.get_ssh_filename()
    ssh_key, pub_ssh_key = None, None
    if private:
        with open(os.path.join(os.environ["HOME"], ".ssh", file_name)) as f:
            ssh_key = f.read()
    if public:
        with open(os.path.join(os.environ["HOME"], ".ssh", f"{file_name}.pub")) as f:
            pub_ssh_key = f.read()
    return ssh_key, pub_ssh_key


def plugin_fetch(options):
    deployment_manager = DeploymentManager(options.deployment)
    plugin_manager = PluginManager(deployment_manager)

    plugin = options.plugin[0]
    try:
        plugin_name, version = plugin.split(":")
    except ValueError:
        plugin_name = plugin
        version = None

    return plugin_manager.fetch_plugin(plugin_name, version)


def plugin_create(options):
    project_name = options.project_name[0]
    default_project_directory = None
    if "CARTA_PLUGIN_DIRECTORIES" in os.environ:
        directories = os.environ["CARTA_PLUGIN_DIRECTORIES"].split(":")
        default_project_directory = os.path.join(directories[0], project_name)
    else:
        default_project_directory = os.path.join("/carta/plugins", project_name)
    project_directory = options.project_directory or default_project_directory
    from carta_client.plugins import PluginManager

    manager = PluginManager()
    manager.create_plugin(project_name, project_directory)


def plugin_install(options):
    from carta_client import Dotfile, PluginInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    plugin_interface = PluginInterface(base_url, access_token)

    if options.pip:
        deployment = options.deployment or os.environ.get("CARTA_DEPLOYMENT")
        if deployment is None:
            print(
                "Deployment must be specified either in the --deployment arg or in the CARTA_DEPLOYMENT env var."
            )
            sys.exit(2)

        deployment_manager = DeploymentManager(deployment)
        plugin_manager = PluginManager(deployment_manager)
        pypi_url, plugins = plugin_manager.get_install_arguments()
        if options.plugins:
            plugins = options.plugins
        plugin_interface.install_plugins(pypi_url, plugins)

    else:
        plugins = set()
        manifests = []
        ssh_key, _ = read_ssh_keys(dotfile=dotfile, private=True) if options.upload_ssh_key else (None, None)

        for plugin in options.plugins:
            if plugin.endswith(".yml") or plugin.endswith(".yaml"):
                with open(plugin) as f:
                    manifests.append(yaml.load(f, yaml.SafeLoader))
            else:
                plugins.add(plugin)

        for manifest in manifests:
            for k, v in manifest.items():
                plugins.add(k)

        branch = None
        if options.branch:
            branch = options.branch
        for plugin in plugins:
            response = plugin_interface.install_plugin(
                plugin, ssh_key=ssh_key, branch=branch
            )

    plugin_interface.reload_plugins()
    response = plugin_interface.check_plugins_status()
    print(f"Validating {plugins} install")
    plugins_intalled = response.get("plugins")
    for plugin in plugins:
        if plugin not in plugins_intalled:
            print(f"Plugin {plugin} was not installed")
        else:
            current_plugin = plugins_intalled[plugin]
            if "error" in current_plugin:
                print(
                    f"Couldn't verify plugin {plugin}, please verify manually if the installation was successful."
                )
            else:
                branch_name = current_plugin.get("branch_name")
                commit_hash = current_plugin.get("commit_hash")
                if branch and branch != branch_name:
                    print(
                        f"Installed branch name: {branch_name} doesn't match user's requested branch name: {branch}, current commit hash is {commit_hash}, try uninstalling and reinstalling the plugin"
                    )
                else:
                    print(f"{plugin} with branch {branch_name} installed successfully")


def plugin_list(options):
    from carta_client import Dotfile, PluginInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    plugin_interface = PluginInterface(base_url, access_token)
    response = plugin_interface.list_plugins()
    print_response(response)


def plugin_uninstall(options):
    if not options.deployment:
        print(
            "Deployment is required--either specify using --deployment "
            "or by setting a default deployment in ~/.carta-client",
            file=sys.stderr,
        )
        sys.exit(1)
    deployment = dockerize_deployment(options.deployment)

    for plugin in options.plugins:

        docker_base_cmd = [
            "docker",
            "exec",
            "-it",
            f"{deployment}_ocean_1",
            "bash",
            "-c",
        ]

        carta_popen(docker_base_cmd + [f"rm -rf /carta/plugins/{plugin}"]).wait()


def plugin_update(options):
    from carta_client import Dotfile, PluginInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    plugin_interface = PluginInterface(base_url, access_token)

    ssh_key, _ = read_ssh_keys(dotfile=dotfile, private=True) if options.upload_ssh_key else (None, None)

    plugins = []
    manifests = []
    for plugin in options.plugins:
        if plugin.endswith(".yml") or plugin.endswith(".yaml"):
            with open(plugin) as f:
                manifests.append(yaml.load(f, yaml.SafeLoader))
        else:
            plugins.append(plugin)

    if len(manifests) > 0:
        for manifest in manifests:
            plugin_interface.update_plugins_by_manifest(manifest, ssh_key=ssh_key)
    if len(plugins) > 0:
        plugin_interface.update_plugins(plugins, ssh_key=ssh_key)


def plugin_update_all(options):
    from carta_client.plugins import update_all_local_plugins

    update_all_local_plugins(options)


def plugin_reload(options):
    from carta_client import Dotfile, PluginInterface

    dotfile = Dotfile()
    base_url = options.base_url or dotfile.get_base_url()
    if not base_url:
        print(
            "Must specify base url in either ~/.carta-client.json or with --base-url",
            file=sys.stderr,
        )
        sys.exit(1)

    access_token = dotfile.get_access_token()
    if not access_token:
        print("No access token found. Please log in first", file=sys.stderr)
        sys.exit(1)

    plugin_interface = PluginInterface(base_url, access_token)
    return plugin_interface.reload_plugins()


class PluginCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)
        # Plugin management command
        plugin_parser = subparsers.add_parser("plugin", help="Manage plugins")
        plugin_subparsers = plugin_parser.add_subparsers(help="Manage plugins")

        # Add subcommands

        # Plugin create command
        plugin_create_parser = plugin_subparsers.add_parser(
            "create", help="Create plugin"
        )
        plugin_create_parser.set_defaults(func=plugin_create)
        plugin_create_parser.add_argument(
            "--project-directory",
            default=None,
            help="Directory in which to create new project. Defaults to /carta/plugins",
        )
        plugin_create_parser.add_argument("project_name", nargs=1, help="Project name")

        # Plugin fetch command
        plugin_fetch_parser = plugin_subparsers.add_parser("fetch", help="Fetch plugin")
        plugin_fetch_parser.set_defaults(func=plugin_fetch)
        plugin_fetch_parser.add_argument("plugin", nargs=1, help="Plugin name")
        self.deployment_arg(plugin_fetch_parser)

        # Plugin install command
        plugin_install_parser = plugin_subparsers.add_parser(
            "install", help="Install plugin"
        )
        plugin_install_parser.set_defaults(func=plugin_install)
        self.base_url_arg(plugin_install_parser)
        plugin_install_parser.add_argument(
            "--upload-ssh-key",
            action="store_true",
            default=False,
            help="Upload your default SSH key to authenticate a git checkout",
        )
        plugin_install_parser.add_argument(
            "--pip",
            action="store_true",
            default=False,
            help="Installs plugins using carta's private pypi server.",
        )
        plugin_install_parser.add_argument(
            "--deployment",
            default=None,
            required=False,
            action="store",
            help="The name of the deployment to install",
        )
        plugin_install_parser.add_argument(
            "--branch",
            action="store",
            required=False,
            default=None,
            help="Installs branch provided as name.",
        )
        plugin_install_parser.add_argument(
            "plugins",
            nargs="*",
            help="The plugins to install. Either give a full git repository URL or just a plugin name, "
            "in which case it will assume the carta bitbucket workspace is the root",
        )

        # Plugin list command
        plugin_list_parser = plugin_subparsers.add_parser("list", help="List Plugins")
        plugin_list_parser.set_defaults(func=plugin_list)
        self.base_url_arg(plugin_list_parser)

        # Plugin uninstall command
        plugin_uninstall_parser = plugin_subparsers.add_parser(
            "uninstall", help="Uninstall plugin"
        )
        plugin_uninstall_parser.set_defaults(func=plugin_uninstall)
        plugin_uninstall_parser.add_argument(
            "--deployment",
            action="store",
            default=self.defaults.get("deployment"),
            required=True,
            help="The deployment to uninstall the plugin from",
        )
        plugin_uninstall_parser.add_argument(
            "plugins", nargs="+", help="The name of the plugins to uninstall",
        )

        # Plugin update command
        plugin_update_parser = plugin_subparsers.add_parser(
            "update", help="Update plugin"
        )
        plugin_update_parser.set_defaults(func=plugin_update)
        plugin_update_parser.add_argument(
            "--upload-ssh-key",
            action="store_true",
            default=False,
            help="Upload your default SSH key to authenticate a git checkout",
        )
        plugin_update_parser.add_argument(
            "plugins", nargs="+", help="The name of the plugins to update",
        )

        # Plugin reload
        plugin_reload_parser = plugin_subparsers.add_parser(
            "reload", help="Reload plugins"
        )
        plugin_reload_parser.set_defaults(func=plugin_reload)
        plugin_reload_parser.add_argument(
            "--base-url",
            default=self.defaults.get("base_url"),
            help="The base url that points to the Cartographer deployment",
        )

        # Plugin Update-All command
        plugin_update_all_parser = plugin_subparsers.add_parser(
            "update-all", help="Update all plugins from Bitbucket"
        )
        plugin_update_all_parser.add_argument(
            "--force", action="store_true", help="Remove any local changes."
        )
        plugin_update_all_parser.add_argument(
            "--verbose",
            action="store_true",
            help="Display additional logging information.",
        )
        plugin_update_all_parser.set_defaults(func=plugin_update_all)

import json
import shutil
import subprocess
import sys
from urllib.parse import urlparse

from carta_client.utils import BaseCommands, Dotfile, get_base_url_and_token


def astrolabe_show_jdbc(options):

    from carta_client import Dotfile, IdentityInterface

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    base_url_parsed = urlparse(options.base_url)
    identity_interface = IdentityInterface(base_url, access_token)
    response = identity_interface.check_permissions(
        required_grants=[], required_scopes=[]
    )
    jdbc_url = f"jdbc:trino://{base_url_parsed.hostname}:8086?SSL=true"
    username = f"{response['data']['user']['customer_id']}/{response['data']['user']['username']}"
    password = (
        "<your api key> (run `carta-client api-key create` if you don't have one yet)"
    )

    print(
        json.dumps(
            {"jdbc_url": jdbc_url, "username": username, "password": password}, indent=4
        )
    )


def astrolabe_cli(options):

    from carta_client import Dotfile, IdentityInterface

    trino_path = shutil.which("trino")
    java_path = shutil.which("java")

    if not trino_path:
        print(
            "Trino CLI not installed. Please follow the instructions at https://trino.io/docs/current/installation/jdbc.html"
        )
        sys.exit(1)
    if not java_path:
        print("Java not installed. Please install Java before continuing.")
        sys.exit(1)

    dotfile = Dotfile()
    base_url, access_token = get_base_url_and_token(options, dotfile)
    base_url_parsed = urlparse(options.base_url)
    identity_interface = IdentityInterface(base_url, access_token)
    response = identity_interface.check_permissions(
        required_grants=[], required_scopes=[]
    )
    server_url = f"https://{base_url_parsed.hostname}:8086"
    username = f"{response['data']['user']['customer_id']}/{response['data']['user']['username']}"
    # password = "<your api key> (run `carta-client api-key create` if you don't have one yet)"

    cmd = [trino_path, "--server", server_url, "--user", username, "--password"]
    if options.statements:
        for statement in options.statements:
            cmd += ["--execute", statement]

    retval = subprocess.Popen(cmd).wait()
    if retval != 0:
        print("Server connection failed")


class AstrolabeCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)
        # Astrolabe
        astrolabe_parser = subparsers.add_parser("astrolabe", help="Manage Astrolabe")
        astrolabe_subparsers = astrolabe_parser.add_subparsers(help="Manage Astrolabe")

        # Add subcommands
        astrolabe_show_jdbc_parser = astrolabe_subparsers.add_parser(
            "show-jdbc", help="Return JDBC Connection Parameters"
        )
        astrolabe_show_jdbc_parser.set_defaults(func=astrolabe_show_jdbc)
        self.base_url_arg(astrolabe_show_jdbc_parser)

        astrolabe_cli_parser = astrolabe_subparsers.add_parser("cli", help="Start CLI")
        astrolabe_cli_parser.set_defaults(func=astrolabe_cli)
        self.base_url_arg(astrolabe_cli_parser)
        astrolabe_cli_parser.add_argument(
            "statements", nargs="*", help="SQL statements to run"
        )

from carta_client.utils import BaseCommands, Dotfile


def backup(options):
    from carta_client.database import DatabaseOps

    ops = DatabaseOps(options.deployment)
    ops.handle(options.restore)


def local_backup(options):
    from carta_client.backup import BackupManager

    bm = BackupManager(options.deployment, options.path, options.tmp_path)
    if options.customer_id:
        bm.customer_id = options.customer_id

    if options.all or not (
        options.all
        or options.minio
        or options.cartadb
        or options.elasticsearch
        or options.plugins
        or options.config
        or options.semaphore
    ):
        bm.backup(
            components={
                "minio": True,
                "cartadb": True,
                "elasticsearch": True,
                "plugins": True,
                "config": True,
                "semaphore": True,
            }
        )
    else:
        bm.backup(
            components={
                "minio": options.minio,
                "cartadb": options.cartadb,
                "elasticsearch": options.elasticsearch,
                "plugins": options.plugins,
                "config": options.config,
                "semaphore": options.semaphore,
            }
        )


def local_restore(options):
    from carta_client.backup import RestoreManager

    rm = RestoreManager(options.deployment, options.path, options.tmp_path)
    if options.all or not (
        options.all
        or options.minio
        or options.cartadb
        or options.elasticsearch
        or options.plugins
        or options.semaphore
    ):
        rm.restore()
    else:
        rm.restore(
            components={
                "minio": options.minio,
                "cartadb": options.cartadb,
                "elasticsearch": options.elasticsearch,
                "plugins": options.plugins,
                "semaphore": options.semaphore,
            }
        )


class BackupCommands(BaseCommands):
    def __init__(self, subparsers, dotfile: Dotfile):
        super().__init__(subparsers, dotfile)

        # Backup command
        backup_parser = subparsers.add_parser("backup", help="Create/restore backup")
        backup_parser.set_defaults(func=backup)

        # Add subcommands
        backup_parser.add_argument(
            "--deployment", default=None, help="Deployment to operate on"
        )
        backup_parser.add_argument(
            "--restore", default=None, help="dump file to restore"
        )

        local_backup_parser = subparsers.add_parser(
            "local-backup", help="Create/restore local backups"
        )
        local_backup_parser.set_defaults(func=local_backup)
        local_backup_parser.add_argument(
            "--deployment", default=None, help="Deployment to operate on"
        )
        local_backup_parser.add_argument(
            "--path", default=".", help="Path for the backup"
        )
        local_backup_parser.add_argument(
            "--tmp-path", default="/tmp/backup", help="Path to generate artifacts"
        )
        local_backup_parser.add_argument(
            "--all", default=False, help="Backup everything", action="store_true"
        )
        local_backup_parser.add_argument(
            "--minio", default=False, help="Backup minio", action="store_true"
        )
        local_backup_parser.add_argument(
            "--cartadb",
            default=False,
            help="Backup all schemas from cartadb",
            action="store_true",
        )
        local_backup_parser.add_argument(
            "--customer_id",
            default=None,
            help="Backup system schemas and customer schemas cartadb",
        )
        local_backup_parser.add_argument(
            "--elasticsearch",
            default=False,
            help="Backup Elasticsearch",
            action="store_true",
        )
        local_backup_parser.add_argument(
            "--plugins", default=False, help="Backup Plugins list", action="store_true"
        )
        local_backup_parser.add_argument(
            "--config",
            default=False,
            help="Backup deployment configurations",
            action="store_true",
        )
        local_backup_parser.add_argument(
            "--semaphore",
            default=False,
            help="Backup Semaphore data",
            action="store_true",
        )

        local_restore_parser = subparsers.add_parser(
            "local-restore", help="Create/restore local backups"
        )
        local_restore_parser.set_defaults(func=local_restore)
        local_restore_parser.add_argument(
            "--deployment", default=None, help="Deployment to operate on"
        )
        local_restore_parser.add_argument(
            "path", nargs=1, help="Path for the backup file to restore"
        )
        local_restore_parser.add_argument(
            "--tmp-path", default="/tmp/backup", help="Path to generate artifacts"
        )
        local_restore_parser.add_argument(
            "--all", default=False, help="Restore everything", action="store_true"
        )
        local_restore_parser.add_argument(
            "--minio", default=False, help="Restore minio", action="store_true"
        )
        local_restore_parser.add_argument(
            "--cartadb", default=False, help="Restore cartadb", action="store_true"
        )
        local_restore_parser.add_argument(
            "--elasticsearch",
            default=False,
            help="Restore Elasticsearch",
            action="store_true",
        )
        local_restore_parser.add_argument(
            "--plugins", default=False, help="Restore Plugins list", action="store_true"
        )
        local_restore_parser.add_argument(
            "--semaphore", default=False, help="Restore Semaphore data", action="store_true"
        )

import base64  # Protection
import copy
from urllib.parse import urljoin, urlparse
import json
import logging
import os
import subprocess
import sys
import tempfile
import time
import requests

from yaml import SafeLoader as loader
from yaml import dump as yaml_dump
from yaml import load as yaml_load

import carta_client
from carta_client.utils import get_machine_uuid, get_active_branch_name

LOG = logging.getLogger(__name__)


class VaultSecretClient(object):
    def __init__(self, deployment, role_id=None, secret_id=None):
        self.vault_url = carta_client.VAULT_ADDR
        self.token = None
        self.deployment = deployment
        self.role_id = role_id
        self.secret_id = secret_id
        if role_id == None or secret_id == None:
            self.get_vault_credentials()
        self.approle_login()

    def approle_login(self):
        # Authenticate with App Role API
        url = urljoin(self.vault_url, "v1/auth/approle/login")
        payload = {"role_id": f"{self.role_id}", "secret_id": f"{self.secret_id}"}
        response = requests.post(url, json=payload)

        if response.status_code != 200:
            print(f"Failed to login to vault with approle.", file=sys.stderr)
            print(response.content)
            sys.exit(2)

        value = response.json()

        auth_info = value["auth"]
        self.token = auth_info["client_token"]

    def read_secret(self, secret_name):
        url = urljoin(self.vault_url, urljoin("v1/", secret_name))
        headers = {"X-Vault-Token": self.token}
        response = requests.get(url, headers=headers)
        if response.status_code != 200:
            print(f"Failed to read secrets from vault.", file=sys.stderr)
            print(response.content)
            sys.exit(2)
        result = response.json()
        return result["data"]

    def get_vault_credentials(self):
        """Get vault secret id and role id
        Returns:
        """
        deployment_name = self.deployment

        # Try authenticating with existing vault token
        secret_path = f"secret/deployments/{deployment_name}/helm-values"
        url = urljoin(self.vault_url, "v1/auth/token/lookup-self")
        vault_token_path = os.path.join(os.environ["HOME"], ".vault-token")
        if os.path.exists(vault_token_path):
            with open(os.path.join(os.environ["HOME"], ".vault-token"), "r") as file:
                self.token = file.read().rstrip("\n")
        headers = {"X-Vault-Token": self.token}
        response = requests.get(url, headers=headers) if self.token else None
        if response is None or response.status_code != 200:
            if "VAULT_ROLE_ID" not in os.environ or "VAULT_SECRET_ID" not in os.environ:
                print(
                    "You must set the environment variables VAULT_ROLE_ID and VAULT_SECRET_ID or log in to vault another way to use carta-client. Exiting."
                )
                sys.exit(1)
            self.role_id, self.secret_id = (
                os.environ["VAULT_ROLE_ID"],
                os.environ["VAULT_SECRET_ID"],
            )
        else:
            result = response.json()
            self.token = result["data"]["id"]
            data = self.read_secret(secret_path)
            self.role_id, self.secret_id = (
                data["vault"]["role_id"],
                data["vault"]["secret_id"],
            )


class ContainerRegistryManager(object):
    def __init__(self):
        pass

    def get_container_registry_credentials(self, profile=None):
        import boto3

        session = boto3.Session(profile_name=profile)
        client = session.client("ecr", region_name="us-west-2")
        response = client.get_authorization_token()
        username, password = (
            base64.b64decode(response["authorizationData"][0]["authorizationToken"])
            .decode()
            .split(":")
        )

        return username, password

    def docker_login(self, profile=None):
        username, password = self.get_container_registry_credentials(profile=profile)
        LOG.info(f"Logging into {carta_client.DOCKER_REGISTRY}")

        docker = subprocess.Popen(
            [
                "docker",
                "login",
                carta_client.DOCKER_REGISTRY,
                "-u",
                username,
                "--password-stdin",
            ],
            stdin=subprocess.PIPE,
        )
        docker.stdin.write(password.encode("utf-8"))
        docker.stdin.close()
        retval = docker.wait()
        if not retval == 0:
            print("Docker login failed. Exiting.", file=sys.stderr)
            sys.exit(retval)


class DeploymentManager:
    def __init__(
        self,
        deployment,
        configuration_override=None,
        deployment_values_override=None,
        version_override=None,
        development_mode=False,
    ):
        self.deployment = deployment
        role_id, secret_id = None, None
        self.vault_client = VaultSecretClient(deployment, role_id, secret_id)
        self._configuration = None
        self._deployment_values = None
        self._deployment_env = None
        self.configuration_override = configuration_override
        self.deployment_values_override = deployment_values_override
        self.version_override = version_override
        self.development_mode = development_mode

    @property
    def configuration(self):
        if self._configuration is None:
            raise ValueError("Deployment Manager not initialized")

        return self._configuration

    @property
    def deployment_values(self):
        if self._deployment_values is None:
            raise ValueError("Deployment Manager not initialized")

        return self._deployment_values

    @property
    def deployment_env(self):
        if self._deployment_env is None:
            raise ValueError("Deployment Manager not initialized")

        return self._deployment_env

    def get_vault_credentials(self):
        """Get vault secret id and role id

        Returns:

        """
        deployment_name = self.deployment

        # Try authenticating with existing vault token
        secret_path = f"secret/deployments/{deployment_name}/helm-values"

        get_command = ["vault", "read", "-format=json", secret_path]
        check_command = ["vault", "token", "lookup"]
        vault = subprocess.Popen(check_command, stdout=subprocess.PIPE)
        retval = vault.wait()
        if retval != 0:
            if "VAULT_ROLE_ID" not in os.environ or "VAULT_SECRET_ID" not in os.environ:
                print(
                    "You must set the environment variables VAULT_ROLE_ID and VAULT_SECRET_ID or log in to vault another way to use carta-client. Exiting."
                )
                sys.exit(1)
            return os.environ["VAULT_ROLE_ID"], os.environ["VAULT_SECRET_ID"]
        else:
            vault = subprocess.Popen(get_command, stdout=subprocess.PIPE)
            retval = vault.wait()
            if retval != 0:
                print(
                    f"Currently logged in Vault user does not have permission to read {secret_path}"
                )
                print(
                    "Please set VAULT_ROLE_ID and VAULT_SECRET_ID to a user that has the appropriate permissions."
                )
                sys.exit(1)
            value = json.load(vault.stdout)["data"]
            return value["vault"]["role_id"], value["vault"]["secret_id"]

    def initialize(self, pull=False):
        """Get config from vault and log into docker with it.

        Args:
            pull:

        Returns:

        """
        host_net = False
        if hasattr(self, "host_net"):
            host_net = self.host_net

        if self._configuration is not None:
            return self._configuration, self._deployment_values, self._deployment_env

        role_id = self.vault_client.role_id
        secret_id = self.vault_client.secret_id

        config_override = self.configuration_override
        deployment_values_override = self.deployment_values_override
        if config_override is not None:
            if not os.path.exists(config_override):
                raise RuntimeError(
                    f"Config override file {config_override} does not exist"
                )
            config_override = os.path.abspath(config_override)

        if deployment_values_override is not None:
            if not os.path.exists(deployment_values_override):
                raise RuntimeError(
                    f"Deployment values override file {deployment_values_override} does not exist"
                )
            deployment_values_override = os.path.abspath(deployment_values_override)

        deployment_values = self.vault_client.read_secret(
            f"secret/deployments/{self.deployment}/helm-values"
        )

        if deployment_values_override:
            with open(deployment_values_override) as f:
                overrides = yaml_load(f, Loader=loader)
                deployment_values.update(overrides)
        import boto3

        client = boto3.client("ecr", region_name="us-west-2")
        response = client.get_authorization_token()
        username, password = (
            base64.b64decode(response["authorizationData"][0]["authorizationToken"])
            .decode()
            .split(":")
        )

        LOG.info(f"Logging into {carta_client.DOCKER_REGISTRY}")

        docker = subprocess.Popen(
            [
                "docker",
                "login",
                carta_client.DOCKER_REGISTRY,
                "-u",
                username,
                "--password-stdin",
            ],
            stdin=subprocess.PIPE,
        )
        docker.stdin.write(password.encode("utf-8"))
        docker.stdin.close()
        retval = docker.wait()
        if not retval == 0:
            print("Docker login failed. Exiting.", file=sys.stderr)
            sys.exit(retval)

        branch = None
        if self.development_mode or self.deployment in ["development", "staging"]:
            home = os.environ.get("HOME")
            main_repo = os.path.join(home, "dev/carta")
            branch = get_active_branch_name(main_repo).replace("/", "-")

        version = self.version_override or branch or deployment_values["docker_tag"]

        if "/" in version:
            print(
                f"Docker tag {version} is invalid. It has a slash in it. "
                "Remove the slash in the config and rerun"
            )
            sys.exit(1)
        ocean_image = f"{carta_client.DOCKER_REGISTRY}/carta/ocean:{version}"

        subprocess.Popen(["docker", "pull", ocean_image]).wait()

        docker_base_command = [
            "docker",
            "run",
            "-i",
            "--rm",
            "-e",
            f"VAULT_ROLE_ID={role_id}",
            "-e",
            f"VAULT_SECRET_ID={secret_id}",
        ]
        if (
            "VAULT_ACCESS_CERTIFICATE_PEM" in os.environ
            or "CARTA_DEPLOYMENT_CA_BUNDLE_PEM" in os.environ
        ):
            host_certificate_path = os.environ.get(
                "CARTA_DEPLOYMENT_CA_BUNDLE_PEM",
                os.environ.get("VAULT_ACCESS_CERTIFICATE_PEM"),
            )

            # get the current certifi path
            certifi_path_command = docker_base_command + [
                ocean_image,
                "python",
                "-c" "import certifi; print(certifi.where())",
            ]
            certifi_path_command = subprocess.Popen(
                certifi_path_command, stdout=subprocess.PIPE
            )
            certifi_path_command.wait()
            certifi_path = certifi_path_command.stdout.read()
            certifi_path = certifi_path.decode("utf-8").strip()
            docker_base_command += [
                "-v",
                f"{host_certificate_path}:{certifi_path}",
            ]

        if config_override:
            docker_base_command += [
                "-v",
                f"{config_override}:/carta/carta-configuration.yml:ro",
            ]

        base_command = docker_base_command + [
            ocean_image,
            "carta",
            "show-config",
        ]
        if not config_override:
            base_command += ["--deployment", self.deployment]

        carta_command = subprocess.Popen(base_command, stdout=subprocess.PIPE)
        retval1 = carta_command.wait()

        if not retval1 == 0:
            raise RuntimeError(
                f"Failed to get configuration for deployment {self.deployment}. Exiting."
            )

        deployment_config = json.load(carta_command.stdout)

        carta_command = subprocess.Popen(
            base_command + ["--format", "env"], stdout=subprocess.PIPE
        )
        retval2 = carta_command.wait()

        if not retval2 == 0:
            print(
                f"Failed to get configuration for deployment {self.deployment}. Exiting.",
                file=sys.stderr,
            )
            if self.deployment in ["development", "staging"]:
                print("Most commonly caused by not running: carta release build ")
            sys.exit(retval2)
        deployment_env = json.load(carta_command.stdout)

        if "deployment_uuid" not in deployment_config:
            deployment_uuid = get_machine_uuid()
            deployment_config["deployment_uuid"] = deployment_uuid
            deployment_env["CARTA_DEPLOYMENT_UUID"] = deployment_uuid

        self._configuration = deployment_config
        self._deployment_values = deployment_values
        self._deployment_env = deployment_env

        # Switch all of the relevant hostnames to localhost if host_net is true
        if host_net:
            variables = [
                "atlasdb_postgres_hostname",
                "dgraph_host",
                "dgraph_zero_host",
                "cartadb_postgres_hostname",
                "wiseordb_postgres_hostname",
                "fluentd_hostname",
                "prometheus_hostname",
                "redis_hostname",
                "wiseor_mysql_hostname",
                "trino_hostname",
            ]
            for variable in variables:
                deployment_config[variable] = "local.carta.healthcare"
                deployment_env[f"CARTA_{variable.upper()}"] = "local.carta.healthcare"

            # elasticsearch_external_url: http://local.carta.healthcare:9200
            # elasticsearch_internal_url: http://local.carta.healthcare:9200
            urls = {
                "elasticsearch_external_url": "http://local.carta.healthcare:9200",
                "elasticsearch_internal_url": "http://local.carta.healthcare:9200",
                "object_store_url": "http://local.carta.healthcare:9000",
                "internal_object_store_url": "http://local.carta.healthcare:9000",
                "usermgmt_internal_base_url": "http://local.carta.healthcare:5010/api/usermgmt",
            }

            for url, value in urls.items():
                deployment_config[url] = value
                deployment_env[f"CARTA_{url.upper()}"] = value

        # Append some of the key deployment values to each container's environment
        if "ssh" in deployment_values:
            for k, v in deployment_values["ssh"].items():
                deployment_env["CARTA_DEPLOYMENT_SSH_" + k.upper()] = v

        if "azure" in deployment_values:
            for k, v in deployment_values["azure"].items():
                deployment_env["CARTA_AZURE_" + k.upper()] = v

        if "vault" in deployment_values:
            for k, v in deployment_values["vault"].items():
                deployment_env["CARTA_VAULT_" + k.upper()] = v

        return deployment_config, deployment_values, deployment_env

    def deploy(self, version=None, attach=None, code_to_mount=None):
        """Deploy code.

        Args:
            version: Version of cartographer to deploy
            attach: List of services to attach to after they are booted
            code_to_mount: A local path to code that will be mounted at /carta in the appropriate containers
        Returns:

        """
        raise NotImplementedError("Not implemented")

    def stop(self):
        """Stop a deployment.

        Returns:

        """
        raise NotImplementedError("Not implemented")

    def destroy(self):
        """Destroy a deployment.

        Returns:

        """
        raise NotImplementedError("Not implemented")


class DockerComposeDeploymentManager(DeploymentManager):
    def __init__(
        self,
        deployment,
        configuration_override=None,
        deployment_values_override=None,
        host_net=False,
        ignore_version_matching=True,
        version_override=None,
        development_mode=False,
    ):
        super(DockerComposeDeploymentManager, self).__init__(
            deployment,
            configuration_override=configuration_override,
            deployment_values_override=deployment_values_override,
            version_override=version_override,
            development_mode=development_mode,
        )
        self.host_net = host_net
        self.ignore_version_matching = ignore_version_matching
        self.proxies = []

    def write_traefik_external_rules(self, external_rules):
        rules = {"http": {"routers": {}, "services": {}, "middlewares": {}}}
        rules["http"]["middlewares"]["https-redirect"] = {
            "redirectRegex": {"regex": "^http://(.*)", "replacement": "https://${{1}}"}
        }

        rules["http"]["middlewares"]["remove-powered-by"] = {
            "headers": {"customResponseHeaders": {"X-Powered-By": ""}}  # Removes
        }

        for i, rule in enumerate(external_rules):
            rules["http"]["routers"][f"my-route-{i}"] = {
                "rule": "PathPrefix(`{}`)".format(rule["from"]),
                "service": f"external-service-{i}",
                "middlewares": [f"my-route-{i}-prefix"],
                "entryPoints": ["websecure", "web"],
                "tls": {},
            }

            if self.wiseor_hostname:
                rules["http"]["routers"][f"my-route-{i}-host"] = {
                    "rule": "PathPrefix(`{}`) && Host(`{}`)".format(
                        rule["from"], self.wiseor_hostname
                    ),
                    "service": f"external-service-{i}",
                    "middlewares": [f"my-route-{i}-prefix"],
                    "entryPoints": ["websecure", "web"],
                    "tls": {},
                }

            rules["http"]["services"][f"external-service-{i}"] = {
                "loadBalancer": {"servers": [{"url": rule["to"]}]},
            }

            rules["http"]["middlewares"][f"my-route-{i}-prefix"] = {
                "stripPrefix": {"prefixes": ["{}".format(rule["from"])]},
            }

        traefik_rules = os.path.join(carta_client.TOOLBOX_ROOT, "external-rules.yml")
        with open(traefik_rules, "w") as f:
            f.write(yaml_dump(rules))

    def write_docker_compose(self, docker_compose_data):
        build_directory = os.path.join(carta_client.TOOLBOX_ROOT, "build")
        if not os.path.exists(build_directory):
            os.makedirs(build_directory)

        docker_compose_file = os.path.join(build_directory, "docker-compose.yaml")
        with open(docker_compose_file, "w") as f:
            f.write(yaml_dump(docker_compose_data))

        return docker_compose_file, docker_compose_data

    def get_docker_compose_command(self, config_path):
        working_directory = carta_client.TOOLBOX_ROOT
        cmd = ["docker-compose"]
        cmd += ["--project-directory", working_directory]

        cmd += ["-f", config_path]
        cmd += ["-p", self.deployment]

        root_cmd = list(cmd)

        return root_cmd

    def get_version_from_config(self):
        _, deployment_values, _ = self.initialize()
        return deployment_values.get("docker_tag")

    def dev_mode(self):
        # return self.deployment == "development"
        return self.development_mode

    def wiseor_dev_setup(self, install_dir, customer_id, code_root):
        LOG.info("WiseOR Platform")
        if not os.path.exists(
            os.path.join(
                code_root, "plugins", "carta-wiseor", "wiseor-platform", "Frontend2"
            )
        ):
            subprocess.Popen(
                [
                    "git",
                    "clone",
                    "git@bitbucket.org:carta-healthcare/wiseor-platform.git",
                ],
                cwd=os.path.join(code_root, "plugins", "carta-wiseor"),
            ).wait()
        else:
            subprocess.Popen(
                ["git", "pull"],
                cwd=os.path.join(
                    code_root, "plugins", "carta-wiseor", "wiseor-platform"
                ),
            ).wait()

        LOG.info("{} does not exist, starting WiseOR setup:".format(install_dir))
        base_path = "{}/{}.wiseor.com".format(install_dir, customer_id)
        extract_path = "{}/extract".format(base_path)
        rawdata_path = "{}/rawdata".format(base_path)
        for folder in [base_path, extract_path, rawdata_path]:
            LOG.info("Creating folder {}".format(folder))
            os.makedirs(folder, exist_ok=True)

        LOG.info("Downloading init-db.sql.gz")
        subprocess.Popen(
            [
                "curl",
                "-L",
                "-o",
                "{}/init-db.sql.gz".format(base_path),
                "https://www.dropbox.com/s/lcnlkot5yshx119/init-db.sql.gz?dl=1",
            ]
        ).wait()
        LOG.info("Downloading {}-default.tar.gz".format(customer_id))
        subprocess.Popen(
            [
                "curl",
                "-L",
                "-o",
                "{}/{}-default.tar.gz".format(rawdata_path, customer_id),
                "https://www.dropbox.com/s/1rxvvkb8kebnpih/mch-default.tar.gz?dl=1",
            ]
        ).wait()
        LOG.info("Uncompressing")
        subprocess.Popen(
            ["tar", "-xzvf", "{}-default.tar.gz".format(customer_id)], cwd=rawdata_path
        ).wait()

        LOG.info("Logging in to container registry")
        subprocess.Popen(["az", "acr", "login", "--name", "cartacr"]).wait()
        LOG.info("Building docker images for WiseOR")
        subprocess.Popen(
            ["./scripts/wiseor", "docker", "build"], cwd="{}/..".format(install_dir)
        ).wait()

    def wiseror_deployment_setup(self, attach, code_root=None):
        configuration = self.configuration
        plugins_configuration = configuration.get("plugins") or {}

        # WiseOR deployment setup
        wiseor_configuration = plugins_configuration.get("wiseor") or {}
        wiseor_services = {}
        if wiseor_configuration:
            default_install_dir = os.path.join(carta_client.TOOLBOX_ROOT, "install")
            wiseor_platform_version = configuration.get("wiseor_platform_version")
            if not wiseor_platform_version:
                raise OSError(f"Config wiseor_platform_version not set.")

            wiseor_backend_url = configuration.get("wiseor_backend_url")
            for customer_id, customer_config in wiseor_configuration.items():

                install_dir = (
                    customer_config.get("install_directory") or default_install_dir
                )
                proxy = customer_config.get("proxy")
                if proxy:
                    self.proxies.append(proxy)

                if not os.path.exists(install_dir):
                    if self.dev_mode():
                        self.wiseor_dev_setup(install_dir, customer_id, code_root)
                    else:
                        raise OSError(
                            f"Install directory {install_dir} does not exist. Please create it before deploying"
                        )
                service_name = f"wiseor-{customer_id}"
                self.wiseor_hostname = wiseor_hostname = (
                    customer_config.get("hostname") or f"{customer_id}.wiseor.com"
                )
                demo_rule = ""
                if customer_id == "mch":
                    demo_rule = "Host(`demo.carta.healthcare`) || "

                wiseor_services[service_name] = {
                    "image": f"{carta_client.DOCKER_REGISTRY}/carta/wiseor-platform:{wiseor_platform_version}",
                    "command": f"entrypoint.py run production {customer_id}",
                    "restart": "always",
                    "mem_limit": customer_config.get("memory_limit") or "16g",
                    "ports": ["8443", "8080"],
                    "labels": [
                        "traefik.enable=true",
                        # Core configuration
                        f"traefik.http.routers.{service_name}.service={service_name}",
                        f"traefik.http.middlewares.{service_name}.stripprefix.prefixes={wiseor_backend_url}",
                        f"traefik.http.routers.{service_name}.rule={demo_rule}PathPrefix(`{wiseor_backend_url}`) && Host(`{wiseor_hostname}`)",
                        f"traefik.http.routers.{service_name}.tls=true",
                        f"traefik.http.routers.{service_name}.entrypoints=websecure",
                        f"traefik.http.routers.{service_name}.middlewares={service_name}",
                        f"traefik.http.routers.{service_name}-prefix.service={service_name}",
                        f"traefik.http.middlewares.{service_name}-prefix.stripprefix.prefixes={wiseor_backend_url}",
                        f"traefik.http.routers.{service_name}-prefix.rule=PathPrefix(`{wiseor_backend_url}`)",
                        f"traefik.http.routers.{service_name}-prefix.tls=true",
                        f"traefik.http.routers.{service_name}-prefix.middlewares={service_name}-prefix",
                        f"traefik.http.routers.{service_name}-prefix.entrypoints=websecure",
                        f"traefik.http.services.{service_name}.loadbalancer.server.port=8443",
                        f"traefik.http.services.{service_name}.loadbalancer.server.scheme=https"
                        # HTTP -> HTTPS redirects
                        # f"traefik.http.routers.{service_name}-http.service={service_name}-http",
                        # f"traefik.http.routers.{service_name}-http.middlewares=https-redirect@file",
                        # f"traefik.http.routers.{service_name}-http.rule=PathPrefix(`{wiseor_base_url}`) && Host(`{wiseor_hostname}`)",
                        # f"traefik.http.routers.{service_name}-http.entrypoints=web",
                    ],
                    "volumes": [
                        f"{install_dir}:/carta/plugins/carta-wiseor/wiseor-platform/install"
                    ],
                }

                if self.dev_mode():
                    wiseor_services[service_name][
                        "command"
                    ] = f"entrypoint.py run development {customer_id}"

                if code_root:
                    carta_wiseor_directory = os.path.join(
                        code_root, "plugins", "carta-wiseor"
                    )
                    if not os.path.exists(carta_wiseor_directory):
                        raise ValueError(
                            f"Carta WiseOR Plugin not found in {carta_wiseor_directory}"
                        )
                    wiseor_services[service_name]["volumes"].append(
                        f"{carta_wiseor_directory}:/carta/plugins/carta-wiseor"
                    )

                if customer_config.get("proxy"):
                    del wiseor_services[service_name]

            try:
                mysql_password = (
                    configuration.get("plugins")
                    .get("wiseor")
                    .get("mch")
                    .get("secure.properties")
                    .get("dbpassword")
                )
            except:
                mysql_password = configuration.get("cartadb_postgres_password")

            wiseor_services["wiseor-mysql"] = {
                "image": f"{carta_client.DOCKER_REGISTRY}/carta/wiseor-mysql:{wiseor_platform_version}",
                "command": "--default-authentication-plugin=mysql_native_password",
                "restart": "always",
                "environment": {"MYSQL_ROOT_PASSWORD": mysql_password},
                "ports": ["3306"],
                "volumes": ["/var/lib/mysql"],
            }

            wiseor_ui_base_url = configuration.get("wiseor_ui_base_url", "/wiseor-ui/")
            wiseor_ui_port = configuration.get("wiseor_ui_port", "3001")
            wiseor_services["wiseor-ui"] = {
                "image": f"{carta_client.DOCKER_REGISTRY}/carta/wiseor-ui:{wiseor_platform_version}",
                # "command": 'bash -c "sleep 100000d"',
                "command": "entrypoint.sh",
                "restart": "always",
                "labels": [
                    "traefik.enable=true",
                    f"traefik.http.routers.wiseor-ui.rule=PathPrefix(`{wiseor_ui_base_url}`)",
                    "traefik.http.routers.wiseor-ui.entrypoints=websecure",
                    "traefik.http.routers.wiseor-ui.tls=true",
                    f"traefik.http.services.wiseor-ui.loadbalancer.server.port={wiseor_ui_port}",
                ],
                "ports": [wiseor_ui_port],
            }

        return wiseor_services

    def carta_documentation_setup(self, attach, code_root=None):
        configuration = self.configuration

        # carta-documentation deployment setup
        carta_documentation_version = None
        if code_root and os.path.exists(
            os.path.join(code_root, "plugins", "carta-documentation")
        ):

            carta_doc_tag = get_active_branch_name(
                os.path.join(code_root, "plugins", "carta-documentation")
            )
            carta_doc_tag = carta_doc_tag.replace("/", "-")
            carta_documentation_version = carta_doc_tag

        carta_documentation_usermgmt_internal = None
        carta_documentation_configuration = configuration.get("carta_documentation")
        if carta_documentation_configuration:
            carta_documentation_version = carta_documentation_configuration.get(
                "version"
            )
            carta_documentation_usermgmt_internal = carta_documentation_configuration.get(
                "usermgmt_internal_base_url"
            )

        if not carta_documentation_version:
            carta_documentation_version = "dev"

        carta_documentation = {
            "image": f"{carta_client.DOCKER_REGISTRY}/carta/carta-documentation:{carta_documentation_version}",
            "command": "carta-documentation serve --watch",
            "restart": "always",
            "labels": [
                "traefik.enable=true",
                "traefik.http.middlewares.carta-documentation-trailingslash.redirectregex.regex=^(.*)/carta-documentation$$",
                "traefik.http.middlewares.carta-documentation-trailingslash.redirectregex.replacement=$${1}/carta-documentation/",
                "traefik.http.middlewares.carta-documentation-trailingslash.redirectregex.permanent=true",
                "traefik.http.routers.carta-documentation.rule=PathPrefix(`/carta-documentation`)",
                "traefik.http.routers.carta-documentation.entrypoints=websecure",
                "traefik.http.routers.carta-documentation.tls=true",
                "traefik.http.services.carta-documentation.loadbalancer.server.port=8444",
            ],
            "ports": ["8444"],
        }

        # Add authintication to carta-documentation
        if carta_documentation_usermgmt_internal:
            carta_documentation["labels"].extend(
                [
                    f"traefik.http.middlewares.carta-documentation-usermgmt-auth.forwardauth.address={carta_documentation_usermgmt_internal}",
                    "traefik.http.routers.carta-documentation.middlewares=carta-documentation-usermgmt-auth,carta-documentation-trailingslash",
                ]
            )
        else:
            carta_documentation["labels"].append(
                "traefik.http.routers.carta-documentation.middlewares=carta-documentation-trailingslash",
            )
        return carta_documentation

    def generate_docker_compose_file(self, attach, code_root=None):
        # WiseOR deployment setup
        wiseor_services = {}

        # TODO: I believe we should depracate this -matt
        # if code_root is not None and os.path.exists(
        #     os.path.join(code_root, "plugins", "carta-wiseor")
        # ):
        #     wiseor_services = self.wiseror_deployment_setup(attach, code_root)

        # carta-documentation deployment setup
        # carta_documentation = self.carta_documentation_setup(attach, code_root)

        # Pull key things from the data
        data_mounts = self.deployment_values.get(
            "data_mounts"
        ) or self.deployment_values.get("mounts")
        if data_mounts is None:
            data_mounts = []

        mirth_ports = self.deployment_values.get("mirth_ports")
        if mirth_ports is None:
            mirth_ports = []

        expose_ports = self.deployment_values.get("expose_ports")
        if expose_ports is None:
            expose_ports = []

        attach_data_mounts = []
        if code_root:
            valid_code = False
            if (
                os.path.exists(code_root)
                and os.path.exists(os.path.join(code_root, "carta-core"))
                and os.path.exists(os.path.join(code_root, "modules"))
            ):
                valid_code = True
            if not valid_code:
                raise ValueError(f"Code Root {code_root} does not exist or is invalid")

            attach_data_mounts.append(f"{code_root}:/carta")

        compose_path = os.path.join(
            carta_client.TOOLBOX_ROOT, "carta_client/docker/docker-compose.yaml"
        )
        with open(compose_path) as f:
            compose_data = yaml_load(f, Loader=loader)

        # Merge in traefik config
        traefik_config = self.deployment_values.get("traefik")
        if traefik_config:
            load_balancer_env = (
                compose_data["services"]["load-balancer"].get("environment") or {}
            )
            compose_data["services"]["load-balancer"]["environment"] = load_balancer_env
            load_balancer_env["TRAEFIK_CONFIG"] = json.dumps(traefik_config)

        # traefik_external_rules = self.deployment_values.get("traefik_external_rules")
        # if traefik_external_rules:
        if self.proxies:
            self.write_traefik_external_rules(self.proxies)
            compose_data["services"]["load-balancer"]["environment"][
                "timestamp"
            ] = time.time()  # force reload
            compose_data["services"]["load-balancer"]["volumes"].append(
                "./external-rules.yml:/etc/traefik.d/external-rules.yml"
            )

        # Merge in WiseOR
        for service_name, service_data in wiseor_services.items():
            compose_data["services"][service_name] = service_data

        # Merge in carta-documentation
        # compose_data["services"]["carta-documentation"] = carta_documentation

        mount_services = [
            "ocean",
            "mustang",
            "cartographer-worker",
            "cartographer-cron",
            "cartographer-websocket",
            "grafana",
            "mirth",
            "backup",
            "ide",
            "wiseor-mch",
            "wiseor-shc",
            "wiseor-ui",
            "astrolabe",
            "navigator",
            "semaphore",
            "babelfish-ocr",
            "babelfish-nlp",
            "babelfish-worker",
        ]
        # if code_root and os.path.exists(os.path.join(code_root, "plugins", "carta-documentation")):
        #     mount_services.append('carta-documentation')

        # Add additional volumes
        for service in mount_services:
            if service not in compose_data["services"]:
                continue
            conf = compose_data["services"][service]
            if "volumes" in conf:
                volumes = conf["volumes"]
            else:
                volumes = []

            # Add the data mounts from config
            volumes += data_mounts

            # Add the external plugins directory
            volumes.append("./external-plugins:/carta/external-plugins")

            # Add the common shared directory
            volumes.append("deployment-shared:/carta/shared")

            if code_root and service in attach:
                to_remove = []
                for i, v in enumerate(volumes):
                    if "/carta/plugins" in v:
                        to_remove.append(v)
                    # Don't overwrite the main /carta/datasets,
                    # but allow umls/notes snapshots shared with elasticsearch
                    if v.startswith("carta-datasets:"):
                        to_remove.append(v)

                for r in to_remove:
                    volumes.remove(r)

            conf["volumes"] = volumes

        if self.development_mode and "semaphore" in compose_data["services"]:
            compose_data["services"]["semaphore"]["volumes"].append(
                f"./workdir/semaphore:/home/semaphore/user"
            )

        for service in attach:
            if service not in compose_data["services"]:
                service_list = [key for key in compose_data["services"].keys()]
                LOG.error(
                    f"{service} is no a valid service, you can attach to: {service_list}"
                )
                sys.exit()
            conf = compose_data["services"][service]
            volumes = conf.get("volumes") or []
            conf["volumes"] = volumes
            volumes += attach_data_mounts
            conf["command"] = 'bash -c "sleep 100000d"'

        # Open up ports
        new_mirth_ports = set(compose_data["services"]["mirth"].get("ports") or [])
        for mp in mirth_ports:
            new_mirth_ports.add(mp)

        compose_data["services"]["mirth"]["ports"] = list(new_mirth_ports)
        if not self.configuration["enable_dgraph_ingest"]:
            for dg in ["dgraph-alpha", "dgraph-zero", "ratel"]:
                if dg in compose_data:
                    del compose_data["services"][dg]

        services = self.deployment_values.get("services") or {}
        services_to_disable = {"dgraph"}
        for key, value in services.items():
            if value.get("disabled") or value.get("enabled") is False:
                services_to_disable.add(key)
            elif (
                value.get("enabled") or value.get("disabled") is False
            ) and key in services_to_disable:
                services_to_disable.remove(key)

        for key in services_to_disable:
            if key == "dgraph":
                for dg in ["dgraph-alpha", "dgraph-zero", "ratel"]:
                    if dg in compose_data["services"]:
                        del compose_data["services"][dg]
            elif key in compose_data["services"]:
                del compose_data["services"][key]

        for service, conf in compose_data["services"].items():
            if self.host_net:
                conf["network_mode"] = "host"
                if "networks" in conf:
                    del conf["networks"]
            if "environment" not in conf:
                conf["environment"] = {}
            conf["environment"]["CARTA_SERVICE"] = service

        if self.configuration.get("prometheus_additional_configs_path"):
            prometheus_additional_configs_path = self.configuration[
                "prometheus_additional_configs_path"
            ]
            if os.path.isdir(prometheus_additional_configs_path):
                compose_data["services"]["prometheus"]["volumes"].append(
                    f"{prometheus_additional_configs_path}:/etc/prometheus/conf.d"
                )
            else:
                print(
                    "Skipping additional configuration mounting, please make sure of the path configured in prometheus_additional_configs_path."
                )

        compose_data = self.inject_extra_hosts(compose_data)
        # host mode and port mappings became incompatible on docker 20
        compose_data = self.remove_port_mappings(compose_data)
        compose_data = self.add_security_headers(compose_data)
        # sys.exit(json.dumps(compose_data, indent=4, sort_keys=True))
        return compose_data

    def add_security_headers(self, compose_data):
        services = ["ocean", "mustang"]
        for service in compose_data["services"]:
            if service not in services:
                continue
            if "labels" not in compose_data["services"][service]:
                continue

            routers = {}
            for label in compose_data["services"][service]["labels"]:
                if label.startswith("traefik.http.routers."):
                    router = label.split(".")[3]
                    if router not in routers:
                        routers[router] = False

            # Exclude swagger route specifically, as it isn't a security risk
            for router in routers:
                if "swagger-doc" in router:
                    routers[router] = True

            for i, label in enumerate(compose_data["services"][service]["labels"]):
                if label.startswith("traefik.http.routers.") and "middlewares" in label:
                    router = label.split(".")[3]
                    if not routers[router]:
                        routers[router] = True
                        compose_data["services"][service]["labels"][i] = ",".join(
                            [compose_data["services"][service]["labels"][i], "csrf"]
                        )

            for router, processed in routers.items():
                if not processed:
                    compose_data["services"][service]["labels"].append(
                        f"traefik.http.routers.{router}.middlewares=csrf"
                    )

            if len(routers) > 0:
                for label in [
                    "traefik.http.middlewares.csrf.headers.customResponseHeaders.X-XSS-Protection=1; mode=block",
                    "traefik.http.middlewares.csrf.headers.customResponseHeaders.Content-Security-Policy=1; default-src https:",
                ]:
                    compose_data["services"][service]["labels"].append(label)
        return compose_data

    def remove_port_mappings(self, compose_data):
        if self.host_net:
            for service in compose_data["services"]:
                if "ports" in compose_data["services"][service]:
                    del compose_data["services"][service]["ports"]

        return compose_data

    def inject_extra_hosts(self, compose_data):
        extra_hosts = self.deployment_values.get("extra_hosts")
        if extra_hosts:
            for service in compose_data["services"]:
                if "extra_hosts" not in compose_data["services"][service]:
                    compose_data["services"][service]["extra_hosts"] = {}
                for host, ip in extra_hosts.items():
                    compose_data["services"][service]["extra_hosts"][host] = ip

        return compose_data

    def inject_cacert_mount(self, compose_data):
        # VAULT_ACCESS_CERTIFICATE_PEM for backwards compatibility
        if (
            "CARTA_DEPLOYMENT_CA_BUNDLE_PEM" not in os.environ
            and "VAULT_ACCESS_CERTIFICATE_PEM" not in os.environ
        ):
            return compose_data
        # Services that require outbound access
        services = [
            "load-balancer",
            "mustang",
            "ocean",
            "astrolabe",
            "cartographer-worker",
            "ide",
            "semaphore",
        ]
        # Configure the volumes
        host_certificate_path = os.environ.get(
            "CARTA_DEPLOYMENT_CA_BUNDLE_PEM",
            os.environ.get("VAULT_ACCESS_CERTIFICATE_PEM"),
        )
        for service in services:
            ca_bundle_dest = "/etc/ssl/certs/ca-certificates.crt"
            if "volumes" not in compose_data["services"][service]:
                compose_data["services"][service]["volumes"] = []
            compose_data["services"][service]["volumes"].append(
                f"{host_certificate_path}:{ca_bundle_dest}"
            )

        extra_env = {
            "REQUESTS_CA_BUNDLE": ca_bundle_dest,  # OS, azure, python
            "VAULT_CACERT": ca_bundle_dest,  # vault cli
            "NODE_EXTRA_CA_CERTS": ca_bundle_dest,  # npm, nodejs
            "CA_BUNDLE": ca_bundle_dest,  # pip
            "CURL_CA_BUNDLE": ca_bundle_dest,  # requests, conda
            "AWS_CA_BUNDLE": ca_bundle_dest,  # aws
        }
        compose_data["services"][service]["environment"][
            "CA_BUNDLE_ENV_TOOLS"
        ] = ",".join(k for k in extra_env.keys())

        # Configure the environment variables
        for service in services:
            compose_data["services"][service]["environment"].update(extra_env)

        return compose_data

    def create_docker_environment_file(self, temp, additional_env):
        def write_env(temp, env):
            for k, v in env.items():
                if k == "PATH":
                    continue
                if k == "CARTA_EXTERNAL_CLIENT":
                    v = "false"
                temp.write("{k}={v}\n".format(k=k, v=v).encode("utf-8"))

        write_env(temp, self.deployment_env)
        write_env(temp, additional_env)
        temp.flush()

    def deploy(
        self,
        version=None,
        attach=None,
        code_to_mount=None,
        extra_env=None,
        force_recreate=False,
        pull=False,
        separate_ocr_nlp=False,
        containers_to_exclude=[]
    ):
        """Deploy code to docker compose.

        Args:
            version:
            attach:
            code_to_mount:
            pull: Pull before launching
            host_net: Run services on the host network

        Returns:

        """
        args = ["up", "-d", "--remove-orphans"]
        if force_recreate:
            args.append("--force-recreate")

        if pull:
            self._run_docker_compose(
                ["pull"],
                version,
                attach=None,
                code_to_mount=None,
                extra_env=extra_env,
                separate_ocr_nlp=separate_ocr_nlp,
            )

        # Stop running Jupyter containers
        self.kill_jupyter_containers()

        return self._run_docker_compose(
            args,
            version,
            attach,
            code_to_mount,
            extra_env,
            separate_ocr_nlp=separate_ocr_nlp,
            containers_to_exclude=containers_to_exclude
        )

    def impute_environment(self, docker_compose_data, additional, attach):
        docker_compose_data = copy.deepcopy(docker_compose_data)
        environment = self.deployment_env.copy()
        environment.update(additional)
        for service, definition in docker_compose_data["services"].items():
            if "env_file" in definition:
                del definition["env_file"]
            environment_variables = definition.get("environment") or {}
            definition["environment"] = environment_variables

            for name, value in environment.items():
                if value is None:
                    raise Exception(
                        f"Failed to get a proper value for the {name} environment variable."
                    )

                environment_variables[name] = value.replace(
                    "$", "$$"
                )  # Avoids docker-compose interpolation errors

            # Add prefer file env var if we are attaching and in hostnet mode
            if self.host_net and service in attach:
                environment_variables["CARTA_PREFER_FILE_CONFIG"] = "true"

        return docker_compose_data

    def _screen_session_attach(self, attach, code_to_mount=None):
        # screen docker exec -it development_ocean_1 sh
        with tempfile.NamedTemporaryFile() as temp2:
            LOG.info(
                "Opening interactive session in screen for services {}".format(attach)
            )

            codebase = "/carta"
            if code_to_mount is not None:
                codebase = code_to_mount

            screenrc_file = os.path.join(codebase, ".screenrc")
            if not os.path.exists(screenrc_file):
                screenrc_file = os.path.join(
                    carta_client.CARTA_CLIENT_ROOT, "docker/.screenrc"
                )

            with open(screenrc_file) as screenrc:
                temp2.write(screenrc.read().encode("utf-8"))

            for i, service in enumerate(attach):
                service_name = "{}_{}_1".format(self.deployment, service)
                cmd = ["docker", "exec", "-it", service_name, "/bin/bash"]
                temp2.write(
                    "screen -t {service}       {number} {cmd}\n".format(
                        service=service, cmd=" ".join(cmd), number=i + 1
                    ).encode("utf-8")
                )
            temp2.flush()

            subprocess.Popen(["screen", "-c", temp2.name], env={"TERM": "vt100"}).wait()

    def get_toolbox_paths(self):
        file_path = os.path.realpath(__file__)
        from os.path import dirname, join

        main_toolbox_path = dirname(dirname(file_path))
        dev_path = dirname(main_toolbox_path)
        toolbox_releaser_path = join(dev_path, "carta-toolbox-releaser")
        return main_toolbox_path, toolbox_releaser_path

    def carta_toolbox_release(self, version):
        _, toolbox_releaser_path = self.get_toolbox_paths()
        if not os.path.exists(toolbox_releaser_path):
            subprocess.run(
                [
                    "git",
                    "clone",
                    "https://carta-vitor@bitbucket.org/carta-healthcare/carta-toolbox.git",
                    toolbox_releaser_path,
                ]
            )
        subprocess.run(["git", "fetch"], cwd=toolbox_releaser_path)
        checkout_command = subprocess.run(
            ["git", "checkout", version.replace("release-", "release/")],
            cwd=toolbox_releaser_path,
        )
        if checkout_command.returncode != 0:
            LOG.error(f"Branch {version} doesn't exist")
            sys.exit(1)
        subprocess.run(
            ["python", "carta-client", *sys.argv[1:]],
            cwd=os.path.join(toolbox_releaser_path, "scripts"),
        )

    def defer_releasing_process_to_matching_carta_toolbox_version(self, version):
        if version == "dev":
            return

        main_toolbox_path, _ = self.get_toolbox_paths()
        git_branch = subprocess.run(
            ["git", "rev-parse", "--abbrev-ref", "HEAD"],
            stdout=subprocess.PIPE,
            cwd=main_toolbox_path,
        )
        if git_branch.returncode != 0:
            raise Exception("Could not find current carta-client version.")

        branch = git_branch.stdout.decode("utf-8")
        branch = branch.replace("\n", "").replace("release/", "release-")

        subprocess.run(["git", "fetch"], stdout=subprocess.PIPE, cwd=main_toolbox_path)

        all_branches = subprocess.run(
            ["git", "branch", "-r"], stdout=subprocess.PIPE, cwd=main_toolbox_path
        )
        all_branches = (
            all_branches.stdout.decode("utf-8").replace("origin/", "").split("\n")
        )
        all_branches = [
            branch.replace("HEAD ->", "").replace("release/", "release-").strip()
            for branch in all_branches
        ]

        if version not in all_branches:
            LOG.warn(
                f"""
                Could not find a matching release version for carta-client.
                To proceed with the release processe you can:
                Option 1) ignore version matching by removing the flag --version-matching
                Option 2) create the {version} branch in the carta-toolbox repository
                """
            )
            sys.exit(1)

        self.carta_toolbox_release(version)
        sys.exit(1)

    def _run_docker_compose(
        self,
        compose_args,
        version=None,
        attach=None,
        code_to_mount=None,
        extra_env=None,
        separate_ocr_nlp=False,
        containers_to_exclude=[]
    ):
        host_net = self.host_net
        if attach is None:
            attach = []

        # Get configuration and deployment values
        if compose_args and compose_args[0] == "pull":
            self.initialize(pull=True)
        else:
            self.initialize(pull=False)

        deployment_config, deployment_values, deployment_env = (
            self.configuration,
            self.deployment_values,
            self.deployment_env,
        )
        aws_config = deployment_values.get("aws") or {}

        # Figure out what version we're deploying if it's not specified
        if not version:
            version = self.get_version_from_config()
        version = version.replace("/", "-")

        if not self.ignore_version_matching:
            self.defer_releasing_process_to_matching_carta_toolbox_version(version)

        # Get vault creds
        vault_role_id, vault_secret_id = (
            self.vault_client.role_id,
            self.vault_client.secret_id,
        )

        # Create the compose file
        docker_compose_data = self.generate_docker_compose_file(
            attach, code_root=code_to_mount
        )

        if separate_ocr_nlp:
            docker_compose_data["services"].pop("cartographer-worker", None)
        else:
            docker_compose_data["services"].pop("babelfish-worker", None)
            docker_compose_data["services"].pop("babelfish-ocr", None)
            docker_compose_data["services"].pop("babelfish-nlp", None)

        additional = {
            "BRANCH_NAME": version,
            "CARTA_DOCKER_TAG": version,
            "DOCKER_TAG_NAME": version,
            "VAULT_ROLE_ID": vault_role_id,
            "VAULT_SECRET_ID": vault_secret_id,
            "CARTA_DEPLOYMENT": self.deployment,
            "AWS_ACCESS_KEY_ID": aws_config.get("access_id"),
            "AWS_SECRET_ACCESS_KEY": aws_config.get("secret_key"),
        }
        if extra_env:
            additional.update(extra_env)
        if self.dev_mode():
            additional["CARTA_DEVELOPMENT_MODE"] = "true"
            additional["CARTA_DEVELOPMENT_ATTACHED"] = ":".join(attach)
            additional["CARTA_DEVELOPMENT_CODE_ROOT"] = (
                code_to_mount if code_to_mount else "/carta"
            )
            additional["PYTHONWARNINGS"] = "default"
        else:
            additional["PYTHONWARNINGS"] = "ignore"

        if host_net:
            additional["CARTA_DOCKER_NETWORK_MODE"] = "host"
        else:
            additional["CARTA_DOCKER_NETWORK_MODE"] = "bridge"
        
        if not self.deployment_env["CARTA_PLATFORM_VERSION"].startswith("1."):
            additional["CARTA_POSTGRES_SSL_MODE"] = "require"
        else:
            additional["CARTA_POSTGRES_SSL_MODE"] = "disable"

        if "CARTA_ELASTICSEARCH_INTERNAL_URL" in self.deployment_env:
            elastic_url = urlparse(self.deployment_env["CARTA_ELASTICSEARCH_INTERNAL_URL"])
            additional["CARTA_ELASTICSEARCH_INTERNAL_URL_PORT"] = str(elastic_url.port) if elastic_url.port else "9200"
            additional["CARTA_ELASTICSEARCH_INTERNAL_URL_HOST"] = elastic_url.hostname
        
        docker_compose_data = self.impute_environment(
            docker_compose_data, additional, attach
        )

        # Mount host's certificates into containers that require outbound connections behind proxy
        docker_compose_data = self.inject_cacert_mount(docker_compose_data)

        # Get the compose command to run
        docker_env = deployment_env.copy()
        docker_env.update(additional)
        docker_env.update(
            {
                "HOME": os.environ["HOME"],
                "PATH": os.environ["PATH"],
                "COMPOSE_HTTP_TIMEOUT": "300",
            }
        )

        custom_docker_host = deployment_config.get("docker_host")
        if custom_docker_host is not None:
            docker_env["DOCKER_HOST"] = custom_docker_host

        if "CARTA_DGRAPH_ZERO_HOST" not in docker_env:
            docker_env["CARTA_DGRAPH_ZERO_HOST"] = "dgraph-zero"

        for container in containers_to_exclude:
            if container in docker_compose_data['services'].keys():
                del docker_compose_data['services'][container]
            else:
                raise Exception(f"There is no container named {container} to remove.")
        docker_compose_file, docker_compose_data = self.write_docker_compose(
            docker_compose_data
        )
        docker_compose_command = self.get_docker_compose_command(docker_compose_file)
        docker_compose_command += compose_args

        if compose_args and not (
            compose_args[0] == "pull"
            or compose_args[0] == "down"
            or compose_args[0] == "stop"
        ):
            scaled_services = ["ocean", "wiseor-ui", "mustang"]
            if separate_ocr_nlp:
                scaled_services += [
                    "babelfish-ocr",
                    "babelfish-nlp",
                    "babelfish-worker",
                ]
            else:
                scaled_services += ["cartographer-worker"]
            for service in scaled_services:
                service_scale_variable = f"{service.replace('-', '_')}_scale"
                scale = (
                    deployment_values.get(service_scale_variable)
                    or deployment_config.get(service_scale_variable)
                    or 1
                )
                docker_compose_command.append("--scale")
                docker_compose_command.append(f"{service}={scale}")

        docker_compose_command_string = " ".join(docker_compose_command)
        docker_compose_command = subprocess.Popen(
            docker_compose_command, env=docker_env, cwd=carta_client.TOOLBOX_ROOT,
        )
        retval = docker_compose_command.wait()
        if retval != 0:
            raise ValueError(
                f"Failed to launch docker-compose with command {docker_compose_command_string}"
            )
        if len(attach) > 0:
            self._screen_session_attach(attach, code_to_mount)

        return docker_compose_data

    def destroy(self, version=None):
        """Destroy deployment deployed in docker compose.

        Args:


        Returns:

        """
        found_version = None
        if not version:
            found_version = False
            name = f"{self.deployment}_ocean_1:"
            for container in self.list_running_containers():
                if name in container and len(container.split(":")) > 2:
                    v = container.split(":")[2]
                    v = v.replace("'\\n\"", "")
                    found_version = v
                    break

        # Sneakly kill jupyter containers
        self.kill_jupyter_containers()

        args = ["down", "--volumes"]

        if found_version:
            yes = {"yes", "y", "ye", ""}
            choice = input(
                f"About to destroy deployment {self.deployment} for version {found_version}[Y/n]"
            ).lower()
            if choice not in yes:
                return False
            return self._run_docker_compose(args, found_version)
        else:
            return self._run_docker_compose(args, version)

    def stop(self, version=None):
        """Stop a deployment deployed in docker compose."""
        args = ["stop"]
        return self._run_docker_compose(args, version)

    def list_running_containers(self):
        docker_ps = subprocess.Popen(
            ["docker", "ps", "--format", "'{{.Names}}:{{.Image}}'"],
            stdout=subprocess.PIPE,
        )
        docker_ps.wait()
        for line in docker_ps.stdout.readlines():
            yield (str(line))

    def kill_jupyter_containers(self):
        """Search and stop jupyter running semaphore jupyter containers
        """
        # Use docker ps to find jupyter container ids
        docker_ps = subprocess.Popen(
            [
                "docker",
                "ps",
                "--filter",
                "name=semaphore-user",
                "--format",
                "'{{.ID}}'",
            ],
            stdout=subprocess.PIPE,
        )
        docker_ps.wait()

        # Get read out of jupyter container ids
        jupyter_container_ids = []
        for line in docker_ps.stdout.readlines():
            container_id = line.decode().strip().split("'")[1]
            jupyter_container_ids.append(container_id)

        # Kill the containers
        if len(jupyter_container_ids) > 0:
            docker_stop_command = ["docker", "stop"] + jupyter_container_ids
            docker_stop = subprocess.Popen(docker_stop_command, stdout=subprocess.PIPE)
            docker_stop.wait()

    def cleanup(self, deployment=False):
        """Cleans up all images not being used by containers."""
        if deployment:
            LOG.info("Ensuring deployment is running before clean up.")
            # ensure deployment ran successfully
            containers_count = 0
            for container in self.list_running_containers():
                if deployment in container:
                    containers_count = containers_count + 1

            if containers_count == 0:
                LOG.error(
                    f"Could not find containers running for {deployment}, aborting."
                )
                return False

        subprocess.Popen(["docker", "image", "prune", "-af"]).wait()

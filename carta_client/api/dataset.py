import json

from carta_client.api.base import BaseInterface
from carta_client.utils import LOG


class DatasetInterface(BaseInterface):
    """REST API interface for installing/removing/listing/etc. datasets in Cartographer
    """

    def list_versions(self, dataset_id=None):
        payload = {}
        if dataset_id is not None:
            payload["dataset_id"] = dataset_id
        response = self.session.post(
            self.cartographer_url + "/dataset/list", json=payload
        )
        self.check_response(response, "Failed to list dataset version")

        parsed = response.json()
        all_versions = parsed.get("data")
        print_versions = parsed.get("message")
        LOG.info(print_versions)
        return all_versions

    def info(self, dataset_id=None, version=None):
        payload = {}
        if dataset_id is not None:
            payload["dataset_id"] = dataset_id
        if version is not None:
            payload["version"] = version

        response = self.session.post(
            self.cartographer_url + "/dataset/info", json=payload
        )
        self.check_response(response, "Failed to get dataset info")

        parsed = response.json()
        LOG.info(json.dumps(parsed, indent=4))
        return parsed

    def install(
        self,
        dataset_id=None,
        version=None,
        partitions=None,
        synchronous=False,
        limit=None,
        wait=False,
        info=False,
    ):
        payload = {}
        if dataset_id is not None:
            payload["dataset_id"] = dataset_id
        if version is not None:
            payload["version"] = version
        if synchronous:
            payload["synchronous"] = True
        if partitions is not None:
            payload["partitions"] = partitions
        if limit is not None:
            payload["limit"] = limit
        if wait:
            payload["wait"] = True
        if info:
            payload["info"] = True

        response = self.session.post(
            self.cartographer_url + "/dataset/install", json=payload
        )
        self.check_response(
            response, "Failed to install dataset: check the ocean or event logs!"
        )

        parsed = response.json()
        status = parsed.get("status")
        installed_version = parsed.get("data")

        # Raise error if dataset installation fails or is canceled
        if status == "CANCELED":
            raise RuntimeError(
                f"Dataset installation has been canceled: {response.text}"
            )
        elif response.status_code != 200 or status == "ERROR":
            raise RuntimeError(f"Failed to install dataset:  {response.text}")

        LOG.info(f"Successfully installed carta dataset version {installed_version}")

    def uninstall(self, dataset_id=None):
        payload = {}
        response = self.session.post(
            self.cartographer_url + "/dataset/uninstall", json=payload
        )
        self.check_response(response, "Failed to uninstall dataset")

        parsed = response.json()
        LOG.info(f"Successfully removed carta deployment dataset: {parsed}")

    def evaluate(self, evaluation_ids, dataset_id, synchronous=False):
        payload = {"evaluation_ids": evaluation_ids, "dataset_id": dataset_id}
        if synchronous:
            payload["synchronous"] = True
        response = self.session.post(
            self.cartographer_url + "/dataset/evaluate", json=payload
        )
        self.check_response(response, "Failed to evaluate dataset")

        parsed = response.json()
        evaluation_results = parsed.get("data")
        LOG.info(
            f"Successfully evaluated carta deployment dataset:\n {evaluation_results}"
        )

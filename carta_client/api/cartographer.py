import json
import logging
from enum import Enum
from typing import Dict, List

from carta_client.api.base import BaseInterface

LOG = logging.getLogger(__name__)


class CubeUpdateType(Enum):
    COMPLETE = "complete"
    INCREMENTAL = "incremental"


class KnowledgeGraphInterface(BaseInterface):
    def connector_file(
        self,
        customer_id: str,
        source: str,
        files: List[tuple],
        limit: int = None,
        synchronous: bool = False,
        preflight_check: bool = False,
        cohort_str=None,
    ):
        """Connect data via file.

        Args:
            customer_id: Customer ID to upload data to. You need to have permission to submit data to that customer ID.
            source: The source ID for the data. Will be routed to the connector with the same source ID.
            files: A list of tuples of the form (filename, fileobject, [content type]) (content type is optional)
            limit: Limit the number of rows processed. Left up to interpretation by the specific Connector.
            synchronous: Run connector synchronously. Should only be used for debugging, as the processing can take a while.
            preflight_check: Run the preflight-check only.
        """
        connector_url = self.cartographer_url + "/connector"
        payload = {
            "customer_id": customer_id,
            "source": source,
            "registry": cohort_str,
        }
        if customer_id is None:
            raise ValueError("customer_id is a required parameter")
        if limit is not None:
            payload["limit"] = limit
        if preflight_check:
            payload["preflight-check"] = "true"
        if synchronous:
            payload["synchronous"] = "true"

        post_files = [("data[]", f) for f in files]

        response = self.session.post(connector_url, data=payload, files=post_files)

        self.check_response(response, failure_message="Failed to connect file data")

        return response.json()

    def connector_json(
        self,
        customer_id: str,
        source: str,
        payload: Dict,
        limit: int = None,
        synchronous: bool = False,
        preflight_check: bool = False,
    ):
        """Connect data via JSON data

        Args:
            customer_id: Customer ID to upload data to. You need to have permission to submit data to that customer ID.
            source: The source ID for the data. Will be routed to the connector with the same source ID.
            payload: A JSON-serializable dictionary
            limit: Limit the number of rows processed. Left up to interpretation by the specific Connector.
            synchronous: Run connector synchronously. Should only be used for debugging, as the processing can take a while.
        """
        connector_url = self.cartographer_url + "/connector"
        payload = {
            "customer_id": customer_id,
            "source": source,
            "json": json.dumps(payload),
        }
        if customer_id is None:
            raise ValueError("customer_id is a required parameter")
        if limit is not None:
            payload["limit"] = limit
        if synchronous:
            payload["synchronous"] = "true"
        if preflight_check:
            payload["preflight-check"] = "true"

        response = self.session.post(connector_url, data=payload)
        self.check_response(response, failure_message="Failed to connect json data")
        response_data = response.json()
        if preflight_check:
            return response_data["data"]
        else:
            return response_data

    def cube_update(
        self,
        customer_id: str,
        cube_ids: List[str],
        partitions: List[Dict],
        update_type: CubeUpdateType = CubeUpdateType.INCREMENTAL,
        sync: bool = False,
    ):
        cube_update_url = self.cartographer_url + "/cubes/update"
        if not customer_id:
            raise ValueError("customer_id is required")

        if not cube_ids:
            raise ValueError("cube_ids are required")
        payload = {
            "cube_ids": cube_ids,
            "customer_id": customer_id,
            "update_type": update_type.value,
            "partitions": partitions,
        }
        params = {}
        if sync:
            params["sync"] = True

        response = self.session.post(cube_update_url, json=payload, params=params)
        self.check_response(
            response, failure_message="Failed to submit cube update request"
        )

        return response.json()

    def cube_prune(
        self,
        customer_id: str,
        cube_id: str,
        partitions: List[Dict],
        prune_all: bool = False,
    ):
        cube_prune_url = self.cartographer_url + "/cubes/prune"
        if not customer_id:
            raise ValueError("customer_id is required")
        if not cube_id:
            raise ValueError("cube_id are required")
        if len(partitions) == 0 and not prune_all:
            raise ValueError("partitions are required")

        payload = {
            "cube_id": cube_id,
            "customer_id": customer_id,
            "partitions": partitions,
            "all": prune_all,
        }

        response = self.session.post(cube_prune_url, json=payload)
        self.check_response(response, failure_message="Failed to prune cube")

        return response.json()

    def cube_drop(
        self, customer_id: str, cube_id: str,
    ):

        cube_drop_url = self.cartographer_url + "/cubes/drop"

        if not customer_id:
            raise ValueError("customer_id is required")

        if not cube_id:
            raise ValueError("cube_id are required")
        payload = {
            "cube_id": cube_id,
            "customer_id": customer_id,
        }

        response = self.session.post(cube_drop_url, json=payload)
        self.check_response(response, failure_message="Failed to drop cube")

        return response.json()

    def cube_list(
        self, customer_id: str,
    ):

        cube_list_url = self.cartographer_url + "/cubes/list"

        if not customer_id:
            raise ValueError("customer_id is required")

        payload = {
            "customer_id": customer_id,
        }

        response = self.session.post(cube_list_url, json=payload)
        self.check_response(response, failure_message="Failed to list cubes")

        return response.json()

    def cube_info(
        self, customer_id: str, cube_id: str,
    ):

        cube_info_url = self.cartographer_url + "/cubes/info"

        if not customer_id:
            raise ValueError("customer_id is required")

        if not cube_id:
            raise ValueError("cube_id are required")
        payload = {
            "cube_id": cube_id,
            "customer_id": customer_id,
        }

        response = self.session.post(cube_info_url, json=payload)
        self.check_response(response, failure_message="Failed to info cube")

        return response.json()


class JobInterface(BaseInterface):
    def get_job_details(self, job_id, logs=False, graph=False):
        job_url = self.cartographer_url + f"/jobs/{job_id}"
        request = {}
        if logs:
            request["logs"] = True
        if graph:
            request["graph"] = True

        response = self.session.get(job_url, params=request)
        self.check_response(response, failure_message="Failed to get job")

        return response.json()

    def cancel_job(self, job_id, cancel_customer_id=None):
        cancel_url = self.cartographer_url + f"/jobs/{job_id}/cancel"

        response = self.session.post(
            cancel_url, json={"cancel_customer_id": cancel_customer_id}
        )
        self.check_response(response, failure_message="Failed to cancel job")

        return response.json()


class CartographerStatusInterface(BaseInterface):
    def get_preflight_check(self):
        preflight_url = self.cartographer_url + f"/preflight-check"
        response = self.session.get(preflight_url)
        self.check_response(response, failure_message="Failed to get preflight status")
        return response.json()

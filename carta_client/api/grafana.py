import logging
import requests
from urllib.parse import urlparse

from carta_client.api.base import BaseInterface

LOG = logging.getLogger(__name__)


class GrafanaInterface(BaseInterface):
    """Interface for configuring Cartographer's Grafana instance."""

    def __init__(self, base_url, jwt):
        super().__init__(base_url, jwt)
        del self.session.headers["Authorization"]
        hostname = urlparse(self.base_url).hostname
        auth_cookie = requests.cookies.create_cookie(
            domain=hostname, name="JWT", value=self.jwt
        )
        self.session.cookies.set_cookie(auth_cookie)

    def get_datasources(self):
        """Get Grafana data sources."""
        response = self.session.get(self.grafana_url + "/datasources")
        return response.json()

    def get_datasource(self):
        """Get data sources.
        
        Deprecated. Please use get_datasources()
        """
        return self.get_datasources()

    def get_search(self):
        """Search Grafana dashboards."""
        response = self.session.get(self.grafana_url + "/search")
        return response.json()

    def create(self, title):
        """Create Grafana dashboard."""
        payload = {
            "dashboard": {
                "title": title,
                "tags": ["templated"],
                "timezone": "browser",
                "schemaVersion": 16,
                "version": 0,
                "refresh": "25s",
            },
            "folderId": 0,
            "overwrite": False,
        }
        response = self.session.post(self.grafana_url + "/dashboards/db", json=payload)
        return response.json()

    def load_dynamic_grafana_configuration(self):
        """Load dynamic Grafana dashboards.
        
        This loads any plugin-generated Grafana dashboards.
        """
        response = self.session.get(
            self.base_url + "/api/ocean/plugins/grafana/configurations"
        )
        self.check_response(
            response, failure_message="Failed to load grafana dashboards"
        )
        all_grafana_extensions = response.json()
        total_dashboards = 0
        for plugin_extensions in all_grafana_extensions:
            plugin_dashboards = plugin_extensions.get("dashboards") or []
            # plugin_datasources = plugin_extensions.get("data_sources") or []
            total_dashboards += len(plugin_dashboards)
            for dashboard_definition in plugin_dashboards:

                dashboard = {
                    "folderId": 0,
                    "overwrite": True,
                }
                dashboard["dashboard"] = dashboard_definition
                response = self.session.post(
                    self.grafana_url + "/dashboards/db", json=dashboard
                )

        return {"total_dashboards": total_dashboards}

    def delete(self, uid):
        response = self.session.delete(self.grafana_url + "/dashboards/uid/" + uid)
        return response.json()

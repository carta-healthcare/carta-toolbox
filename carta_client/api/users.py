from typing import Dict, List
import logging

from carta_client.exceptions import CartaClientError
from .base import BaseInterface


LOG = logging.getLogger(__name__)


class UserInterface(BaseInterface):
    def list_entities(self):
        session = self.session
        response = session.get(self.entities_url)
        self.check_response(response, failure_message="Failed to list entities")

        return response.json()

    def create_user(self, customer_id, username, password, name):
        session = self.session
        entities = self.list_entities()["data"]
        entities = list(filter(lambda x: x["customer_id"] == customer_id, entities))
        if len(entities) == 0:
            raise CartaClientError(
                f"Entity for {customer_id} does not "
                f"exist or you do not have permission to write to it"
            )
        entity = entities[0]
        entity_ids = [entity.get("id") for entity in entities]
        response = session.post(
            self.users_url,
            json={
                "name": name,
                "user_pwd": password,
                "is_active": True,
                "product_roles": [],
                "entities": entity_ids,
                "entity_id": entity["id"],
                "email": username,
            },
        )

        if "error" in response.json():
            if response.json()["error"] == "ObjectAlreadyExistsError":
                return response.json()["error"]

        self.check_response(response, failure_message="Failed to create User")

        return response.json()

    def delete_user(self, username):
        session = self.session
        response = session.delete(self.users_url + "/" + username)
        self.check_response(response)

        return response.json()

    def list_users(self):
        session = self.session
        response = session.delete(self.users_url)
        self.check_response(response)
        return response.json()

    def set_user_password(self, username, password):
        session = self.session
        response = session.put(
            self.users_url + f"/{username}/password", json={"password": password}
        )

        self.check_response(
            response, failure_message="Failed to reset password for user"
        )
        return response.json()


class IdentityInterface(BaseInterface):
    def get_identity(self, identity_id):
        session = self.session

        response = session.get(self.identities_url + f"/{identity_id}")
        self.check_response(response, failure_message="Failed to get identity")

        return response.json()

    def check_permissions(self, required_grants, required_scopes=[], identity_id=None):
        session = self.session

        response = session.get(
            self.usermgmt_url + f"/check-permissions",
            json={
                "required_grants": required_grants,
                "identity_id": None,
                "required_scopes": required_scopes,
            },
        )
        self.check_response(response, failure_message="Failed to check permissions")

        return response.json()

    def delete_identity(self, identity_id):
        session = self.session
        response = session.delete(self.identities_url + f"/{identity_id}")
        self.check_response(response, f"Failed to delete identity")
        deleted = response.json()["data"]
        return deleted

    def add_identity_role(
        self, authenticator, system_role, role_name, username=None, customer_id=None
    ):
        session = self.session
        payload = {
            "authenticator": authenticator,
            "role_name": role_name,
            "customer_id": customer_id,
            "username": username,
            "system_role": system_role,
        }
        request_url = f"{self.identities_url}/roles"
        response = session.put(request_url, json=payload)

        self.check_response(response, failure_message="Add identity role failed")
        return response.json()

    def find_identity(self, authenticator, username, customer_id=None):
        session = self.session
        payload = {
            "authenticator": authenticator,
            "username": username,
            "customer_id": customer_id,
        }
        response = session.get(self.identities_url, params=payload)
        self.check_response(response, f"Failed to fetch identities for query {payload}")
        identities = response.json()["data"]
        if len(identities) != 1:
            raise CartaClientError(
                f"Failed to find unique identity for query {payload}: {identities}"
            )
        return identities[0]

    def remove_identity_role(self, identity_id, role_name, system=False):
        session = self.session

        # Get the identity and customer_id
        identity = self.get_identity(identity_id)["data"]
        customer_id = identity["customer_id"]

        # Get all available roles and get the role that was requested
        role_interface = RoleInterface(self.base_url, self.jwt)
        all_roles = {
            (r["customer_id"], r["name"]): r for r in role_interface.get_roles()["data"]
        }
        remove_role = all_roles.get(
            (customer_id, role_name), all_roles.get((None, role_name))
        )
        if remove_role is None:
            raise CartaClientError(
                f"Role {role_name} in customer_id {customer_id} does not exist"
            )

        # Get the roles that the identity currently has
        current_roles = identity["roles"]
        current_roles = {(r["customer_id"], r["name"]): r for r in current_roles}

        customer_id = identity["customer_id"] if not system else None
        if (customer_id, role_name) not in current_roles:
            LOG.info(f"Identity {identity_id} does not have role {role_name}")
            return False
        role_ids = [r["id"] for r in current_roles.values()]
        role_ids.remove(remove_role["id"])

        payload = {"role_ids": role_ids}
        response = session.put(
            self.identities_url + f"/{identity_id}/roles", json=payload
        )
        self.check_response(response, failure_message="Failed to set roles")
        return response.json()


class RoleInterface(BaseInterface):
    def get_roles(self):
        session = self.session
        response = session.get(self.roles_url)
        self.check_response(response, failure_message="Failed to get roles")

        return response.json()

    def get_role_by_id(self, role_id):
        session = self.session
        response = session.get(self.roles_url + f"/{role_id}")
        self.check_response(response, failure_message="Failed to get role")
        return response.json()

    def get_role_by_name(self, customer_id, role_name):
        roles = self.get_roles()

        for role in roles["data"]:
            if (role["name"] == role_name) and (role["customer_id"] == customer_id):
                found_role = role
                return self.get_role_by_id(found_role["id"])

    def edit_role_grants(self, customer_id, role_name, grants: Dict):
        session = self.session
        role = self.get_role_by_name(customer_id, role_name)
        if role is None:
            raise CartaClientError(
                f"Role named {role_name} in customer_id {customer_id} not found"
            )

        role_to_update = role
        role["path_grants"].update(grants)
        response = session.put(self.roles_url + f"/{role['id']}", data=role_to_update)
        self.check_response(response, failure_message="Failed to update role")
        return response.json

    def remove_role_grants(self, customer_id, role_name, grants: List):
        session = self.session
        role = self.get_role_by_name(customer_id, role_name)
        if role is None:
            raise CartaClientError(
                f"Role named {role_name} in customer_id {customer_id} not found"
            )

        role_to_update = role["data"]
        for grant in grants:
            del role_to_update["path_grants"][grant]
        if "customer_id" in role_to_update and role_to_update["customer_id"] is None:
            del role_to_update["customer_id"]

        response = session.put(
            self.roles_url + f"/{role_to_update['id']}", json=role_to_update
        )
        self.check_response(response, failure_message="Failed to update role")
        return response.json()

    def create_or_edit_role(self, customer_id, role_name, grants: Dict):
        session = self.session

        role_to_create = {
            "name": role_name,
            "role_type": "USER",
            "path_grants": grants,
        }
        if customer_id:
            role_to_create["customer_id"] = customer_id

        found_role = self.get_role_by_name(customer_id, role_name)

        if found_role:
            found_role = found_role["data"]
            if found_role["customer_id"] is None:
                del found_role["customer_id"]
            found_role["path_grants"].update(role_to_create["path_grants"])
            response = session.put(
                self.roles_url + f"/{found_role['id']}", json=found_role
            )
            self.check_response(
                response, failure_message="Failed to create or edit role"
            )
            return response.json()
        else:
            response = session.post(self.roles_url, json=role_to_create)
            self.check_response(
                response, failure_message="Failed to create or edit role"
            )
            return response.json()

import os
import sys
from mimetypes import guess_type
from pathlib import Path
from typing import Any, Dict, List, Tuple

from requests import Response

from carta_client.api.base import BaseInterface
from carta_client.utils import LOG


class BabelfishInterface(BaseInterface):
    def parse_ingest_inquiry(self, job_id: str) -> Tuple[int, Any]:
        """
        Get the status of a job through its id.
        Args:
            job_id (str): the Job Id
        """

        base_url = f"{self.babelfish_url}/ingest/{job_id}"
        return self.evaluate_response(self.session.get(base_url))

    def evaluate_response(self, response: Response) -> Tuple[int, Any]:
        try:
            result = response.json()
        except Exception as e:
            LOG.error(f"Found error in response json parsing: {e}")
            result = f"Sever error response: {response.text}"
        return response.status_code, result

    def get_base_name(self, full_file_name: str) -> str:
        return os.path.basename(full_file_name)

    def get_mime_type_from_file_name(self, file_name: str) -> str:
        mime_type, _ = guess_type(file_name)
        return mime_type

    def parameters_parse(self, note_id: str, sync: bool, params: str = None) -> Dict:
        query_input: Dict = {}
        if note_id is not None:
            query_input.update({"note_id": note_id})
        if sync is not None:
            query_input.update({"sync": sync})
        if params is None:
            return query_input
        for param in params:
            try:
                query_input.update({param.split("=")[0]: param.split("=")[1]})
            except Exception as e:
                LOG.error(f"Failed to parse parameter {param}.  Error {e}")
                sys.exit(2)
        return query_input

    def validate_note(self, note: str) -> None:
        if len(note) == 0:
            LOG.error("File not specified.")
            sys.exit(2)
        if not os.path.exists(note[0]):
            LOG.error(f"File not found {note[0]}.")
            sys.exit(2)

    def take_request(self, url: str, note: str, params: Dict) -> Tuple[int, Any]:
        file_name = self.get_base_name(note)
        response = self.session.post(
            url,
            files={
                "note": (
                    file_name,
                    (Path(note)).open("rb"),
                    self.get_mime_type_from_file_name(file_name),
                )
            },
            params=params,
        )
        return self.evaluate_response(response)

    def parse_ingest_nlp(self, note_id: str, sync: bool, note: str) -> Tuple[int, Any]:
        """
        Parse the given file through NLP engine
        Args:
            note_id (str): the Id for the note (also will be used for the job)
            sync (boolean): true or false, whether the request will be synchronous
            note (str): path to the note file
        """
        base_url = self.babelfish_url + "/ingest/nlp"
        return self.parse_ingest(base_url, note_id, sync, note)

    def parse_ingest_ocr(self, note_id: str, sync: bool, note: str) -> Tuple[int, Any]:
        """
        Parse the given file through OCR engine
        Args:
            note_id (str): the Id for the note (also will be used for the job)
            sync (boolean): true or false, whether the request will be synchronous
            note (str): path to the note file
        """
        base_url = self.babelfish_url + "/ingest/ocr"
        return self.parse_ingest(base_url, note_id, sync, note)

    def parse_ingest_note(
        self, note_id: str, sync: bool, note: str, params: List[str]
    ) -> Tuple[int, Any]:
        """
        Ingest a note through the entire Cartographer pipeline
        Args:
            note_id (str): the Id for the note (also will be used for the job)
            sync (boolean): true or false, whether the request will be synchronous
            note (str): path to the note file
            params (List[str]): parameters to send to ingest function (e.g. patient_id, encounter_id, etc.)
        """
        base_url = self.babelfish_url + "/ingest/note"
        return self.parse_ingest(base_url, note_id, sync, note, params)

    def parse_ingest(
        self,
        base_url: str,
        note_id: str,
        sync: bool,
        note: str,
        params: List[str] = None,
    ) -> Tuple[int, Any]:
        self.validate_note(note)
        param_dict = self.parameters_parse(note_id=note_id, sync=sync, params=params)
        return self.take_request(base_url, note[0], param_dict)

from carta_client.api.base import BaseInterface
import requests


def check_for_errors(response, *_args, **_kwargs):
    """
        check_for_errors: handle API errors
        """
    try:
        response.raise_for_status()

    except requests.exceptions.RequestException:
        print(" ERROR ".center(80, "-"))

    # response_json = response.json()
    # if response_json.get("debug"): In case we only need in debug mode
    print(response.text)


class PluginInterface(BaseInterface):
    def reload_plugins(self):
        response = self.session.post(self.plugins_url + "/reload")
        self.check_response(response, failure_message="Failed to reload plugins")

        return response.json()

    def check_plugins_status(self):
        HOOKS = {"response": check_for_errors}
        headers = {"Authorization": "Bearer {}".format(self.jwt)}
        response = self.session.get(
            self.cartographer_url + "/status", hooks=HOOKS, headers=headers, timeout=1
        )
        return response.json()

    def install_plugins(self, private_pypi_url, plugin_list):
        response = self.session.post(
            self.plugins_url + "/pypi",
            json={"private_pypi_url": private_pypi_url, "requirements": plugin_list,},
        )
        self.check_response(response, failure_message="Failed to install plugin")

        return response.json()

    def install_plugin(self, plugin, ssh_key=None, branch=None):
        HOOKS = {"response": check_for_errors}
        response = self.session.post(
            self.plugins_url,
            json={"plugin": plugin, "ssh_key": ssh_key, "branch_name": branch},
            hooks=HOOKS,
        )

        return response.json()

    def list_plugins(self):
        response = self.session.get(self.plugins_url)
        self.check_response(response, failure_message="Failed to list plugins")

        return response.json()

    def update_plugins(self, plugin_specs: list, ssh_key: str = None):
        for plugin_spec in plugin_specs:
            if "@" in plugin_spec:
                plugin, version = plugin_spec.split("@")
            else:
                plugin = plugin_spec
                version = None
            payload = {"plugin": plugin}
            if version:
                payload["tag"] = version
            if ssh_key:
                payload["ssh_key"] = ssh_key

            response = self.session.put(self.plugins_url, json=payload)
            self.check_response(response, failure_message="Failed to list plugins")

        return response.json()

    def update_plugins_by_manifest(self, manifest: dict, ssh_key: str = None):
        plugin_specs = []
        for k, v in manifest.items():
            plugin = k
            if v and isinstance(v, str):
                tag = v
            else:
                tag = "master"
            plugin_specs.append(f"{plugin}@{tag}")

        return self.update_plugins(plugin_specs, ssh_key=ssh_key)


class BackupInterface(BaseInterface):
    def create_snapshot(self, snapshot_id):
        snapshot_url = self.ocean_url + "/backup/snapshot"
        response = self.session.post(snapshot_url, json={"snapshot_id": snapshot_id},)
        self.check_response(response, failure_message="Failed to create snapshot")

        return response.json()

    def restore_snapshot(self, snapshot_id):
        snapshot_url = self.ocean_url + "/backup/restore"
        response = self.session.post(snapshot_url, json={"snapshot_id": snapshot_id},)
        self.check_response(response, failure_message="Failed to restore snapshot")

        return response.json()

import requests

from carta_client.exceptions import CartaClientError


class BaseInterface(object):
    def __init__(self, base_url, jwt, refresh_token=None):
        while base_url.endswith("/"):
            base_url = base_url[:-1]
        self.base_url = base_url
        self.jwt = jwt
        self.refresh_token = refresh_token
        self.ocean_url = base_url + "/api/ocean"
        self.usermgmt_url = base_url + "/api/usermgmt"
        self.babelfish_url = base_url + "/api/babelfish"
        self.cartographer_url = base_url + "/api/cartographer"
        self.penguinsearch_url = base_url + "/api/penguinsearch"
        self.grafana_url = base_url + "/grafana/api"
        self.compass_url = base_url + "/api/compass"
        self.atlas_url = base_url + "/api/atlas"
        self.users_url = self.usermgmt_url + "/users"
        self.entities_url = self.usermgmt_url + "/entities"
        self.registries_url = self.compass_url + "/registries"
        self.plugins_url = self.ocean_url + "/plugins"
        self.identities_url = self.usermgmt_url + "/identities"
        self.roles_url = self.usermgmt_url + "/roles"
        self.session = requests.Session()
        self.session.timeout = (5.1, 120)

        if jwt:
            self.session.headers = {"Authorization": f"Bearer {jwt}"}
        else:
            self.session.headers = {}

    def renew_token(self):
        """Renew JWT.
        
        This uses a refresh token (if you have one) to renew your JWT.

        Returns:
            tuple: (JWT, refresh token)
        """
        response = self.session.get(
            self.base_url + "/api/usermgmt/account/jwt/authorize"
        )
        if not response.ok and self.refresh_token:
            response = requests.get(
                self.base_url + "/api/usermgmt/refresh",
                headers={f"Authorization": f"Bearer {self.refresh_token}"},
            )
            if response.ok:
                data = response.json()
                self.jwt = data["access_token"]
                self.refresh_token = data.get("refresh_token")

                return self.jwt, self.refresh_token
            else:
                raise ValueError("JWT/Refresh Token are no longer valid")
        else:
            return self.jwt, self.refresh_token

    def check_response(self, response, failure_message):
        """Check a response for an error.
        
        If an error is received from the server, throw an appropriate exception.
        """
        status_code = response.status_code
        if not response.ok:
            raise CartaClientError(
                failure_message, context=response, status_code=status_code
            )

    def login(self, username, password, customer_id=None, method=None):
        """
        Authenticate with given username and password. If customer_id isn't specified, attempt discovery.
        
        Args:
            username (str): The username to authenticate with
            password (str): The password to authenticate with
            customer_id (str): The customer_id to authenticate into
            method (str): The method of authentication
        
        Returns:
            dict: Authentication response
        """
        base_url = self.base_url

        # Figure out customer id and method if not given
        if not customer_id or not method:
            payload = {"username": username}
            if customer_id:
                payload["customer_id"] = customer_id
            url = base_url + "/api/usermgmt/auth/discover"
            response = self.session.post(url, json=payload)
            self.check_response(
                response,
                failure_message=f"Failed to discover auth method for server at {base_url}",
            )
            discovery = response.json()

            method = discovery["default"]
            customer_id = discovery["customer_id"]

        # Authenticate with the provided method
        url = base_url + f"/api/usermgmt/auth/{method}"
        payload = {
            "username": username,
            "password": password,
            "customer_id": customer_id,
            "refresh": True,
        }
        response = self.session.post(url, json=payload)
        self.check_response(
            response,
            failure_message=f"Failed to authenticate with server at {base_url}",
        )

        return response.json()

    def get_api_keys(self):
        """Gets API Keys based on JWT.

        Returns:
            list: List of API key names and IDs
        """
        base_url = self.base_url
        url = base_url + "/api/usermgmt/api-keys"

        response = self.session.get(url)
        self.check_response(
            response, failure_message=f"Failed to get api key from {base_url}",
        )
        return response.json()

    def delete_api_key(self, api_key_ids):
        """Delete API Key based ID.

        Deletes all API keys in the provided list.
        
        Returns:
            None
        """
        base_url = self.base_url
        for api_key_id in api_key_ids:
            url = base_url + f"/api/usermgmt/api-keys/{api_key_id}"

            response = self.session.delete(url)
            self.check_response(
                response, failure_message=f"Failed to delete api key from {base_url}",
            )

    def create_api_key(self, name, scopes, user_config=None):
        """Create API Key.

        Create an API key with the given name.
        """
        base_url = self.base_url
        url = base_url + "/api/usermgmt/api-keys"

        payload = {"scopes": scopes}
        if name:
            payload["name"] = name
        if user_config:
            payload.update(user_config)

        response = self.session.post(url, json=payload)
        self.check_response(
            response, failure_message=f"Failed to create api key from {base_url}",
        )
        json_response = response.json()
        if user_config:
            json_response["user_config"] = user_config
        return json_response

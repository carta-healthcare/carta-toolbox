from carta_client import CartaClientError
from carta_client.api.base import BaseInterface


class RegistryInterface(BaseInterface):
    def get_registry(self, registry_name):
        response = self.session.get(self.registries_url)
        self.check_response(response, failure_message="Failed to fetch registry")
        for registry in response.json()["data"]:
            if registry["name"] == registry_name:
                return registry
        raise CartaClientError(f"Registry named {registry_name} not found")

    def get_registry_program(self, registry_name, registry_program_name):
        registry = self.get_registry(registry_name)
        response = self.session.get(self.registries_url + f"/{registry['id']}/programs")
        self.check_response(response, failure_message="Failed to fetch registry")
        for registry_program in response.json()["data"]["programs"]:
            if registry_program["name"] == registry_program_name:
                return registry, registry_program
        raise CartaClientError(
            f"Registry program named {registry_program_name} not found in registry {registry_name}"
        )

    def get_registry_schema(self, registry_name, registry_program_name, version):
        registry, registry_program = self.get_registry_program(
            registry_name, registry_program_name
        )
        response = self.session.get(
            self.registries_url
            + f"/{registry['id']}/programs/{registry_program['id']}/schemas"
        )
        self.check_response(response, failure_message="Failed to fetch registry")
        for schema in response.json()["data"]["schemas"]:
            if schema["version_number"] == version:
                return registry, registry_program, schema
        raise CartaClientError(
            f"Registry program named {registry_program_name} {version} not found in registry {registry_name}"
        )

    def upload_registry(self, sheet_url):
        payload = {
            "sheets": sheet_url,
        }
        response = self.session.post(
            self.atlas_url + "/registries/upload", json=payload
        )
        data = response.json()
        return data

    def list_registry_releases(self, registry, revision=None):
        payload = {
            "registry": registry,
        }
        response = self.session.post(
            self.atlas_url + "/registries/releases/list", json=payload
        )
        data = response.json()
        return data

    def load_released_registry(self, registry, revision=None):
        payload = {
            "registry": registry,
        }
        if revision is not None:
            payload["revision"] = revision
        response = self.session.post(
            self.atlas_url + "/registries/releases/load", json=payload
        )
        data = response.json()
        return data

    def list_version(self):
        response = self.session.get(self.atlas_url + "/version")
        self.check_response(response, failure_message="Failed to list version")
        data = response.json()
        return data

    def list_regprograms_nested(self):
        response = self.session.get(self.compass_url + "/regprograms_nested")
        self.check_response(response, failure_message="Failed to list reg programs")
        data = response.json()
        return data

    def load_registry_configuration(self, sheet: str):

        response = self.session.post(
            self.atlas_url + "/registry/configuration/load", json={"sheets": [sheet]}
        )
        data = response.json()
        return data


class RedisInterface(BaseInterface):
    def kill_em_all(self, init_schemas=False):
        query_string = "" if not init_schemas else "?init_schemas=true"
        redis_url = self.compass_url + "/registries/cache/redis" + query_string
        response = self.session.delete(redis_url)
        data = response.json()
        return data

    def delete_redis_data_by_reg_sch_id(self, reg_sch_id):
        redis_url = self.compass_url + "/registries/cache/redis/{}".format(reg_sch_id)
        response = self.session.delete(redis_url)
        data = response.json()
        return data

    def get_list_redis_data(self):
        redis_url = self.compass_url + "/registries/cache/redis"
        response = self.session.get(redis_url)
        data = response.json()
        return data

    def get_redis_data_by_reg_sch_id(self, reg_sch_id):
        redis_url = self.compass_url + "/registries/cache/redis/{}".format(reg_sch_id)
        response = self.session.get(redis_url)
        data = response.json()
        return data

    def delete_all_redis_valuesets(self):
        redis_url = self.compass_url + "/registries/cache/redis/valuesets"
        response = self.session.delete(redis_url)
        data = response.json()
        return data

    def delete_redis_valuesets_by_reg_sch_id(self, reg_sch_id):
        redis_url = self.compass_url + "/registries/cache/redis/valuesets/{}".format(
            reg_sch_id
        )
        response = self.session.delete(redis_url)
        data = response.json()
        return data

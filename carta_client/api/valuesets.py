import io
import os
import json
import logging
from datetime import datetime

from carta_client.api.base import BaseInterface

LOG = logging.getLogger(__name__)


class ValuesetInterface(BaseInterface):
    def load_sheet(self, sheet):
        LOG.info(f"Loading Valueset sheet - '{sheet}'")
        url = f"{self.compass_url}/valuesets"

        # Load Valuesets
        payload = {"sheet": sheet}
        response = self.session.post(url, json=payload)

        return response.json()

    def download_valuesets(self, save_path: str):
        import pandas as pd

        LOG.info(f"Performing download valuesets to save_path: '{save_path}'")

        url = f"{self.compass_url}/valuesets/concept_filters?export=True&valueset=True"
        LOG.info(f"Request is sent to: '{url}'")

        # Download valuesets
        payload = {
            "include_relationships": False,
            "exclude_relationship_filter": True,
            "table": False,
            "nested_concepts": False,
            "from_code_system": {},
        }

        response = self.session.post(url, json=payload)

        # Validate Response
        if response.status_code == 200:
            # Create path if it does not exists
            if not os.path.isdir(save_path):
                os.makedirs(save_path)

            # Construct file path to be saved
            current_time = datetime.now().strftime("%m%d%Y_%H%M%S")
            file = os.path.join(save_path, f"valuesets_{current_time}.xlsx")

            with open(file, "wb") as outfile:
                outfile.write(io.BytesIO(response.content).getbuffer())

            # Get the dataframe for count
            df_code_system = pd.read_excel(file, sheet_name="code_system")
            df_concept = pd.read_excel(file, sheet_name="concept")
            df_concept_map = pd.read_excel(file, sheet_name="concept_map")
            df_valueset = pd.read_excel(file, sheet_name="valueset")
            df_valueset_compose = pd.read_excel(file, sheet_name="valueset_compose")

            result = {
                "status": response.status_code,
                "message": "Created Valueeset file successfully",
                "file": file,
                "count": {
                    "code_system": len(df_code_system.index),
                    "concept": len(df_concept.index),
                    "concept_map": len(df_concept_map.index),
                    "valueset": len(df_valueset.index),
                    "valueset_compose": len(df_valueset_compose.index),
                },
            }

        else:
            LOG.error("Error while downloading valuesets")

            result = {
                "status": response.status_code,
                "message": "Error while creating valueset file",
            }

        return result

    # Copy Dataset API
    def list_revisions(self, valueset_name=None):
        payload = {}
        if valueset_name is not None:
            payload["valueset_name"] = valueset_name
        response = self.session.post(
            self.compass_url + "/valuesets/revisions/list", json=payload
        )
        if response.status_code != 200:
            raise RuntimeError(f"Failed to list valueset revision:  {response.text}")

        parsed = response.json()
        all_revisions = parsed.get("data")
        print_revisions = parsed.get("message")
        LOG.info(print_revisions)
        return all_revisions

    def info(self, valueset_name=None, revision=None):
        payload = {}
        if valueset_name is not None:
            payload["valueset_name"] = valueset_name
        if revision is not None:
            payload["revision"] = revision

        response = self.session.post(
            self.compass_url + "/valuesets/revisions/info", json=payload
        )
        if response.status_code != 200:
            raise RuntimeError(f"Failed to get valueset info:  {response.text}")

        parsed = response.json()
        LOG.info(json.dumps(parsed, indent=4))
        return parsed

    def load_revision(self, valueset_name=None, revision=None):
        payload = {}
        if valueset_name is not None:
            payload["valueset_name"] = valueset_name
        if revision is not None:
            payload["revision"] = revision

        response = self.session.post(
            self.compass_url + "/valuesets/revisions/load", json=payload
        )
        if response.status_code != 200:
            raise RuntimeError(f"Failed to install valueset:  {response.text}")

        parsed = response.json()
        installed_revision = parsed.get("data")
        LOG.info(f"Successfully installed carta valueset revision {installed_revision}")

    def uninstall(self, valueset_name=None):
        payload = {}
        response = self.session.post(
            self.compass_url + "/valuesets/revisions/uninstall", json=payload
        )
        if response.status_code != 200:
            raise RuntimeError(f"Failed to uninstall valueset:  {response.text}")

        parsed = response.json()
        LOG.info(f"Successfully removed carta deployment valueset: {parsed}")

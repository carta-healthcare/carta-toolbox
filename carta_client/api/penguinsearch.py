import json

from carta_client.api.base import BaseInterface
from carta_client.utils import LOG


class PenguinSearchInterface(BaseInterface):
    def dictionary_search(self, query_string: str, limit: int):
        url = (
            self.penguinsearch_url
            + "/dictionary/search"
            + f"?query_string={query_string}"
            + f"&limit={limit}"
        )

        response = self.session.get(url)
        self.check_response(
            response, failure_message="Failed to search UMLS dictionary"
        )
        LOG.info(
            f"Successfully searched for {query_string}:\n{json.dumps(response.json(), indent=4)}"
        )
        return response.json()

    def dictionary_get(
        self, cui: str,
    ):
        url = self.penguinsearch_url + f"/dictionary/get/{cui}"
        response = self.session.get(url)
        self.check_response(response, failure_message="Failed to get UMLS dictionary")
        LOG.info(f"Successfully got {cui}:\n{json.dumps(response.json(), indent=4)}")
        return response.json()

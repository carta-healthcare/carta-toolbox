import json
import pandas as pd
from io import StringIO
from typing import Dict
from carta_client.api import RegistryInterface
from carta_client.api.base import BaseInterface


class AtlasInterface(BaseInterface):
    def create_encounter(
        self, encounter_data: Dict, fhir: bool = False, check_response: bool = True
    ):
        """Create Encounter.

        Creates a new encounter in Atlas

        Args:
            encounter_data (dict): Encounter data dictionary
            :param fhir: Boolean
        Returns:
            dict: A dict representing the newly created encounter.
        """
        encounter_url = self.atlas_url + f"/encounters"
        if fhir:
            encounter_url += "/fhir"

        response = self.session.post(encounter_url, json=encounter_data)
        if check_response:
            self.check_response(
                response, failure_message=f"Failed to import encounter."
            )

        return response.json()

    def delete_encounter(self, encounter_id: str, hard: bool = False):
        """Delete Atlas encounter.
        
        Deletes the specified encounter in Atlas.

        Args:
            encounter_id (str): The encounter ID to delete
            hard (bool): Hard vs soft delete
        """
        encounter_url = self.atlas_url + f"/encounters/{encounter_id}"
        params = {}
        if hard:
            params["hard"] = "true"

        response = self.session.delete(encounter_url, params=params)
        self.check_response(
            response, failure_message=f"Failed to delete encounter {encounter_id}"
        )

        return response.json()

    def get_recommendations(self, encounter_id: str):
        encounter_url = self.atlas_url + f"/cartographer_recommendation/{encounter_id}"
        response = self.session.get(encounter_url)
        self.check_response(
            response,
            failure_message=f"Failed to get encounter recommendations for {encounter_id}",
        )

        return response.json()

    def remove_sor_meta(
        self, mrns: list, registry_program: str, version_number: str, customer_id: str
    ):
        encounter_url = self.atlas_url + f"/encounters"
        data = {
            "sor-meta": {"ids": mrns, "field_name": "mrn", "value": None},
            "registry_program": registry_program,
            "version_number": version_number,
            "customer_id": customer_id,
        }
        response = self.session.patch(encounter_url, json=data)
        return response.json()

    def import_patient_encounter(self, registry_name, file_path, customer_id):
        registry_interface = RegistryInterface(self.base_url, self.jwt)
        reg_programs = registry_interface.list_regprograms_nested()
        if registry_name in reg_programs["data"].keys():
            registry_program_id = reg_programs["data"][registry_name][0][
                "registry_program_id"
            ]
        else:
            return f"\nRegistry name '{registry_name}' not found.\nAvailable registries: {list(reg_programs['data'].keys())}\n"
        file_name = file_path.split("/")[-1]
        self.session.headers.update(
            {
                "Content-Disposition": f"attachment; filename={file_name}",
                "Content-type": "text/csv",
            }
        )
        df = pd.read_csv(file_path)
        si = StringIO()
        df.to_csv(si)
        data = si.getvalue()
        customer_id = customer_id if customer_id else "carta"
        payload = {"customer_id": customer_id, "patient_data": data}
        patient_url = self.atlas_url + f"/patients/import/{registry_program_id}"
        response = self.session.post(patient_url, json=payload)
        return response.json()

from .exceptions import CartaClientError
from .api import *
from .utils import Dotfile
from .astrolabe import astrolabe_connect

import os

TOOLBOX_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
CARTA_CLIENT_ROOT = os.path.join(TOOLBOX_ROOT, "carta_client")
DOCKER_REGISTRY = "cr.carta.healthcare"
VAULT_ADDR = "https://vault.carta.healthcare"
